\documentclass[12pt,a4paper]{article}
\usepackage{amsmath}
\usepackage{url}

\author{Grzegorz Mazur\\ \url{mazur@chemia.uj.edu.pl}}
\title{Exchange-Correlation Potential at the Generalized Gradient
  Approximation Level of Theory}

\begin{document}

\maketitle

\begin{abstract}
  Exchange-correlation potential at the Generalized Gradient
  Approximation level of theory is derived in a form which does not
  require calculation of the orbital gradients. While in most cases
  this form is inferior to the commonly used one, it is better suited
  for the DFT-TB two-center calculations.
\end{abstract}

\section{Derivation of the exchange-correlation potential}

The exchange-correlation energy at the Generalized Gradient
Approximation (GGA) level has the form
\begin{equation}
  E_{xc} = \int e_{xc}(\rho_\alpha, \rho_\beta, \gamma_{\alpha\alpha},\gamma_{\alpha\beta}, \gamma_{\beta\beta}) \mathrm{d}\mathbf{r}^3
\end{equation}
where $e_{xc}$ is the exchange-correlation functional and the density
gradient invariants are defined as
\begin{eqnarray}
  \gamma_{\alpha\alpha} & = & (\nabla\rho_\alpha)\circ(\nabla\rho_\alpha)\\
  \gamma_{\alpha\beta} & = & (\nabla\rho_\alpha)\circ(\nabla\rho_\beta)\\
  \gamma_{\beta\beta} & = & (\nabla\rho_\beta)\circ(\nabla\rho_\beta)
\end{eqnarray}

The corresponding exchange-correlation potential is a functional
derivative with respect to the spin density
\begin{equation}
  V^\sigma_{xc} = \frac{\delta E_{xc}}{\delta \rho_\sigma}
\end{equation}
where $\sigma$ denotes spin.

To find the functional derivative, we analyze behaviour of the xc
energy upon perturbation of the spin density by a test function
$\phi$. We assume $\phi$ to be smooth with compact support.
\begin{multline}
  \langle\delta E_{xc}, \phi\rangle=\\=
  \left.\frac{\mathrm{d}}{\mathrm{d}\epsilon}\int e_{xc}(\rho_\alpha+\epsilon\phi, \rho_\beta, (\nabla(\rho_\alpha+\epsilon\phi))\circ(\nabla(\rho_\alpha+\epsilon\phi)),(\nabla(\rho_\alpha+\epsilon\phi))\circ(\nabla\rho_\beta), (\nabla\rho_\beta)\circ(\nabla\rho_\beta)\mathrm{d}\mathbf{r}^3\right\vert_{\epsilon=0}=\\=
\int\left(\frac{\partial e_{xc}}{\partial\rho_\alpha}\phi+2\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}(\nabla\rho_\alpha)\circ(\nabla\phi)+\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}(\nabla\rho_\beta)\circ(\nabla\phi)\right)\mathrm{d}\mathbf{r}^3
\end{multline}
Integrating by parts terms involving gradient of the test function
$\phi$ yields
\begin{equation}
  \langle\delta E_{xc}, \phi\rangle= \int\left(\frac{\partial e_{xc}}{\partial\rho_\alpha}-2\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}\nabla\rho_\alpha\right)-\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}\nabla\rho_\beta\right)\right)\phi\mathrm{d}\mathbf{r}^3
\end{equation}
therefore
\begin{equation}
V^\alpha_{xc} = \frac{\delta E_{xc}}{\delta \rho_\alpha} = \frac{\partial e_{xc}}{\partial\rho_\alpha}-2\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}\nabla\rho_\alpha\right)-\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}\nabla\rho_\beta\right)
\end{equation}
or, more generally
\begin{equation}
V^\sigma_{xc} = \frac{\delta E_{xc}}{\delta \rho_\sigma} = \frac{\partial e_{xc}}{\partial\rho_\sigma}-2\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\nabla\rho_\sigma\right)-\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\nabla\rho_\tau\right)
\end{equation}
where $\sigma$ and $\tau$ denote opposite spins. The formula we
obtained agrees with Eq.~(9) of [Pople1992].

It should be noted that the obtained formula can be transformed
further, leading to a form which is better suited for typical
calculations[pople1992]. However, for our purpose the form is superior
as it does not require calculation of orbital derivatives.

\section{Explicit form of the exchange-correlation potential}

The formula for the GGA exchange-correlation potential 
\begin{equation}
  V^\sigma_{xc} = \frac{\partial e_{xc}}{\partial\rho_\sigma}-2\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\nabla\rho_\sigma\right)-\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\nabla\rho_\tau\right)
\end{equation}
consists of 3 terms. The first of them is the same as in case of the
LDA potential. The next two however are more involved. Transforming
them to the explicit form yields
\begin{equation}
  \nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\nabla\rho_\sigma\right) = \left(\nabla\frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\right)\circ\nabla\rho_\sigma + \frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\nabla^2\rho_\sigma
\end{equation}
\begin{equation}
  \nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\nabla\rho_\tau\right) = \left(\nabla\frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\right)\circ\nabla\rho_\tau + \frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\nabla^2\rho_\tau
\end{equation}
To move further we need to calculate gradient of the xc functional
derivative with respect to the density gradient invariant
\begin{equation}
  \nabla\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}=
  \frac{\partial^2 e_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\alpha}}\nabla\rho_\alpha+
  \frac{\partial^2 e_{xc}}{\partial\rho_\beta\partial\gamma_{\alpha\alpha}}\nabla\rho_\beta+
  \frac{\partial^2 e_{xc}}{\partial\gamma_{\alpha\alpha}^2}\nabla\gamma_{\alpha\alpha}+
  \frac{\partial^2 e_{xc}}{\partial\gamma_{\alpha\beta}\partial\gamma_{\alpha\alpha}}\nabla\gamma_{\alpha\beta}+
  \frac{\partial^2 e_{xc}}{\partial\gamma_{\beta\beta}\partial\gamma_{\alpha\alpha}}\nabla\gamma_{\beta\beta}
\end{equation}
which, apart from the familiar density gradient involves gradient of
the density gradient invariant. 

\begin{eqnarray}
  \nabla\gamma_{\sigma\sigma} & = & 2\mathbf{H}\rho_\sigma\nabla\rho_\sigma\\
  \nabla\gamma_{\sigma\tau} & = & \mathbf{H}\rho_\sigma\nabla\rho_\tau + \mathbf{H}\rho_\sigma\nabla\rho_\tau
\end{eqnarray}
where $\mathbf{H\rho}$ denotes Hessian of the density.

% \begin{multline}
%   \nabla\gamma_{\alpha\alpha} = 2\left(\frac{\partial^2\rho_\alpha}{\partial x^2}\frac{\partial\rho_\alpha}{\partial x} + \frac{\partial^2\rho_\alpha}{\partial x\partial y}\frac{\partial\rho_\alpha}{\partial y}+\frac{\partial^2\rho_\alpha}{\partial x\partial z}\frac{\partial\rho_\alpha}{\partial z}, \right.\\
%     \frac{\partial^2\rho_\alpha}{\partial y\partial x}\frac{\partial\rho_\alpha}{\partial x} + \frac{\partial^2\rho_\alpha}{\partial y^2}\frac{\partial\rho_\alpha}{\partial y}+\frac{\partial^2\rho_\alpha}{\partial x\partial z}\frac{\partial\rho_\alpha}{\partial z}, \\
%     \left.\frac{\partial^2\rho_\alpha}{\partial z\partial x}\frac{\partial\rho_\alpha}{\partial x} + \frac{\partial^2\rho_\alpha}{\partial z\partial y}\frac{\partial\rho_\alpha}{\partial y}+\frac{\partial^2\rho_\alpha}{\partial z^2}\frac{\partial\rho_\alpha}{\partial z}\right)
% \end{multline}
% with similar expression for the mixed spin case.

\section{Explicit form of the exchange-correlation potential for the
  spin-unpolarized systems}

For the spin-unpolarized systems the formulas derived in the previous
section can be simplified even further. Assuming that
\begin{equation}
  \rho_\alpha = \rho_\beta
\end{equation}
the potential
\begin{equation}
  V^\sigma_{xc} = \frac{\partial e_{xc}}{\partial\rho_\sigma}-2\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\sigma}}\nabla\rho_\sigma\right)-\nabla\circ\left(\frac{\partial e_{xc}}{\partial\gamma_{\sigma\tau}}\nabla\rho_\tau\right)
\end{equation}
takes the form
\begin{equation}
  V_{xc} = V^\beta_{xc} = V^\alpha_{xc} = \frac{\partial e_{xc}}{\partial\rho_\alpha}-\nabla\circ\left[\left(2\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}+\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}\right)\nabla\rho_\alpha\right]
\end{equation}
Separating the term involving density Laplacian yields
\begin{equation}
V_{xc} = \frac{\partial e_{xc}}{\partial\rho_\alpha}-\left(2\nabla\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}+\nabla\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}\right)\circ\nabla\rho_\alpha-\left(2\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}+\frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}\right)\nabla^2\rho_\alpha
\end{equation}
and, after simple algebraic transformations,
\begin{multline}
  V_{xc} = \frac{\partial e_{xc}}{\partial\rho_\alpha}-
  2\left(
    \frac{\partial^2e_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\alpha}}+
    \frac{\partial^2e_{xc}}{\partial\rho_\beta\partial\gamma_{\alpha\alpha}}+
    \frac{\partial^2e_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\beta}}
  \right)\gamma_{\alpha\alpha}-\\-
  \left(
    2\frac{\partial^2e_{xc}}{\partial\gamma_{\alpha\alpha}^2}+
    4\frac{\partial^2e_{xc}}{\partial\gamma_{\alpha\beta}\partial\gamma_{\alpha\alpha}}+
    2\frac{\partial^2e_{xc}}{\partial\gamma_{\alpha\alpha}\partial\gamma_{\beta\beta}}+
    \frac{\partial^2e_{xc}}{\partial\gamma_{\alpha\beta}^2}
  \right)\nabla\gamma_{\alpha\alpha}\circ\nabla\rho_\alpha
  -\\-
  \left(
    2\frac{\partial e_{xc}}{\partial\gamma_{\alpha\alpha}}+
    \frac{\partial e_{xc}}{\partial\gamma_{\alpha\beta}}
  \right)\nabla^2\rho_\alpha  
\end{multline}

\section{Explicit form of the exchange-correlation potential for the
  spin-unpolarized, spherically symmetric systems}

For the sperically symmetric the density-gradient related terms in the
exchange-correlation formula take particulary simple form
\begin{equation}
  \gamma_{\alpha\alpha} = \left(\frac{\partial\rho_\alpha}{\partial r}\right)^2
\end{equation}
\begin{equation}
  \nabla\gamma_{\alpha\alpha}\circ\nabla\rho_\alpha=
  2\left(\frac{\partial\rho_\alpha}{\partial r}\right)^2
  \frac{\partial^2\rho_\alpha}{\partial r^2}
\end{equation}
\begin{equation}
  \nabla^2\rho_\alpha = \frac{\partial^2\rho_\alpha}{\partial r^2}+
  \frac{2}{r}\frac{\partial\rho_\alpha}{\partial r}
\end{equation}



\section*{References}

\begin{description}
\item[pople1992] John A. Pople; Peter M. W. Gill; Benny O. Johnson;
  \textit{Chem. Phys. Lett.} 199 (1992) 557
\end{description}

\end{document}
