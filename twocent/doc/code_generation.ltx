\documentclass[12pt,a4paper,notitlepage]{article}
\usepackage[T1]{fontenc}
\usepackage{ae}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{url}

\DeclareMathOperator{\arcsinh}{arcsinh}

\author{Grzegorz Mazur\\<\url{mazur@chemia.uj.edu.pl}>} 

\title{Using the Automatically Generated Exchange-Correlation
  Functional Derivatives Code \mbox{(FORTRAN version)}}



\begin{document}
\maketitle

\section{Introduction}

The document describes usage of a code-generation framework originally
created\cite{mazur2008:development_optimization_computational} for the
purpose of the niedoida\cite{niedoida03} computational chemistry
package, and later extended to generate FORTRAN code.  Primary
objective of the framework was to simplify coding the plethora of
commonly used exchange-correlation functionals.  However, afterwards
it was used to simplify other tedious programming tasks.

The whole framework was written in Yacas, a computer algebra system,
and requires Yacas to be run. However, apart from the code generation
facility, the package contains pre-generated FORTRAN code for several
popular functionals which can be used directly.

The package contains files necessary to generate the code:
\begin{description}
\item[xc.ys] definitions of several popular xc functionals in Yacas
\item[xc\_generate.ys] the xc functional code generator (Yacas)
\item[fortran\_lda\_skeleton.ey] skeleton of a subroutine calculating an
  LDA functional and its derivatives (mixed FORTRAN/Yacas)
\item[fortran\_gga\_skeleton.ey] skeleton of a subroutine calculating an
  LDA functional and its derivatives (mixed FORTRAN/Yacas)
\item[cse.ys] implementation of the Common Subexpression Elimination
  algorithm in Yacas (Yacas)
\item[hyperbolic.ys] definition of $\arcsinh$ function in Yacas
  (Yacas)
\item[cpp\_form.ys] C++ code generator (Yacas)
\item[fortran\_form.ys] FORTRAN code generator (Yacas)
\item[xc\_module.ey] skeleton of a FORTRAN module containing
  subroutines calculating functionals and derivatives (mixed
  FORTRAN/Yacas)
\end{description}
and pre-generated code:
\begin{description}
\item[xc\_module.f90] FORTRAN module containing subroutines
  calculating functionals and derivatives
\item[xc\_tester.f90] FORTRAN program which tests some of the
  functionals (test data taken from Density Functional Repository)
\item[types\_module.f90] auxilliary module used by xc\_tester
\end{description}

\section{Generating the code}

Yacas version 1.2.2 is required to generate the code. It can be
obtained from http://yacas.sourceforge.net/. To generate the code run
\begin{verbatim}
yacas -c -p --patchload xc_module.ey > xc_module.f90
\end{verbatim}
Note that the procedure may take quite a long time. On an AMD
Athlon(tm) 64 Processor 3500+ it takes almost an hour.

\section{Using the generated code}

In order to use the generated code it is necessary to add 
\begin{verbatim}
use xc_module
\end{verbatim}
to the source code of the program and add xc\_module.f90 to the list
of the source files of the project. To perform the calculations one
has to call 
\begin{itemize}
\item in the case of an LDA functional
\begin{verbatim}
call functional(n, rhoa, rhob, res)
\end{verbatim}
\item in the case of a GGA functional
\begin{verbatim}
call functional(n, rhoa, rhob, gammaaa, gammaab, gammabb, res)
\end{verbatim}
\end{itemize}
where \verb|functional| stands for the name of the functional (see
Tab.~\ref{tab:functionals}), \verb|n| is the highest order of the
derivatives requested (up to 2 for now), \verb|rhoa|, \verb|rhob| are
spin densities, \verb|gammaaa|, \verb|gammaab|, \verb|gammabb| are
density gradient invariants and res is an array in which calculated
values will be returned.

Arrangement of the values in the \verb|res| array is described in
Tab.~\ref{tab:arrangement}. The minimal size of the array depends on
the class of the functional (LDA or GGA) and on the requested order of
the derivatives.

\begin{table}
  \centering
  \begin{tabular}{ll}
    \toprule
    Functional & Subroutine \\
    \midrule
    Slater exchange & slater\_x\_functional\\
    VWN3 correlation & vwn3\_c\_functional \\
    VWN5 correlation & vwn\_c\_functional \\
    VWN5 correlation (RPA reparametrization) & vwn\_rpa\_c\_functional\\
    PW91 exchange & pw91\_x\_functional \\
    PW91 correlation & pw91\_c\_functional \\
    PBE exchange & pbe\_x\_functional \\
    PBE correlation & pbe\_c\_functional \\
    Becke88 exchange & becke88\_x\_functional \\
    LYP correlation & lyp\_c\_functional \\
    B3LYP exchange-correlation & b3lyp\_xc\_functional\\
    PBE0 exchange-correlation & pbe0\_xc\_functional\\
    \bottomrule
  \end{tabular}
  \caption{Implemented functionals}
  \label{tab:functionals}
\end{table}

\begin{table}
  \centering
  \begin{onehalfspacing}
  \begin{tabular}{rl}
    \toprule
    Cell & Value \\
    \midrule
    1  & $E_{xc}$\\
    2  & $\frac{\partial E_{xc}}{\partial\rho_\alpha}$\\
    3  & $\frac{\partial E_{xc}}{\partial\rho_\beta}$\\
    4  & $\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\rho_\alpha}$\\
    5  & $\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\rho_\beta}$\\
    6  & $\frac{\partial^2E_{xc}}{\partial\rho_\beta\partial\rho_\beta}$\\
    7  & $\frac{\partial E_{xc}}{\partial\gamma_{\alpha\alpha}}$\\
    8  & $\frac{\partial E_{xc}}{\partial\gamma_{\alpha\beta}}$\\
    9  & $\frac{\partial E_{xc}}{\partial\gamma_{\beta\beta}}$\\
    10 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\alpha\alpha}\partial\gamma_{\alpha\alpha}}$\\
    11 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\alpha\alpha}\partial\gamma_{\alpha\beta}}$\\
    12 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\alpha\alpha}\partial\gamma_{\beta\beta}}$\\
    13 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\alpha\beta}\partial\gamma_{\alpha\beta}}$\\
    14 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\beta\beta}\partial\gamma_{\alpha\beta}}$\\
    15 & $\frac{\partial^2E_{xc}}{\partial\gamma_{\beta\beta}\partial\gamma_{\beta\beta}}$\\
    16 & $\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\alpha}}$\\
    17 & $\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\beta}}$\\
    18 & $\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\gamma_{\beta\beta}}$\\
    19 & $\frac{\partial^2E_{xc}}{\partial\rho_\beta\partial\gamma_{\alpha\alpha}}$\\
    20 & $\frac{\partial^2E_{xc}}{\partial\rho_\beta\partial\gamma_{\alpha\beta}}$\\
    21 & $\frac{\partial^2E_{xc}}{\partial\rho_\beta\partial\gamma_{\beta\beta}}$\\
    \bottomrule
  \end{tabular}
  \end{onehalfspacing}
  \caption{Arrangement of the values in the results array}
  \label{tab:arrangement}
\end{table}

All the functionals described in Tab.~\ref{tab:functionals} except
VWN3 and PBE0 were tested against point data from Density Functional
Repository\cite{functional_density_repository} and are in good
agreement except for
$\frac{\partial^2E_{xc}}{\partial\rho_\alpha\partial\gamma_{\alpha\alpha}}$
in some specific points. I believe that the discrepancy is caused by
an error in the Density Functional Repository data. While VWN3 was not
specifically tested, it seems to be correct because otherwise the
results for B3LYP would be incorrect. PBE0 was tested on several
molecules against other quantum-chemical programs.

\section{Citation}

In addition to the original scientific reference of the functionals,
users of this code are required to cite
Ref.~\cite{mazur2008:development_optimization_computational} and
Ref.~\cite{niedoida03}.

\newpage

\bibliographystyle{unsrt}
\bibliography{twocent}

\end{document}
