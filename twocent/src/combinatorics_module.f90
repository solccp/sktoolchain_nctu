module combinatorics_module

  private

  public :: factorial, factorial_2, choose, binomial_prefactor

contains


  function generalized_factorial(n, n0, s)

    use types_module

    implicit none

    integer(kint) :: generalized_factorial
    integer(kint), intent(in) :: n, n0, s

    integer(kint) :: i

    generalized_factorial = 1

    do i = n0, n, s
       generalized_factorial = generalized_factorial * i
    end do

  end function generalized_factorial

  function factorial(n)

    use types_module

    implicit none

    integer(kint) :: factorial
    integer(kint), intent(in) :: n
!    integer, save, parameter :: pre_calc(n)

    factorial = generalized_factorial(n, 1, 1)

  end function factorial

  function factorial_2(n)

    use types_module

    implicit none

    integer(kint) :: factorial_2
    integer(kint), intent(in) :: n

    factorial_2 = generalized_factorial(n, 1, 2)

  end function factorial_2

  function choose(k, n)

    use types_module

    implicit none

    integer(kint) :: choose
    integer(kint), intent(in) :: k, n

    if (k < n / 2) then
       choose = generalized_factorial(n, k + 1, 1) / factorial(n - k)
    else
       choose = generalized_factorial(n, n - k + 1, 1) / factorial(k);
    end if

  end function choose

  function binomial_prefactor(j, m, n, a, b)

    use types_module

    implicit none

    real(kreal) :: binomial_prefactor

    integer(kint), intent(in) :: j, m, n
    real(kreal), intent(in) :: a, b

    integer(kint) :: k, l

    binomial_prefactor = 0

    do k = 0, m
       do l = 0, n
          if (k + l == j) then

             print *, 'bp', choose(k, m) * choose(l, n) * a**(m - k) * b**(n - l)

             binomial_prefactor = binomial_prefactor + &
                  choose(k, m) * &
                  choose(l, n) * &
                  a**(m - k) * &
                  b**(n - l)
          end if
       end do
    end do

    print *, 'binomial', binomial_prefactor

  end function binomial_prefactor

end module combinatorics_module
