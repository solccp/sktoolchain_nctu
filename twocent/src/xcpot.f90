module libxc


    use types_module
    use xc_f90_types_m
    use xc_f90_lib_m

    logical, save :: libxc_inited = .false.
    logical, save :: gga = .true.
    logical, save :: combined_xc = .false.
    TYPE(xc_f90_pointer_t), save :: xc_func_x, xc_func_c
    TYPE(xc_f90_pointer_t), save :: xc_info_x, xc_info_c

contains

subroutine init_libxc(functional_name)
    implicit none
    character(len=*), intent(in) :: functional_name
    integer :: jj
    jj=index(functional_name," ")-1

    if (libxc_inited) then
        libxc_inited = .false.
        call xc_f90_func_end(xc_func_x)
        if (.not. combined_xc) then
            call xc_f90_func_end(xc_func_c)
        end if
    end if
    gga = .true.
    combined_xc = .false.
    if (functional_name(1:jj) .eq. 'lda') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_LDA_X, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_LDA_C_VWN, XC_POLARIZED)
        gga = .false.
    else if (functional_name(1:jj) .eq. 'pbemol') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_PBE_MOL, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_PBE_MOL, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'pbesol') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_PBE_SOL, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_PBE_SOL, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'pbefe') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_PBEFE, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_PBEFE, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'pbe') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_PBE, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_PBE, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'sg4') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_SG4, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_SG4, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'n12') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_N12, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_N12, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'pw91') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_PW91, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_PW91, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'gam') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_X_GAM, XC_POLARIZED)
        call xc_f90_func_init(xc_func_c, xc_info_c, XC_GGA_C_GAM, XC_POLARIZED)
    else if (functional_name(1:jj) .eq. 'hle16') then
        call xc_f90_func_init(xc_func_x, xc_info_x, XC_GGA_XC_HLE16, XC_POLARIZED)
        combined_xc = .True.
    else
        write(*,*) "Ooops, not supported functional"
        write(*,*) "XC functional =", functional_name
        stop
    end if
    libxc_inited = .true.
end subroutine

subroutine xcpotential(rho,gmaa,res)
!       ****************************************************************
!       *
!       * calculate the value of exchange-correlation potential at point r
!       * type of the potential is decided by the flag functional_name
!       *
!       * input:
!       *   rho   - electron density
!       *   
!       * output:
!       *   vxc   - value of the potential
!       *   
!       ****************************************************************
    implicit none

    real(dp), intent(in)  :: rho, gmaa
    real(dp), intent(out) :: res(21)

    real(dp) :: rhoa
    
    real(dp) :: exc2_x, exc2_c

    real(dp) :: l_rho(2), l_sigma(3)
    real(dp) :: vrho_x(2), vsigma_x(3)
    real(dp) :: v2rho2_x(3), v2rhosigma_x(6), v2sigma2_x(6)

    real(dp) :: vrho_c(2), vsigma_c(3)
    real(dp) :: v2rho2_c(3), v2rhosigma_c(6), v2sigma2_c(6)

    if (.not. libxc_inited) then
        print *, 'libxc is not initialized.'
        stop
    end if

    rhoa = rho/2.0_dp
    l_rho(:) = rhoa

    if (.not. gga) then
        call xc_f90_lda_exc_vxc(xc_func_x, 1, l_rho(1), exc2_x, vrho_x(1))
        call xc_f90_lda_exc_vxc(xc_func_c, 1, l_rho(1), exc2_c, vrho_c(1))
        res(1) = (exc2_x + exc2_c)*rho
        res(2) = vrho_x(1)+vrho_c(1)
        return
    end if

    l_sigma(:) = gmaa
    

    call xc_f90_gga_exc_vxc(xc_func_x, 1, l_rho(1), l_sigma(1), exc2_x, vrho_x(1), vsigma_x(1))
    call xc_f90_gga_fxc(xc_func_x, 1, l_rho(1), l_sigma(1), v2rho2_x(1), v2rhosigma_x(1), v2sigma2_x(1) )

    exc2_c = 0.0_dp
    vrho_c = 0.0_dp
    vsigma_c = 0.0_dp
    v2rho2_c = 0.0_dp
    v2rhosigma_c = 0.0_dp
    v2sigma2_c = 0.0_dp

    if (.not. combined_xc) then   
        call xc_f90_gga_exc_vxc(xc_func_c, 1, l_rho(1), l_sigma(1), exc2_c, vrho_c(1), vsigma_c(1)) 
        call xc_f90_gga_fxc(xc_func_c, 1, l_rho(1), l_sigma(1), v2rho2_c(1), v2rhosigma_c(1), v2sigma2_c(1) )
    end if


    res(1) = (exc2_x + exc2_c)*rho
    res(2:3) = vrho_x+vrho_c
    res(4:6) = v2rho2_x+v2rho2_c
    res(7:9) = vsigma_x+vsigma_c
    res(10:15) = v2sigma2_x+v2sigma2_c
    res(16:21) = v2rhosigma_x+v2rhosigma_c




end subroutine
end module
