module types_module

  implicit none
  integer, parameter :: kint = kind(0)
  integer, parameter :: kreal = kind(0.0d0)
  integer, parameter :: dp = kind(1.0d0)
  real(dp), parameter :: zero=0.0_dp, one=1.0_dp, two=2.0_dp, half=0.5_dp

end module types_module
