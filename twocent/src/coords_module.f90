module coords_module

  use types_module

  type transformation_params_t
     real(kreal) :: a
     real(kreal) :: c(2)
  end type transformation_params_t

contains

  function x1x2_to_uv_transformation(x1x2, tp)

    use types_module

    implicit none

    real(kreal), intent(in) :: x1x2(3)
    type(transformation_params_t), intent(in) :: tp
    real(kreal) :: x1x2_to_uv_transformation(3)
    real(kreal) :: wf1, wf2

    x1x2_to_uv_transformation(1) = tp%c(1) * atanh(x1x2(1))
    x1x2_to_uv_transformation(2) = x1x2(2) + tp%c(2) * sin(2 * x1x2(2))
    wf1 = tp%c(1) / (1 - x1x2(1)**2)
    wf2 = 1 + 2 * tp%c(2) * cos(2 * x1x2(2))
    x1x2_to_uv_transformation(3) = x1x2(3) * wf1 * wf2

  end function x1x2_to_uv_transformation

  function uv_to_xz_transformation(uv, tp)

    use types_module

    implicit none

    real(kreal), intent(in) :: uv(3)
    type(transformation_params_t), intent(in) :: tp
    real(kreal) :: uv_to_xz_transformation(3)
    real(kreal) :: shu, sv

    uv_to_xz_transformation(1) = tp%a * sinh(uv(1)) * sin(uv(2))
    uv_to_xz_transformation(2) = tp%a * cosh(uv(1)) * cos(uv(2))
    shu = sinh(uv(1))
    sv = sin(uv(2))
    uv_to_xz_transformation(3) = uv(3) * tp%a**3 * shu * sv * (shu**2 + sv**2)

  end function uv_to_xz_transformation

  function x1x2_to_xz_transformation(x1x2, tp)

    use types_module

    implicit none

    real(kreal), intent(in) :: x1x2(3)
    type(transformation_params_t), intent(in) :: tp
    real(kreal) :: x1x2_to_xz_transformation(3)

    x1x2_to_xz_transformation = &
         uv_to_xz_transformation(x1x2_to_uv_transformation(x1x2, tp), tp)

  end function x1x2_to_xz_transformation


end module coords_module
