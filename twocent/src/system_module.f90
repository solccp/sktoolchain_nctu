module system_module

  use types_module
  use cs_module

  type, public :: shell_t
     integer(kint) :: l
     integer(kint) :: n
     type(cs_orbital_fit_t) :: radial
     type(cs_kinetic_energy_fit_t) :: radial_kinetic
     real(kreal) :: nf
  end type shell_t

  type, public :: atom_t
     real(kint) :: z
     type(shell_t), dimension(:), allocatable :: shells
     type(cs_density_fit_t) :: density
     type(cs_kinetic_energy_fit_t) :: kinetic_energy
     type(cs_coulomb_potential_fit_t) :: coulomb_potential
     type(cs_total_potential_fit_t) :: total_potential
  end type atom_t

  type, public :: system_t
     real(kreal) :: a
     type(atom_t) :: atoms(2)
  end type system_t

contains
  subroutine normalize(system, overlap)

    implicit none

    type(system_t), intent(inout) :: system
    real(kreal), dimension(:), intent(in) :: overlap

    integer(kint) :: a, s, l
    integer(kint) :: p, pp

    p = 1
    do a = 1, 2
       do s = 1, size(system%atoms(a)%shells, 1)
          l = system%atoms(a)%shells(s)%l
          pp = (p * (p - 1)) / 2 + p
          system%atoms(a)%shells(s)%nf = 1 / sqrt(overlap(pp))
          p = p + 2 * l + 1
       end do
    end do

  end subroutine normalize

end module system_module
