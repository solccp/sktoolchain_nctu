module cs_module

  use types_module

  type, public :: cs_density_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
     real(kreal) :: begin(2)
     real(kreal) :: end(2)
  end type cs_density_fit_t

  type, public :: cs_orbital_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
     real(kreal) :: end(2)
  end type cs_orbital_fit_t

  type, public :: cs_coulomb_potential_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
  end type cs_coulomb_potential_fit_t

  type, public :: cs_nuclear_potential_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
  end type cs_nuclear_potential_fit_t

  type, public :: cs_total_potential_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
  end type cs_total_potential_fit_t

  type, public :: cs_kinetic_energy_fit_t
     private
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:,:), allocatable :: coeffs
  end type cs_kinetic_energy_fit_t


  private

  public :: &
       cs_density_free, cs_density_value, &
       cs_density_deriv, cs_density_deriv2, &
       cs_density_fit_read, &
       cs_orbital_free, cs_orbital_value, &
       cs_orbital_fit_read, &
       cs_coulomb_potential_free, cs_coulomb_potential_value, &
       cs_coulomb_potential_fit_read, &
       cs_nuclear_potential_free, cs_nuclear_potential_value, &
       cs_kinetic_energy_free, cs_kinetic_energy_value, &
       cs_kinetic_energy_fit_read, &
       cs_total_potential_fit_read, cs_total_potential_value, cs_fit_zero

    interface cs_fit_zero
        module procedure cs_fit_zero_d
        module procedure cs_fit_zero_cp
        module procedure cs_fit_zero_tp
    end interface cs_fit_zero

contains
    

  subroutine cs_zero(points, coeffs)

    use types_module
    
    implicit none

    real(kreal), dimension(:), allocatable, intent(out) :: points
    real(kreal), dimension(:,:), allocatable, intent(out) :: coeffs


    allocate(points(2))
    allocate(coeffs(2, 4))

    points(1) = 0.0d0
    points(2) = 1.0E+8
    
    coeffs = 0.0d0

  end subroutine cs_zero


  subroutine cs_fit_zero_d(fit)
    type(cs_density_fit_t), intent(out) :: fit
    call cs_zero(fit%points,fit%coeffs)
    fit%begin = 0.0d0
    fit%end = 0.0d0
  end subroutine
  subroutine cs_fit_zero_cp(fit)
    type(cs_coulomb_potential_fit_t), intent(out) :: fit
    call cs_zero(fit%points,fit%coeffs)
  end subroutine
  subroutine cs_fit_zero_tp(fit)
    type(cs_total_potential_fit_t), intent(out) :: fit
    call cs_zero(fit%points,fit%coeffs)
  end subroutine

 
    function BinarySearch(length, xdata, r) result(rindex)
        integer, intent(in) :: length
        real(kind=8), intent(in) :: xdata(length)
        real(kind=8), intent(in) :: r
        integer :: rindex, bindex, lindex

        bindex=1
        lindex=length
        
        do while ( (lindex-bindex)>1 )
            rindex=(lindex+bindex)/2
            if (r > xdata(rindex)) then
                bindex=rindex
            else if (r < xdata(rindex)) then
                lindex=rindex
            else
                return
            end if
        end do
        rindex=(lindex+bindex)/2
        if (r>xdata(lindex)) then
            rindex=0
        end if

    end function
  function cs_value(points, coeffs, x)

    use types_module

    implicit none

    real(kreal) :: cs_value

    real(kreal), dimension(:), intent(in) :: points
    real(kreal), dimension(:,:), intent(in) :: coeffs
    real(kreal), intent(in) :: x

    real(kreal) :: x1
    integer(kint) :: n, i, j

    n = size(points, 1)
    i = 1

    ! find spline interval containing point x (logarithmic grid)
    if ( points(1) > 1.0d-6 ) then
        x1 = (n-1) * (log(x) - log(points(1)))
        i = 1 + int(x1 / (log(points(n)) - log(points(1))))
    else
        i = BinarySearch(n-1,points,x)
        if (i == 0) then
            i = 1
        end if
    end if

    ! calculate the value at x
    cs_value = 0
    x1 = 1
    do j = 1, 4
       cs_value = cs_value + coeffs(i, j) * x1
       x1 = x1 * (x - points(i))
    end do

  end function cs_value

  function cs_deriv(points, coeffs, x)

    use types_module

    implicit none

    real(kreal) :: cs_deriv

    real(kreal), dimension(:), intent(in) :: points
    real(kreal), dimension(:,:), intent(in) :: coeffs
    real(kreal), intent(in) :: x

    real(kreal) :: x1
    integer(kint) :: n, i

    n = size(points, 1)
    i = 1
    
    ! find spline interval containing point x (logarithmic grid)
    if ( points(1) > 1.0d-6 ) then
        x1 = (n-1) * (log(x) - log(points(1)))
        i = 1 + int(x1 / (log(points(n)) - log(points(1))))
    else
        i = BinarySearch(n-1,points,x)
        if (i == 0) then
            i = 1
        end if
    end if

    ! calculate the value at x
    x1 = x - points(i)
    cs_deriv = coeffs(i, 2) + 2 * coeffs(i, 3) * x1 + 3 * coeffs(i, 4) * x1 * x1

  end function cs_deriv

  function cs_deriv2(points, coeffs, x)

    use types_module

    implicit none

    real(kreal) :: cs_deriv2

    real(kreal), dimension(:), intent(in) :: points
    real(kreal), dimension(:,:), intent(in) :: coeffs
    real(kreal), intent(in) :: x

    real(kreal) :: x1
    integer(kint) :: n, i

    n = size(points, 1)

    ! find spline interval containing point x (logarithmic grid)
    if ( points(1) > 1.0d-6 ) then
        x1 = (n-1) * (log(x) - log(points(1)))
        i = 1 + int(x1 / (log(points(n)) - log(points(1))))
    else
        do i = 1, n-1
            if ( x > points(i) .and. x < points(i+1) ) then
                exit
            end if
        end do
    end if

    ! calculate the value at x
    x1 = x - points(i)
    cs_deriv2 = 2 * coeffs(i, 3) + 6 * coeffs(i, 4) * x1

  end function cs_deriv2

  subroutine cs_fit_read(unit, points, coeffs)

    use types_module
    
    implicit none

    integer(kint), intent(in) :: unit
    real(kreal), dimension(:), allocatable, intent(out) :: points
    real(kreal), dimension(:,:), allocatable, intent(out) :: coeffs
    real(kreal) :: dummyv

    integer(kint) :: n, ii

    read(unit,*) n

    allocate(points(n))
    allocate(coeffs(n, 4))

    do ii = 1, n
        read(unit,*) points(ii),dummyv,coeffs(ii,:)
    end do

  end subroutine cs_fit_read

  subroutine cs_density_free(fit)

    implicit none

    type(cs_density_fit_t), intent(inout) :: fit

    deallocate(fit%points, fit%coeffs)

  end subroutine cs_density_free

  function cs_density_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_density_value
    type(cs_density_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)
    

    if(x >= fit%points(n))then
       cs_density_value = fit%end(1) * exp(-fit%end(2) * x)
    else if(x < fit%points(1))then
       cs_density_value = fit%begin(1) * exp(-x**2 * fit%begin(2))
    else 
       cs_density_value = cs_value(fit%points, fit%coeffs, x)
    end if

    ! account for numerical inaccuracies - density cannot be negative
    if (cs_density_value < 0) then
       cs_density_value = 0
    end if

  end function cs_density_value

  function cs_density_deriv(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_density_deriv
    type(cs_density_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)

    if(x >= fit%points(n))then
       cs_density_deriv = -fit%end(1) * fit%end(2) * exp(-fit%end(2) * x)
    else if(x < fit%points(1))then
       cs_density_deriv = &
            -2 * fit%begin(1) * fit%begin(2) * x * exp(-fit%begin(2) * x**2)
    else 
       cs_density_deriv = cs_deriv(fit%points, fit%coeffs, x)
    end if

  end function cs_density_deriv

  function cs_density_deriv2(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_density_deriv2
    type(cs_density_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)

    if(x >= fit%points(n))then
       cs_density_deriv2 = fit%end(1) * fit%end(2)**2 * exp(-fit%end(2) * x)
    else if(x < fit%points(1))then
       cs_density_deriv2 = &
            -2 * fit%begin(1) * fit%begin(2) * (1 - 2 * fit%begin(2) * x**2) * exp(-fit%begin(2) * x**2)
    else 
       cs_density_deriv2 = cs_deriv2(fit%points, fit%coeffs, x)
    end if

  end function cs_density_deriv2


  subroutine cs_density_fit_read(unit, fit)

    use types_module
    
    implicit none

    real(kreal), parameter :: eps = 1.0d-16

    integer(kint), intent(in) :: unit
    type(cs_density_fit_t), intent(out) :: fit
    real(kreal) :: ae, be, as, bs, datax(2), datay(2)    

    call cs_fit_read(unit, fit%points, fit%coeffs)

    ! find 1st derivative at the beginning of the grid (gaussian fit)
    datax(1) = fit%points(1)
    datax(2) = fit%points(2)
    datay(1) = fit%coeffs(1,1)
    datay(2) = fit%coeffs(2,1)
    bs = log(datay(2) / datay(1)) / (datax(2) + datax(1))/(datax(1) - datax(2))
    as = datay(2) / exp(-bs * datax(2) * datax(2))
    fit%begin(1) = as
    fit%begin(2) = bs

    ! find 1st derivative at the end of the grid (slater fit)
    datax(1) = fit%points(size(fit%points)-1)
    datax(2) = fit%points(size(fit%points))
    datay(1) = fit%coeffs(size(fit%points,1)-1,1)
    datay(2) = fit%coeffs(size(fit%points,1),1)
    if(abs(datay(2)).lt.eps)then
       be = 1
       ae = 0
    else
       be = log(datay(1) / datay(2)) / (datax(2) - datax(1))
       ae = datay(1) / exp(-be * datax(1))
    end if
    fit%end(1) = ae
    fit%end(2) = be

  end subroutine cs_density_fit_read

  subroutine cs_orbital_free(fit)

    implicit none

    type(cs_orbital_fit_t), intent(inout) :: fit

    deallocate(fit%points, fit%coeffs)

  end subroutine cs_orbital_free

  function cs_orbital_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_orbital_value
    type(cs_orbital_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)

    if(x >= fit%points(n))then
       cs_orbital_value = fit%end(1) * exp(-fit%end(2) * x)
    else if (x < fit%points(1)) then
       cs_orbital_value = fit%coeffs(1,1)
    else
       cs_orbital_value = cs_value(fit%points, fit%coeffs, x)
    end if

  end function cs_orbital_value

  subroutine cs_orbital_fit_read(unit, fit)

    use types_module
    
    implicit none

    real(kreal), parameter :: eps = 1.0d-16

    integer(kint), intent(in) :: unit
    type(cs_orbital_fit_t), intent(out) :: fit
    real(kreal) :: ae, be, datax(2), datay(2)
    
    call cs_fit_read(unit, fit%points, fit%coeffs)

    ! find 1st derivative at the end of the grid (slater fit)
    datax(1) = fit%points(size(fit%points)-1)
    datax(2) = fit%points(size(fit%points))
    datay(1) = fit%coeffs(size(fit%points,1)-1,1)
    datay(2) = fit%coeffs(size(fit%points,1),1)
    if(abs(datay(2)).lt.eps)then
       be = 1
       ae = 0
    else
       be = log(datay(1) / datay(2)) / (datax(2) - datax(1))
       ae = datay(1) / exp(-be * datax(1))
    end if
    fit%end(1) = ae
    fit%end(2) = be

  end subroutine cs_orbital_fit_read

  subroutine cs_coulomb_potential_free(fit)

    implicit none

    type(cs_coulomb_potential_fit_t), intent(inout) :: fit

    deallocate(fit%points, fit%coeffs)

  end subroutine cs_coulomb_potential_free

  function cs_coulomb_potential_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_coulomb_potential_value
    type(cs_coulomb_potential_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)

    if(x >= fit%points(n))then
       cs_coulomb_potential_value = 0
    else if (x < fit%points(1)) then
       cs_coulomb_potential_value = 0
    else
       cs_coulomb_potential_value = cs_value(fit%points, fit%coeffs, x)
    end if

  end function cs_coulomb_potential_value

  function cs_total_potential_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_total_potential_value
    type(cs_total_potential_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)

    if(x >= fit%points(n))then
       cs_total_potential_value = 0
    else if (x < fit%points(1)) then
       cs_total_potential_value = fit%coeffs(1,1)
    else
       cs_total_potential_value = cs_value(fit%points, fit%coeffs, x)
    end if

  end function cs_total_potential_value

  subroutine cs_coulomb_potential_fit_read(unit, fit)

    use types_module
    
    implicit none

    integer(kint), intent(in) :: unit
    type(cs_coulomb_potential_fit_t), intent(out) :: fit

    call cs_fit_read(unit, fit%points, fit%coeffs)

  end subroutine cs_coulomb_potential_fit_read

  subroutine cs_total_potential_fit_read(unit, fit)

    use types_module
    
    implicit none

    integer(kint), intent(in) :: unit
    type(cs_total_potential_fit_t), intent(out) :: fit

    call cs_fit_read(unit, fit%points, fit%coeffs)

  end subroutine cs_total_potential_fit_read

  subroutine cs_nuclear_potential_free(fit)

    implicit none

    type(cs_nuclear_potential_fit_t), intent(inout) :: fit

    deallocate(fit%points, fit%coeffs)

  end subroutine cs_nuclear_potential_free

  function cs_nuclear_potential_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_nuclear_potential_value
    type(cs_nuclear_potential_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

!    integer(kint) :: n

!    n = size(fit%points, 1)

!    if(x >= fit%points(n))then
!       cs_nuclear_potential_value = 0
!    else if (x < fit%points(1)) then
!       cs_nuclear_potential_value = 0
!    else
       cs_nuclear_potential_value = cs_value(fit%points, fit%coeffs, x)
!    end if

  end function cs_nuclear_potential_value

  subroutine cs_kinetic_energy_free(fit)

    implicit none

    type(cs_kinetic_energy_fit_t), intent(inout) :: fit

    deallocate(fit%points, fit%coeffs)

  end subroutine cs_kinetic_energy_free

  function cs_kinetic_energy_value(fit, x)

    use types_module

    implicit none

    real(kreal) :: cs_kinetic_energy_value
    type(cs_kinetic_energy_fit_t), intent(in) :: fit
    real(kreal), intent(in) :: x

    integer(kint) :: n

    n = size(fit%points, 1)
    

    if(x >= fit%points(n))then
       cs_kinetic_energy_value = 0
    else if (x < fit%points(1)) then
       cs_kinetic_energy_value = 0
    else
       cs_kinetic_energy_value = cs_value(fit%points, fit%coeffs, x)
    end if

  end function cs_kinetic_energy_value

  subroutine cs_kinetic_energy_fit_read(unit, fit)

    use types_module
    
    implicit none

    integer(kint), intent(in) :: unit
    type(cs_kinetic_energy_fit_t), intent(out) :: fit
    
    call cs_fit_read(unit, fit%points, fit%coeffs)

  end subroutine cs_kinetic_energy_fit_read

end module cs_module
