module quadrature_module

  use types_module

  type, public :: quadrature_t
     real(kreal), dimension(:), allocatable :: points
     real(kreal), dimension(:), allocatable :: weights
  end type quadrature_t

  private

  public :: free_quadrature, &
       naive_quadrature, &
       gauss_legendre_quadrature, &
       weird_quadrature

contains

  subroutine free_quadrature(q)

    implicit none

    type(quadrature_t), intent(inout) :: q
    
    deallocate(q%points, q%weights)
    
  end subroutine free_quadrature

  function naive_quadrature(x1, x2, n)
    
    implicit none

    type(quadrature_t) :: naive_quadrature
    real(kreal), intent(in) :: x1, x2
    integer(kint), intent(in) :: n

    real(kreal) :: xx1, xx2
    real(kreal) :: step
    integer(kint) :: i

    allocate(naive_quadrature%points(n))
    allocate(naive_quadrature%weights(n))

    if (x1 < x2) then
       xx1 = x1
       xx2 = x2
    else
       xx1 = x2
       xx2 = x1
    end if

    step = (xx2 - xx1) / n

    do i = 1, n
       naive_quadrature%points(i) = xx1 + (i - 0.5) * step
    end do

    naive_quadrature%weights = step

  end function naive_quadrature

  function gauss_legendre_quadrature(x1, x2, n)

    use types_module
    use consts_module

    implicit none

    type(quadrature_t) :: gauss_legendre_quadrature
    real(kreal), intent(in) :: x1, x2
    integer(kint), intent(in) :: n

    real(kreal), parameter :: eps = 1.0d-12

    integer(kint) :: m, i, j
    real(kreal) :: xm, xl
    real(kreal) :: z, z1, p1, p2, p3, pp

    allocate(gauss_legendre_quadrature%points(n))
    allocate(gauss_legendre_quadrature%weights(n))

    gauss_legendre_quadrature%points = 0
    gauss_legendre_quadrature%weights = 0

    xm = 0.5 * (x2 + x1);
    xl = 0.5 * (x2 - x1);

    m = (n + 1) / 2;

    do i = 1, m
        z = cos(pi * (i - 0.25) / (n + 0.5));

        do
           p1 = 1
           p2 = 0

            do j = 1, n
               p3 = p2
               p2 = p1;
               p1 = ((2 * j - 1) * z * p2 - (j - 1) * p3) / j;
            end do
            pp = n * (z * p1 - p2) / (z * z - 1)
            z1 = z
            z = z1 - p1 / pp
           if (abs(z - z1) < eps) then
              exit
           end if
        end do

        gauss_legendre_quadrature%points(i) = xm - xl * z
        gauss_legendre_quadrature%points(n - i + 1) = xm + xl * z
        gauss_legendre_quadrature%weights(i) = 2 * xl / ((1 - z * z) * pp * pp)
        gauss_legendre_quadrature%weights(n - i + 1) = gauss_legendre_quadrature%weights(i)

    end do
    
  end function gauss_legendre_quadrature

  function weird_quadrature(x1, x2, n)
    
    implicit none

    type(quadrature_t) :: weird_quadrature
    real(kreal), intent(in) :: x1, x2
    integer(kint), intent(in) :: n

    real(kreal) :: xx1, xx2
    integer(kint) :: i

    allocate(weird_quadrature%points(n))
    allocate(weird_quadrature%weights(n))

    if (x1 < x2) then
       xx1 = x1
       xx2 = x2
    else
       xx1 = x2
       xx2 = x1
    end if

    do i = 1, n
       weird_quadrature%points(i) = xx1 + (xx2 - xx1) * (i - 0.5)**2 / n**2
       weird_quadrature%weights(i) = (xx2 - xx1) * 2 * (i - 0.5) / n**2
    end do

  end function weird_quadrature


end module quadrature_module
