module fock_module
contains
function has_fock2(fock_type) result(res)
    logical :: res
    integer, intent(in) :: fock_type
    if ( fock_type == 2 .or. fock_type == 3 ) then
        res = .true.
    else
        res = .false.
    end if
end function
function has_fock1(fock_type) result(res)
    logical :: res
    integer, intent(in) :: fock_type
    if ( fock_type == 1 .or. fock_type == 3 ) then
        res = .true.
    else
        res = .false.
    end if
end function
subroutine fock_matrix_dense(system, grid, &
                overlap_matrix, fock2_matrix, total_density)
    use consts_module
    use integration_module
    use iso_varying_string
    use system_module
    use types_module
    use libxc

    implicit none

    type(system_t), intent(in) :: system ! parameters of the dimer
    real(kreal), dimension(:,:), intent(in) :: grid ! integration grid

    real(kreal), dimension(:,:,:), intent(out) :: &
         overlap_matrix, &  ! the overlap matrix
         fock2_matrix

    real(kreal), intent(out) :: total_density

    real(kreal), dimension(:,:,:), allocatable :: kinetic, kinetic_integral
    real(kreal), dimension(:,:,:), allocatable :: overlap
    real(kreal), dimension(:,:,:), allocatable :: &
         coulomb_potential_integral, &
         xc_integral
    
    real(kreal) :: rho
    real(kreal) :: drho_dr(2)
    real(kreal) :: d2rho_dr2(2), d2rho_dx2(2), d2rho_dy2(2), d2rho_dz2(2), d2rho_dxz(2)
    real(kreal) :: drho(2), d2rho(4)
    real(kreal) :: coulomb_potential, xc_energy, xc

    integer(kint) :: i, j, n, no_orbitals
    integer(kint) :: a, s

    real(kreal) :: x
    real(kreal) :: z(2), w
    real(kreal) :: r(2), r2(2), r3(2)
    real(kreal) :: cos_theta(2), cos_phi(2)
    real(kreal) :: dr_dx(2), dr_dz(2)
    real(kreal) :: d2r_dx2(2), d2r_dy2(2), d2r_dz2(2), d2r_dxz(2)

    integer(kint) :: lmax1, lmax2, lmax, no_orb_1, no_orb_2

    no_orb_1 = size(system%atoms(1)%shells,1)
    no_orb_2 = size(system%atoms(2)%shells,1)

 
    lmax1 = maxval(system%atoms(1)%shells(:)%l,1)
    lmax2 = maxval(system%atoms(2)%shells(:)%l,1)

    lmax = min(lmax1, lmax2)+1

    allocate(overlap(no_orb_1, no_orb_2, lmax))
    allocate(kinetic(no_orb_1, no_orb_2, lmax))
    allocate(kinetic_integral(no_orb_1, no_orb_2, lmax))

    allocate(coulomb_potential_integral(no_orb_1, no_orb_2, lmax))
    allocate(xc_integral(no_orb_1, no_orb_2, lmax))
    coulomb_potential_integral = 0
    xc_integral = 0


    overlap_matrix = 0
    total_density = 0
    kinetic_integral = 0

    n = size(grid, 1)

!$OMP parallel do default(none) shared(n,gga) firstprivate(system,grid,overlap,kinetic) &
!$OMP& reduction(+:total_density,overlap_matrix,kinetic_integral,xc_integral,COULOMB_POTENTIAL_INTEGRAL) &
!$OMP& private(x,r,z,r2,r3,cos_theta,cos_phi,w,xc,xc_energy,rho,coulomb_potential) &
!$OMP& private(DRHO_DR,DRHO,D2RHO_DR2,DR_DX,DR_DZ,D2R_DX2,D2R_DY2,D2R_DZ2,D2R_DXZ,D2RHO_DX2,D2RHO_DY2,D2RHO_DZ2,D2RHO_DXZ,D2RHO)
    do i = 1, n
        x = grid(i, 1)
        z = (/grid(i, 2) + system%a, grid(i, 2) - system%a/)
        r2 = x**2 + z**2
        r = sqrt(r2)
        r3 = r * r2
        cos_theta = z / r
        cos_phi = x / r
        w = grid(i, 3)


        call calc_overlap(system, r, cos_theta, overlap)
        call calc_kinetic(system, r, cos_theta, kinetic)

        kinetic_integral = kinetic_integral + kinetic * w
       
        overlap_matrix = overlap_matrix + overlap * w

        rho = &
            cs_density_value(system%atoms(1)%density, r(1)) + &
            cs_density_value(system%atoms(2)%density, r(2))


        ! The 2 * pi factor comes from angular (phi) integration
        total_density = total_density + 2.0_kreal * pi * rho * w

        coulomb_potential = &
            cs_coulomb_potential_value(system%atoms(1)%coulomb_potential, r(1)) + &
            cs_coulomb_potential_value(system%atoms(2)%coulomb_potential, r(2))

        coulomb_potential_integral = coulomb_potential_integral + &
            coulomb_potential * overlap * w

        
        ! conversion from total density to spin density
        rho = rho / 2.0_kreal

        if (gga) then
            ! calculate first derivatives of density...

            drho_dr = &
                (/cs_density_deriv(system%atoms(1)%density, r(1)), &
                cs_density_deriv(system%atoms(2)%density, r(2))/)


            drho = &
                (/dot_product(drho_dr, cos_phi), &
                dot_product(drho_dr, cos_theta)/)

            ! conversion from total density to spin density
            drho = drho / 2.0_kreal


            ! ... second derivatives of density

            d2rho_dr2 = &
                (/cs_density_deriv2(system%atoms(1)%density, r(1)), &
                cs_density_deriv2(system%atoms(2)%density, r(2))/)

            dr_dx = x / r
            dr_dz = z / r

            d2r_dx2 = z**2 / r3
            d2r_dy2 = (x**2 + z**2) / r3
            d2r_dz2 = x**2 / r3
            d2r_dxz = -x*z / r3

            d2rho_dx2 = d2rho_dr2 * dr_dx**2 + drho_dr * d2r_dx2
            d2rho_dy2 = drho_dr * d2r_dy2
            d2rho_dz2 = d2rho_dr2 * dr_dz**2 + drho_dr * d2r_dz2
            d2rho_dxz = d2rho_dr2 * dr_dx * dr_dz + drho_dr * d2r_dxz

            d2rho = (/sum(d2rho_dx2), sum(d2rho_dy2), sum(d2rho_dz2), sum(d2rho_dxz)/)

            ! conversion from total density to spin density
            d2rho = d2rho / 2.0_kreal

            ! and finally the potential
            call gga_xc_functional(rho, drho, d2rho, xc_energy, xc)
        else
            call lda_xc_functional(rho, xc_energy, xc)
        end if


        xc_integral = xc_integral + xc * overlap * w
    end do
!$omp end parallel do

    fock2_matrix = kinetic_integral + coulomb_potential_integral + xc_integral
    deallocate(coulomb_potential_integral)
    deallocate(xc_integral)

    deallocate(overlap)
    deallocate(kinetic)
    deallocate(kinetic_integral)
end subroutine fock_matrix_dense

subroutine fock_matrix_pot(system, grid, &
                overlap_matrix, fock1_matrix, total_density)
    use consts_module
    use integration_module
    use iso_varying_string
    use system_module
    use types_module

    implicit none

    type(system_t), intent(in) :: system ! parameters of the dimer
    real(kreal), dimension(:,:), intent(in) :: grid ! integration grid

    real(kreal), dimension(:,:,:), intent(out) :: &
         overlap_matrix, &  ! the overlap matrix
         fock1_matrix       ! the potential-super potential

    real(kreal), intent(out) :: total_density

    real(kreal), dimension(:,:,:), allocatable :: kinetic, kinetic_integral
    real(kreal), dimension(:,:,:), allocatable :: overlap
    real(kreal), dimension(:,:,:), allocatable :: total_potential_integral
    
    real(kreal) :: total_potential

    integer(kint) :: i, j, n, no_orbitals
    integer(kint) :: a, s

    real(kreal) :: x
    real(kreal) :: z(2), w
    real(kreal) :: r(2), r2(2), r3(2)
    real(kreal) :: cos_theta(2), cos_phi(2)

    integer(kint) :: lmax1, lmax2, lmax, no_orb_1, no_orb_2

    no_orb_1 = size(system%atoms(1)%shells,1)
    no_orb_2 = size(system%atoms(2)%shells,1)

    lmax1 = maxval(system%atoms(1)%shells(:)%l,1)
    lmax2 = maxval(system%atoms(2)%shells(:)%l,1)

    lmax = min(lmax1, lmax2)+1

    allocate(overlap(no_orb_1, no_orb_2, lmax))
    allocate(kinetic(no_orb_1, no_orb_2, lmax))
    allocate(kinetic_integral(no_orb_1, no_orb_2, lmax))

    allocate(total_potential_integral(no_orb_1, no_orb_2, lmax))
    total_potential_integral = 0


    overlap_matrix = 0
    total_density = 0
    kinetic_integral = 0

    n = size(grid, 1)

!$OMP parallel do default(none) shared(n) firstprivate(system,grid,overlap,kinetic) &
!$OMP& reduction(+:total_density,overlap_matrix,kinetic_integral,total_potential_integral) &
!$OMP& private(x,r,z,r2,r3,cos_theta,cos_phi,w,total_potential) 
    do i = 1, n
        x = grid(i, 1)
        z = (/grid(i, 2) + system%a, grid(i, 2) - system%a/)
        r2 = x**2 + z**2
        r = sqrt(r2)
        r3 = r * r2
        cos_theta = z / r
        cos_phi = x / r
        w = grid(i, 3)


        call calc_overlap(system, r, cos_theta, overlap)
        call calc_kinetic(system, r, cos_theta, kinetic)

        kinetic_integral = kinetic_integral + kinetic * w
       
        overlap_matrix = overlap_matrix + overlap * w


        total_potential = &
            cs_total_potential_value(system%atoms(1)%total_potential, r(1)) + &
            cs_total_potential_value(system%atoms(2)%total_potential, r(2))
           
        total_potential_integral = total_potential_integral + &
            total_potential * overlap * w
    end do
!$omp end parallel do

    fock1_matrix = kinetic_integral + total_potential_integral
    deallocate(total_potential_integral)

    deallocate(overlap)
    deallocate(kinetic)
    deallocate(kinetic_integral)


  end subroutine fock_matrix_pot
subroutine fock_matrix_both(system, grid, &
                overlap_matrix, fock1_matrix, fock2_matrix, total_density)
    use consts_module
    use integration_module
    use iso_varying_string
    use system_module
    use types_module
    use libxc

    implicit none

    type(system_t), intent(in) :: system ! parameters of the dimer
    real(kreal), dimension(:,:), intent(in) :: grid ! integration grid

    real(kreal), dimension(:,:,:), intent(out) :: &
         overlap_matrix, &  ! the overlap matrix
         fock1_matrix,   &  ! the potential-super potential 
         fock2_matrix
    real(kreal), intent(out) :: total_density

    real(kreal), dimension(:,:,:), allocatable :: kinetic, kinetic_integral
    real(kreal), dimension(:,:,:), allocatable :: overlap
    real(kreal), dimension(:,:,:), allocatable :: &
         coulomb_potential_integral, &
         total_potential_integral, &
         xc_integral
    
    real(kreal) :: rho
    real(kreal) :: drho_dr(2)
    real(kreal) :: d2rho_dr2(2), d2rho_dx2(2), d2rho_dy2(2), d2rho_dz2(2), d2rho_dxz(2)
    real(kreal) :: drho(2), d2rho(4)
    real(kreal) :: coulomb_potential, total_potential, xc_energy, xc

    integer(kint) :: i, j, n, no_orbitals
    integer(kint) :: a, s

    real(kreal) :: x
    real(kreal) :: z(2), w
    real(kreal) :: r(2), r2(2), r3(2)
    real(kreal) :: cos_theta(2), cos_phi(2)
    real(kreal) :: dr_dx(2), dr_dz(2)
    real(kreal) :: d2r_dx2(2), d2r_dy2(2), d2r_dz2(2), d2r_dxz(2)

    integer(kint) :: lmax1, lmax2, lmax, no_orb_1, no_orb_2

    no_orb_1 = size(system%atoms(1)%shells,1)
    no_orb_2 = size(system%atoms(2)%shells,1)

 
    lmax1 = maxval(system%atoms(1)%shells(:)%l,1)
    lmax2 = maxval(system%atoms(2)%shells(:)%l,1)

    lmax = min(lmax1, lmax2)+1

    allocate(overlap(no_orb_1, no_orb_2, lmax))
    allocate(kinetic(no_orb_1, no_orb_2, lmax))
    allocate(kinetic_integral(no_orb_1, no_orb_2, lmax))

    allocate(total_potential_integral(no_orb_1, no_orb_2, lmax))
    total_potential_integral = 0
    allocate(coulomb_potential_integral(no_orb_1, no_orb_2, lmax))
    allocate(xc_integral(no_orb_2, no_orb_2, lmax))
    coulomb_potential_integral = 0
    xc_integral = 0


    overlap_matrix = 0
    total_density = 0
    kinetic_integral = 0

    n = size(grid, 1)

!$OMP parallel do default(none) shared(n,gga) firstprivate(system,grid,overlap,kinetic) &
!$OMP& reduction(+:total_density,overlap_matrix,kinetic_integral,total_potential_integral,xc_integral,COULOMB_POTENTIAL_INTEGRAL) &
!$OMP& private(x,r,z,r2,r3,cos_theta,cos_phi,w,xc,xc_energy,rho,total_potential,coulomb_potential) &
!$OMP& private(DRHO_DR,DRHO,D2RHO_DR2,DR_DX,DR_DZ,D2R_DX2,D2R_DY2,D2R_DZ2,D2R_DXZ,D2RHO_DX2,D2RHO_DY2,D2RHO_DZ2,D2RHO_DXZ,D2RHO)
    do i = 1, n
        x = grid(i, 1)
        z = (/grid(i, 2) + system%a, grid(i, 2) - system%a/)
        r2 = x**2 + z**2
        r = sqrt(r2)
        r3 = r * r2
        cos_theta = z / r
        cos_phi = x / r
        w = grid(i, 3)


        call calc_overlap(system, r, cos_theta, overlap)
        call calc_kinetic(system, r, cos_theta, kinetic)

        kinetic_integral = kinetic_integral + kinetic * w
       
        overlap_matrix = overlap_matrix + overlap * w


        total_potential = &
            cs_total_potential_value(system%atoms(1)%total_potential, r(1)) + &
            cs_total_potential_value(system%atoms(2)%total_potential, r(2))
       
        total_potential_integral = total_potential_integral + &
            total_potential * overlap * w

        rho = &
            cs_density_value(system%atoms(1)%density, r(1)) + &
            cs_density_value(system%atoms(2)%density, r(2))


        ! The 2 * pi factor comes from angular (phi) integration
        total_density = total_density + 2.0_kreal * pi * rho * w

            coulomb_potential = &
                cs_coulomb_potential_value(system%atoms(1)%coulomb_potential, r(1)) + &
                cs_coulomb_potential_value(system%atoms(2)%coulomb_potential, r(2))

            coulomb_potential_integral = coulomb_potential_integral + &
                coulomb_potential * overlap * w

            
            ! conversion from total density to spin density
            rho = rho / 2.0_kreal

            if (gga) then
                ! calculate first derivatives of density...

                drho_dr = &
                    (/cs_density_deriv(system%atoms(1)%density, r(1)), &
                    cs_density_deriv(system%atoms(2)%density, r(2))/)


                drho = &
                    (/dot_product(drho_dr, cos_phi), &
                    dot_product(drho_dr, cos_theta)/)

                ! conversion from total density to spin density
                drho = drho / 2.0_kreal


                ! ... second derivatives of density

                d2rho_dr2 = &
                    (/cs_density_deriv2(system%atoms(1)%density, r(1)), &
                    cs_density_deriv2(system%atoms(2)%density, r(2))/)

                dr_dx = x / r
                dr_dz = z / r

                d2r_dx2 = z**2 / r3
                d2r_dy2 = (x**2 + z**2) / r3
                d2r_dz2 = x**2 / r3
                d2r_dxz = -x*z / r3

                d2rho_dx2 = d2rho_dr2 * dr_dx**2 + drho_dr * d2r_dx2
                d2rho_dy2 = drho_dr * d2r_dy2
                d2rho_dz2 = d2rho_dr2 * dr_dz**2 + drho_dr * d2r_dz2
                d2rho_dxz = d2rho_dr2 * dr_dx * dr_dz + drho_dr * d2r_dxz

                d2rho = (/sum(d2rho_dx2), sum(d2rho_dy2), sum(d2rho_dz2), sum(d2rho_dxz)/)

                ! conversion from total density to spin density
                d2rho = d2rho / 2.0_kreal

                ! and finally the potential
                call gga_xc_functional(rho, drho, d2rho, xc_energy, xc)
            else
                call lda_xc_functional(rho, xc_energy, xc)
            end if


            xc_integral = xc_integral + xc * overlap * w
    end do
!$omp end parallel do

    fock1_matrix = kinetic_integral + total_potential_integral
    deallocate(total_potential_integral)

    
    fock2_matrix = kinetic_integral + coulomb_potential_integral + xc_integral
    deallocate(coulomb_potential_integral)
    deallocate(xc_integral)

    deallocate(overlap)
    deallocate(kinetic)
    deallocate(kinetic_integral)


  end subroutine fock_matrix_both



  subroutine lda_xc_functional(rho, e, v)

    use types_module
    use libxc
!    use xc_module
    
    implicit none

    real(kreal), intent(in) :: rho
    real(kreal), intent(out) :: e, v

    real(kreal) :: xc(21)
    
    call xcpotential(rho,rho,xc)


    e = xc(1)
    v = xc(2)

  end subroutine lda_xc_functional

  subroutine gga_xc_functional(rho, drho, d2rho, e, v)

    use types_module
    use libxc
!    use xc_module
    
    implicit none

!    real(kreal), intent(in) :: rho, drho(2), d2rho(3) <== a bug?
    real(kreal), intent(in) :: rho, drho(2), d2rho(4)
    real(kreal), intent(out) :: e, v

    real(kreal) :: gamma
    real(kreal) :: dgamma(2)
    real(kreal) :: rho_laplacian

    real(kreal) :: xc(21)
    
    real(kreal) :: de_dra, de_dgaa, de_dgab
    real(kreal) :: d2e_dragaa, d2e_drbgaa, d2e_dgaagaa, d2e_dgabgaa, d2e_dgbbgaa
    real(kreal) :: d2e_dragab, d2e_drbgab, d2e_dgaagab, d2e_dgabgab, d2e_dgbbgab
    real(kreal) :: nabla_de_dgaa(2), nabla_de_dgab(2)

    gamma = dot_product(drho, drho)
    call xcpotential(rho*2.0_dp,gamma,xc)


    de_dra = xc(2)
    ! de_drb = xc(3)
    ! d2e_drara = xc(4)
    ! d2e_drarb = xc(5)
    ! d2e_drbrb = xc(6)
    de_dgaa = xc(7)
    de_dgab = xc(8)
    ! de_dgbb = xc(9)
    d2e_dgaagaa = xc(10)
    d2e_dgabgaa = xc(11)
    d2e_dgaagab = xc(11)
    d2e_dgbbgaa = xc(12)
    d2e_dgabgab = xc(13)
    ! d2e_dgbbgab = xc(14)
    ! d2e_dgbbgbb = xc(15)
    d2e_dragaa = xc(16)
    d2e_dragab = xc(17)
    d2e_drbgaa = xc(19)
    d2e_drbgab = xc(20)

    dgamma = &
         (/2 * (drho(1)*d2rho(1) + drho(2)*d2rho(4)), &
         2 * (drho(1)*d2rho(4) + drho(2)*d2rho(3))/)

    rho_laplacian = d2rho(1) + d2rho(2) + d2rho(3)

    nabla_de_dgaa = &
         (d2e_dragaa + d2e_drbgaa) * drho &
         + (d2e_dgaagaa + d2e_dgabgaa + d2e_dgbbgaa) * dgamma

    nabla_de_dgab = &
         (d2e_dragab + d2e_drbgab) * drho &
         + (2 * d2e_dgaagab + d2e_dgabgab) * dgamma

    e = xc(1)
    v = de_dra &
         - 2 * (dot_product(nabla_de_dgaa, drho) + de_dgaa * rho_laplacian) &
         - (dot_product(nabla_de_dgab, drho) + de_dgab * rho_laplacian)

  end subroutine gga_xc_functional

end module fock_module
