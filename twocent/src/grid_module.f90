module grid_module

  private

  public :: build_grid, integral

contains

  subroutine build_grid(a, rm, c2, n1, n2, g)

    use types_module
    use quadrature_module
    use coords_module
    use consts_module

    implicit none

    real(kreal), intent(in) :: a
    real(kreal), intent(in) :: rm
    real(kreal), intent(in) :: c2
    integer(kint), intent(in) :: n1, n2
    real(kreal), dimension(:, :), intent(out) :: g

    type(transformation_params_t) :: tp
    type(quadrature_t) :: q1
    type(quadrature_t) :: q2
    integer(kint) :: i, j, k, n

    tp = transformation_params_t(&
         a, &
         (/2 / log(3.0_kreal) * acosh(1 + rm / a), c2/))


    !q1 = naive_quadrature(0.0_kreal, 1.0_kreal, n1)
    q1 = gauss_legendre_quadrature(0.0_kreal, 1.0_kreal, n1)
    !q1 = weird_quadrature(0.0_kreal, 1.0_kreal, n1)
    !q2 = naive_quadrature(0.0_kreal, pi, n2)
    q2 = gauss_legendre_quadrature(0.0_kreal, pi, n2)

    do i = 1, n1
       do j = 1, n2
          k = (i - 1) * n2 + j
          g(k, 1) = q1%points(i)
          g(k, 2) = q2%points(j)
          g(k, 3) = q1%weights(i) * q2%weights(j)
       end do
    end do

    n = n1 * n2

    do k = 1, n
       g(k, :) = x1x2_to_xz_transformation(g(k, :), tp)
    end do

  end subroutine build_grid

  function integral(g, f)

    use types_module

    implicit none

    real(kreal) :: integral
    real(kreal), dimension(:, :), intent(in) :: g
    real(kreal), external :: f

    integer(kint) :: n, i

    n = size(g, 1)

    integral = 0

    do i = 1, n
       integral = integral + f(g(i, 1), g(i, 2)) * g(i, 3)
    end do

  end function integral


end module grid_module
