program twocent

    use types_module
    use iso_varying_string
    use system_module
    use cs_module
    use grid_module
    use consts_module
    use integration_module
    use pp_module
    use fock_module
    use chem
    use libxc

    implicit none

    integer(kint) :: argc
    integer(kint) :: input_unit
    character(len=256) :: input_fname
    type(varying_string) :: line
    type(varying_string) :: word
    character(len = 256) :: t
    integer(kint) :: iostat

    integer(kint) :: n1, n2
    real(kreal) :: rm, c2
    real(kreal) :: from, to, step
    character(len = 256) :: xc_fun_class, xc_fun_name
    type(varying_string) :: density_fname, potnc_fname, potot_fname
    integer(kint) :: a, s
    integer(kint) :: no_shells, n, l
    type(varying_string) :: radial_fname, lapl_fname
    real(kreal) :: time_begin, time_end
    real(kreal) :: total_time_begin, total_time_end
    real(kreal) :: distance, dummy


    type(system_t) :: system

    integer(kint) :: fock_type
    integer(kint) :: no_steps
    integer(kint) :: i, j
    real(kreal), dimension(:, :), allocatable :: g

    real(kreal), dimension(:), allocatable :: distances

    real(kreal), dimension(:,:,:,:), allocatable :: &
       overlap_integral, &
       fock1_integral, &
       fock2_integral
    real(kreal) :: density_integral

    integer(kint) :: a1, a2, s1, s2, l1, l2, m, ns1, ns2, nm2
    integer(kint) :: p, q, pq, nz, ipair

    integer(kint) :: lmax1, lmax2, lmax, no_orb_1, no_orb_2

    character(len=1), parameter :: l_n(4) = (/'s','p','d','f'/)
    character(len=5), parameter :: b_n(4) = (/'sigma','pi   ','delta','phi  '/)
    character(len=100) :: out_temp, outputname_overl, outputname_fock1, outputname_fock2

    integer :: atom1, atom2
    character(len=32), parameter :: version = '20160224'


!    real(kreal), parameter :: cutoff = 1.0d-5

    call cpu_time(total_time_begin)

    argc = command_argument_count()

    if (argc /= 1) then
     stop 'abnormal termination: wrong arguments'
    end if

    call get_command_argument(1, input_fname)
    if ( trim(input_fname) == '-v' ) then
        print *, version
        stop
    end if



    !  print *, 'processing input file: ', trim(input_fname) 

    input_unit = 7
    open(unit = input_unit, file = input_fname, status = 'old')

    fock_type = 3
    call get(input_unit, line, iostat = iostat)
    t = char(line)
    read (t, *, iostat=iostat) n1, n2, rm, c2, from, to, step, xc_fun_class, xc_fun_name, fock_type

    call init_libxc(xc_fun_name)

    if ( fock_type < 1 .or. fock_type > 3 ) then
        print *, "warning: invalid input for superposition method"
        print *, "  should be 1(potential) or 2(density) or 3(both), assuming 3(both)."
        fock_type = 3
    end if

    if ( fock_type == 1 ) then
        print *, 'Computing only potential superposition integrals'
    elseif ( fock_type == 2 ) then
        print *, 'Computing only density superposition integrals'
    end if
    if ( fock_type == 3 ) then
        print *, 'Computing potential and density superposition integrals'
    end if


    !read the input
    do a = 1, 2
        call get(input_unit, line, iostat = iostat)
        call split(line, word,  var_str(' '))
        t = char(word)
        read (t, *) system%atoms(a)%z

        call split(line, density_fname, var_str(' '))
        if ( has_fock2(fock_type) ) then
            open(17, file = char(density_fname), action = 'read', form='formatted', status = 'old')
            call cs_density_fit_read(17, system%atoms(a)%density)
            close(17)
        end if
        
        call split(line, potnc_fname, var_str(' '))
        if ( has_fock2(fock_type) ) then
            open(17, file = char(potnc_fname), action = 'read', form='formatted', status = 'old')
            call cs_coulomb_potential_fit_read(17, system%atoms(a)%coulomb_potential)
            close(17)
        end if

        call split(line, potot_fname, var_str(' '))
        if ( has_fock1(fock_type) ) then
            open(17, file = char(potot_fname), action = 'read', form='formatted', status = 'old')
            call cs_total_potential_fit_read(17, system%atoms(a)%total_potential)
            close(17)
        end if

        call split(line, word,  var_str(' '))
        t = char(word)
  
        read (t, *) no_shells
        allocate(system%atoms(a)%shells(no_shells))
        do s = 1, no_shells
            call split(line, word,  var_str(' '))
            t = char(word)
            read (t, *) system%atoms(a)%shells(s)%n
            call split(line, word,  var_str(' '))
            t = char(word)
            read (t, *) system%atoms(a)%shells(s)%l
            call split(line, radial_fname, var_str(' '))
            open(17, file = char(radial_fname), action = 'read', form = 'formatted', status = 'old')
            call cs_orbital_fit_read(17, system%atoms(a)%shells(s)%radial)
            close(17)
            system%atoms(a)%shells(s)%nf = 1
            call split(line, lapl_fname, var_str(' '))
            open(17, file = char(lapl_fname), action = 'read', form = 'formatted', status = 'old')
            call cs_kinetic_energy_fit_read(17, system%atoms(a)%shells(s)%radial_kinetic)
            close(17)
        end do
    end do

    no_orb_1 = size(system%atoms(1)%shells,1)
    no_orb_2 = size(system%atoms(2)%shells,1)
    lmax1 = maxval(system%atoms(1)%shells(:)%l,1)
    lmax2 = maxval(system%atoms(2)%shells(:)%l,1)
    lmax = min(lmax1, lmax2)+1


    from = step
    no_steps = ((to - from) + 0.5*step)/ step
    distance = from

    allocate(g(n1 * n2, 3))

    ! normal 3-d matrices representation
    allocate(overlap_integral(no_orb_1, no_orb_2, lmax, no_steps+1 ))

    if ( has_fock1(fock_type) ) then
        allocate(fock1_integral(no_orb_1, no_orb_2, lmax, no_steps+1 ))
    end if
    if ( has_fock2(fock_type) ) then
        allocate(fock2_integral(no_orb_1, no_orb_2, lmax, no_steps+1 ))
    end if
    
    allocate(distances(no_steps+1))

    atom1 = 1
    atom2 = 2
    ns1 = size(system%atoms(atom1)%shells, 1) 
    ns2 = size(system%atoms(atom2)%shells, 1)
    do s = 1, no_steps + 1
        call cpu_time(time_begin)
        distance = from + (s - 1) * step
        distances(s) = distance

        system%a = distance / 2.0_kreal

        call build_grid(system%a, rm, c2, n1, n2, g)

        if ( has_fock1(fock_type) .and. has_fock2(fock_type)) then
            call fock_matrix_both(system, g, &
                overlap_integral(:,:,:,s), fock1_integral(:,:,:,s), fock2_integral(:,:,:,s), density_integral)
        else
            if ( has_fock1(fock_type) ) then
                call fock_matrix_pot(system, g, &
                    overlap_integral(:,:,:,s), fock1_integral(:,:,:,s), density_integral)
            else
                call fock_matrix_dense(system, g, &
                    overlap_integral(:,:,:,s), fock2_integral(:,:,:,s), density_integral)
            end if
        end if

        call cpu_time(time_end)

        write(*, '(a10f14.8a12f5.2a8f8.3)') 'distance: ', distance, 'accuracy: ', &
            -log10(abs(density_integral - nint(density_integral)) / nint(density_integral)), &
            'time: ', time_end - time_begin

    end do

    deallocate(g)


    open(unit=50, file='result.yaml')
    write(50, '(a)') "---"
    write(50, '(a)') "twocent:"        

    do ipair = 1, 2
        if (ipair == 1) then        
            atom1 = 1
            atom2 = 2
        else
            if (system%atoms(1)%z == system%atoms(2)%z) cycle
            atom1 = 2
            atom2 = 1
        end if

        write(50, '(a,a,a,a,a)') "  - pair: [ ", char(trim(var_str(sym(system%atoms(atom1)%z)))), &
        & ", ", char(trim(var_str(sym(system%atoms(atom2)%z)))), " ]"
        write(50, '(a,i0)')       "    points: ", no_steps+1
        write(50, '(a,f10.6)')   "    step: ", step
        write(50, '(a,f10.6)')   "    start: ", from
        write(50, '(a)')         "    x: " 
        do s = 1, no_steps+1
            write(50, '(a,F10.5)')   "        - ", distances(s)
        end do

        write(50, '(a)')         "    integrals: "

        ns1 = size(system%atoms(atom1)%shells, 1) 
        ns2 = size(system%atoms(atom2)%shells, 1)


        do s1 = 1,  ns1
            do s2 = 1, ns2
                if (ipair == 1) then        
                    l1 = s1
                    l2 = s2
                else
                    l1 = s2
                    l2 = s1
                end if
                lmax1 = system%atoms(atom1)%shells(s1)%l
                lmax2 = system%atoms(atom2)%shells(s2)%l
                lmax = min(lmax1, lmax2)+1

                dummy = 1.0
                if (ipair == 2) then
                    dummy = (-1)**(abs(lmax2-lmax1))
                end if

                do m = 1, lmax

                    write(50, '(a,i1,a,a,i1,a,a)')   "    - orbitals: [ ", &
                        & system%atoms(atom1)%shells(s1)%n, &
                        & l_n(lmax1+1), ', ', &
                        & system%atoms(atom2)%shells(s2)%n, &
                        & l_n(lmax2+1), ' ]'
                    write(50, '(a,a,a,a,a,a)')   "      type: ", &
                    & l_n(lmax1+1),'-', &
                    & l_n(lmax2+1),'-',char(trim(var_str(b_n(m))))

!                    write(out_temp, '(a,a,a,a,i1,a,a,i1,a,a,a)') char(trim(var_str(sym(system%atoms(atom1)%z)))) &
!                        & ,'-',char(trim(var_str(sym(system%atoms(atom2)%z)))), &
!                        & '_', system%atoms(atom1)%shells(s1)%n, l_n(lmax1+1),'-', &
!                        & system%atoms(atom2)%shells(s2)%n, l_n(lmax2+1), &
!                        & '-', char(trim(var_str(b_n(m))))


!                    out_temp=adjustl(out_temp)

!                    write(outputname_overl,'(a,a)') 'overl-', trim(out_temp)
!                    if ( has_fock1(fock_type) ) then
!                        write(outputname_fock1,'(a,a)') 'fock1-', trim(out_temp)
!                        open(unit=98,file=trim(outputname_fock1))
!                    end if
!                    if ( has_fock2(fock_type) ) then
!                        write(outputname_fock2,'(a,a)') 'fock2-', trim(out_temp)
!                        open(unit=99,file=trim(outputname_fock2))
!                    end if
                   
!                    open(unit=97,file=trim(outputname_overl))
                
          
                    write(50, '(a)')   "      overlap: "
                    do s = 1, no_steps+1
!                        write(97, '(F10.5, ES25.16)') distances(s),overlap_integral(l1, l2, m, s)*dummy
                        write(50, '(a, ES25.16)')   "        - ", overlap_integral(l1, l2, m, s)*dummy
                    end do
                    if ( has_fock1(fock_type) ) then
                        write(50, '(a)')   "      potential_hamil: "      
                        do s = 1, no_steps+1
!                            write(98, '(F10.5, ES25.16)') distances(s),fock1_integral(l1, l2, m, s)*dummy
                            write(50, '(a, ES25.16)')   "        - ", fock1_integral(l1, l2, m, s)*dummy
                        end do
                    end if
                    if ( has_fock2(fock_type) ) then
                        write(50, '(a)')   "      density_hamil: "                
                        do s = 1, no_steps+1
!                           write(99, '(F10.5, ES25.16)') distances(s),fock2_integral(l1, l2, m, s)*dummy
                            write(50, '(a, ES25.16)')   "        - ", fock2_integral(l1, l2, m, s)*dummy
                        end do
                    end if
        
!                    if ( has_fock2(fock_type) ) then
!                        close(99)
!                    end if
!                    if ( has_fock1(fock_type) ) then
!                        close(98)
!                    end if
!                    close(97)

                end do
            end do
        end do
    end do



    call cpu_time(total_time_end)
    deallocate(overlap_integral, distances)
    if ( allocated(fock1_integral)) then
        deallocate(fock1_integral)
    end if
    if ( allocated(fock2_integral)) then
        deallocate(fock2_integral)
    end if
    write(*, '(a14f8.3)') 'total time: ', total_time_end - total_time_begin
    
    write(*, *) 'normal termination'
    stop
999 continue
end program 
