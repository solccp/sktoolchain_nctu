module special_functions_module

  private

  public :: legendre
  integer, parameter :: factorial_2_arr(9) = (/ 1, 0, 3, 0, 15, 0, 105, 0, 945 /)

contains

  function legendre(l, m, x)

    use types_module
!    use combinatorics_module

    implicit none

    real(kreal) :: legendre
    integer(kint), intent(in) :: l, m
    real(kreal), intent(in) :: x

    real(kreal) :: pmm, pmm1, plm
    integer i

    if (m > l) then
       legendre = 0
       return
    end if

    if (m == 0) then
       pmm = 1
    else
       pmm = factorial_2_arr(2 * m - 1) * (1 - x**2)**(m / 2.0)
       if (mod(m, 2) == 1) then
          pmm = -pmm
       end if
    end if

    if (m == l) then
       legendre = pmm
       return
    end if

    pmm1 = x * (2 * m + 1) * pmm

    if (m + 1 == l) then
       legendre = pmm1
       return
    end if

    do i = m + 2, l
       plm = (x * (2 * i - 1) * pmm1 - (i + m - 1) * pmm) / (i - m)
       pmm = pmm1
       pmm1 = plm
    end do

    legendre = plm;

  end function legendre

end module special_functions_module
