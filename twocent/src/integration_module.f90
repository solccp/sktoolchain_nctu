module integration_module
    integer, parameter :: factorial_arr(0:10) = (/1, 1, &
        & 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800 /)

contains

  ! Calculate the spherical harmonic norm. 
  function angular_norm(l, m)

    use types_module
!    use combinatorics_module
    use consts_module

    implicit none
    
    real(kreal) :: angular_norm
    integer(kint), intent(in) :: l, m

    ! Note the factor 2 instead of 4 - this is because we do numerical
    ! integration only for half of the xz plane
    angular_norm = &
         sqrt((2 * l + 1) * (dble(factorial_arr(l-abs(m)))) &
         /(2 * pi * dble(factorial_arr(l+abs(m)))))

  end function angular_norm


  ! Calculate the orbital product at given point
  subroutine calc_overlap(system, r, cos_theta, overlap)

    use types_module
    use system_module
    use special_functions_module
    use consts_module

    implicit none

    type(system_t), intent(in) :: system
    real(kreal), intent(in) :: r(2), cos_theta(2)
    real(kreal), dimension(:, :, :), intent(inout) :: overlap

    integer(kint) :: a1, a2, s1, s2, l1, l2, m, ns1, ns2, nm2
    real(kreal) :: radial1, radial2
    real(kreal) :: norm1, norm2
    real(kreal) :: f1, f2
    real(kreal) :: nf1, nf2
    real(kreal) :: phi_integral

    a1 = 1
    a2 = 2

    overlap = 0

    ns1 = size(system%atoms(a1)%shells, 1) 
    ns2 = size(system%atoms(a2)%shells, 1)
    
    do s1 = 1, ns1
        nf1 = system%atoms(a1)%shells(s1)%nf
        radial1 = cs_orbital_value(system%atoms(a1)%shells(s1)%radial, r(a1)) * nf1
        l1 = system%atoms(a1)%shells(s1)%l

        do s2 = 1, ns2
            nf2 = system%atoms(a2)%shells(s2)%nf
            radial2 = cs_orbital_value(system%atoms(a2)%shells(s2)%radial, r(a2)) * nf2
            l2 = system%atoms(a2)%shells(s2)%l
            nm2 = l2

            do m = 0, min(l1,l2)
                norm1 = angular_norm(l1, m)
                f1 = norm1 * legendre(l1, abs(m), cos_theta(a1)) * radial1

                norm2 = angular_norm(l2, m)
                f2 = norm2 * legendre(l2, abs(m), cos_theta(a2)) * radial2

                phi_integral = pi
                overlap(s1, s2, m+1) = overlap(s1, s2, m+1) + f1 * f2 * phi_integral
            end do
        end do
    end do

  end subroutine calc_overlap


  ! Calculate the kinetic energy contribution from given grid point
  subroutine calc_kinetic(system, r, cos_theta, kinetic)

    use types_module
    use system_module
    use special_functions_module
    use consts_module

    implicit none

    type(system_t), intent(in) :: system
    real(kreal), intent(in) :: r(2), cos_theta(2)
    real(kreal), dimension(:, :, :), intent(inout) :: kinetic

    integer(kint) :: a1, a2, s1, s2, l1, l2, m, ns1, ns2, nm2
    real(kreal) :: norm1, norm2
    real(kreal) :: radial1, radial2
    real(kreal) :: f1, f2
    real(kreal) :: nf1, nf2
    real(kreal) :: phi_integral

    kinetic = 0
    a1 = 1
    a2 = 2


    ns1 = size(system%atoms(a1)%shells, 1)
    ns2 = size(system%atoms(a2)%shells, 1)

    do s1 = 1, ns1
        nf1 = system%atoms(a1)%shells(s1)%nf
        radial1 = cs_orbital_value(system%atoms(a1)%shells(s1)%radial, r(a1)) * nf1
        l1 = system%atoms(a1)%shells(s1)%l

        do s2 = 1, ns2
            nf2 = system%atoms(a2)%shells(s2)%nf
            radial2 = cs_kinetic_energy_value(system%atoms(a2)%shells(s2)%radial_kinetic, r(a2)) * nf2
            l2 = system%atoms(a2)%shells(s2)%l
            nm2 = l2

            do m = 0, min(l1,l2)

                norm1 = angular_norm(l1, m)
                f1 = norm1 * legendre(l1, abs(m), cos_theta(a1)) * radial1
                norm2 = angular_norm(l2, m)
                f2 = norm2 * legendre(l2, abs(m), cos_theta(a2)) * radial2

                phi_integral = pi

                kinetic(s1, s2, m+1) = kinetic(s1, s2, m+1) + f1 * f2 * phi_integral
            end do
        end do
    end do



  end subroutine calc_kinetic

end module integration_module
