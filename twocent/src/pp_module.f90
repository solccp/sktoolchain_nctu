module pp_module

  contains

    subroutine pp_symmetric_matrix(a)

      use types_module

      implicit none

      real(kreal), dimension(:), intent(in) :: a

      integer(kint) :: s, n, p, q

      s = size(a, 1)

      n = (sqrt(1.0 + 8 * s) - 1) / 2

      do p = 1, n
         write(*,'(20f18.8)') (a((p * (p - 1)) / 2 + q), q = 1, p)
      end do

    end subroutine pp_symmetric_matrix

    subroutine pp_matrix(a)

      use types_module

      implicit none

      real(kreal), dimension(:, :), intent(in) :: a

      integer(kint) :: s, n, p, q

      print*, 'size is ',size(a, 1)


      do p = 1, size(a, 1)
         write(*,'(20f25.16)') (a(p, q), q = 1, size(a, 2))
      end do

    end subroutine pp_matrix

end module pp_module
