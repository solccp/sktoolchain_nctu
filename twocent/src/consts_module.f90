module consts_module
  
  use types_module

  public

  real(kreal), parameter :: pi = 3.141592653589793238462643383279502884_kreal
  real(kreal), parameter :: tiny = 1e-30_kreal

end module consts_module
