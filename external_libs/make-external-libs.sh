#!/bin/bash

git clone https://gitlab.com/libxc/libxc.git
cd libxc
autoreconf -i
./configure --prefix=`pwd`/install

make && make install
cd ..



