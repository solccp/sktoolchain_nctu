
module Limits
    use Accuracy
    integer, parameter :: maxsh=50, maxgrid=10751
    integer, parameter :: maxl = 4
    integer, parameter :: maxn = 10
    real(dp), parameter :: pi = dacos(-1.0_dp)
end module
