

module accuracy
    implicit none
    integer, parameter :: dp = kind(1.0d0)
    real(dp), parameter :: zero=0.0_dp, one=1.0_dp, two=2.0_dp, half=0.5_dp
end module 
