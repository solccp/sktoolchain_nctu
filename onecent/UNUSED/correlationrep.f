      subroutine correlationrep(rhoa1,sigmaaa1,vrhoa,vsigmaaa)
c
c     J.P. Perdew, K. Burke, and M. Ernzerhof
c     Generalized gradient approximation made simple
c     Phys. Rev. Lett. 77 (1996) 3865-3868
c
c
      implicit real*8 (a-h,o-z)
      integer npt
      parameter(npt=1)
      real*8 rhoa1(npt)
      real*8 sigmaaa1(npt)
      real*8 zk(npt),vrhoa(npt),vsigmaaa(npt)
      parameter(tol=1.0d-20)
      

      do i=1,npt
      rho = dmax1(0.D0,rhoa1(i))
      if(rho.gt.tol) then
      sigma = dmax1(0.D0,sigmaaa1(i))
      t2 = 1/rho
      t3 = t2**(1.D0/3.D0)
      t5 = 1.D0+0.1325688999052018D0*t3
      t6 = t2**(1.D0/6.D0)
      t9 = dsqrt(t2)
      t11 = t3**2
      t13 = 0.598255043577108D1*t6+0.2225569421150687D1*t3
     #+0.8004286349993634D0*t9+0.1897004325747559D0*t11
      t16 = 1.D0+0.1608197949869254D2/t13
      t17 = dlog(t16)
      t18 = t5*t17
      t19 = 0.621814D-1*t18
      t20 = rho**2
      t21 = rho**(1.D0/3.D0)
      t23 = 1/t21/t20
      t24 = sigma*t23
      t26 = dexp(0.2000000587336264D1*t18)
      t27 = t26-1.D0
      t28 = 1/t27
      t29 = t28*sigma
      t31 = 0.1362107888567592D0*t29*t23
      t32 = 1.D0+t31
      t33 = t27**2
      t34 = 1/t33
      t35 = sigma**2
      t36 = t34*t35
      t37 = t20**2
      t38 = t21**2
      t40 = 1/t38/t37
      t43 = 1.D0+t31+0.1855337900098064D-1*t36*t40
      t44 = 1/t43
      t45 = t32*t44
      t48 = 1.D0+0.1362107888567592D0*t24*t45
      t49 = dlog(t48)
      t50 = 0.310906908696549D-1*t49
      zk(i) = rho*(-t19+t50)
      t53 = 1/t20
      t54 = 1/t11*t53
      t56 = 0.2747773264188438D-2*t54*t17
      t57 = t13**2
      t60 = t6**2
      t61 = t60**2
      t77 = 0.1D1*t5/t57*(-0.99709173929518D0/t61/t6*t53
     #-0.7418564737168958D0*t54-0.4002143174996817D0/t9*t53
     #-0.1264669550498372D0/t3*t53)/t16
      t80 = 1/t21/t20/rho
      t84 = t34*sigma
      t85 = t56+t77
      t89 = 0.4381079514373337D1*t84*t23*t85*t26
      t91 = 0.3178251739991049D0*t29*t80
      t96 = t43**2
      t98 = t32/t96
      t116 = 1/t48
      vrhoa(i) = -t19+t50+rho*(t56+t77+0.310906908696549D-1*(
     #-0.3178251739991049D0*sigma*t80*t45+0.1362107888567592D0*t24*
     #(t89-t91)*t44-0.1362107888567592D0*t24*t98*(t89-t91
     #+0.119350059339396D1/t33/t27*t35*t40*t85*t26
     #-0.8658243533790967D-1*t36/t38/t37/rho))*t116)
      vsigmaaa(i) = 0.1243627634786196D0*rho*(0.1362107888567592D0*t23
     #*t32*t44+0.1855337900098064D-1*sigma*t40*t28*t44
     #-0.1362107888567592D0*t24*t98*(0.1362107888567592D0*t28*t23
     #+0.3710675800196129D-1*t84*t40))*t116
      else ! rho
      zk(i) = 0.0d0
      vrhoa(i) = 0.0d0
      vsigmaaa(i) = 0.0d0
      endif ! rho
      enddo
      
      return
      end
