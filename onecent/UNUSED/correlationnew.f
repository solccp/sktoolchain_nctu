      SUBROUTINE CORRELATIONNEW(RS,T,UU,VV,igga,vc)
      IMPLICIT REAL*8 (A-H,O-Z)
c----------------------------------------------------------------------
C  INPUT: RS=SEITZ RADIUS=(3/4pi rho)^(1/3)
C       : t=ABS(GRAD rho)/(rho*2.*KS*G)  -- only needed for PBE
C       : UU=(GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*KS*G)**3)
C       : VV=(LAPLACIAN rho)/(rho * (2*KS*G)**2)
c       : igga = (0,LDA), (1,PBE)
c  output: ecl=lda correlation energy from [a]
c        : ecn=NONLOCAL PART OF CORRELATION ENERGY PER ELECTRON
c        : vc=lda correlation potential
c        : dvc=nonlocal correction to vc
c----------------------------------------------------------------------
c References:
c [a] PBE, Phys.Rev.Lett. 77, 3865 (1996)
c [b] PBW, Phys.Rev.B, 54, 16533 (1996)
c [c] PW, Phys.Rev.B 45, 13244 (1992)
c----------------------------------------------------------------------
c     bet=coefficient in gradient expansion for correlation, [a](4).
      parameter(thrd=1.d0/3.d0,thrdm=-thrd,thrd2=2.d0*thrd)
      parameter(GAM=0.5198420997897463295344212145565d0)
      parameter(thrd4=4.d0*thrd, fzz=8.d0/(9.d0*GAM))
      parameter(gamma=0.03109069086965489503494086371273d0)
      parameter(bet=0.06672455060314922d0,delt=bet/gamma)
      dimension u(6)
      data u/ 0.03109070D0, 0.2137000D0, 7.5957000D0,
     $        3.58760000D0, 1.6382000D0, 0.4929400D0/
c----------------------------------------------------------------------
c     find LDA energy contributions [c](10)
c     ECL=LDA correlation energy [c](8), EURS=dEU/drs
c
      rtrs=dsqrt(rs)
      Q0 = -2.D0*u(1)*(1.D0+u(2)*rs)
      Q1 = 2.D0*u(1)*rtrs*(u(3)+rtrs*(u(4)+rtrs*(u(5)+u(6)*rtrs)))
      Q2 = DLOG(1.D0+1.D0/Q1)
      Q3 = u(1)*(u(3)/rtrs+2.D0*u(4)+rtrs*(3.D0*u(5)+4.D0*u(6)*rtrs))
      ECL = Q0*Q2
      EURS = -2.D0*u(1)*u(2)*Q2-Q0*Q3/(Q1*(1.d0+Q1))
c----------------------------------------------------------------------
c     LDA potential from [c](A1)
c
      VC = ECL-RS*EURS/3.D0
      if(igga.eq.0)then
        return
      endif
c----------------------------------------------------------------------
c     PBE correlation energy
c     DELT=bet/gamma , B=A of [a](8)
c
      B = DELT/(DEXP(-ECL/(gamma))-1.D0)
      B2 = B*B
      T2 = T*T
      T4 = T2*T2
      Q4 = 1.D0+B*T2
      Q5 = 1.D0+B*T2+B2*T4
      ECN= (BET/DELT)*DLOG(1.D0+DELT*Q4*T2/Q5)
c----------------------------------------------------------------------
c     ENERGY DONE. NOW THE POTENTIAL, using appendix E of [b].
c
      FAC = DELT/B+1.D0
      BG = -3.D0*B2*ECL*FAC/(BET)
      BEC = B2*FAC/(BET)
      Q8 = Q5*Q5+DELT*Q4*Q5*T2
      Q9 = 1.D0+2.D0*B*T2
      hB = -BET*B*T2*T4*(2.D0+B*T2)/Q8
      hRS = -RS*hB*BEC*EURS/3.0d0
      FACT0 = 2.D0*DELT-6.D0*B
      FACT1 = Q5*Q9+Q4*Q9*Q9
      hBT = 2.D0*BET*T4*((Q4*Q5*FACT0-DELT*FACT1)/Q8)/Q8
      hRST = RS*T2*hBT*BEC*EURS/3.0d0
      hT = 2.d0*BET*Q9/Q8
      FACT3 = 2.D0*B*Q5*Q9+DELT*(Q4*Q5+B*T2*(Q4*Q9+Q5))
      hTT = 4.D0*BET*T*(2.D0*B/Q8-(Q9*FACT3/Q8)/Q8)
      COMM = ECN+HRS+HRST+T2*HT/6.D0+7.D0*T2*T*HTT/6.D0
      DVC = COMM-UU*HTT-VV*HT
      VC = VC + DVC
      
      RETURN
      END
