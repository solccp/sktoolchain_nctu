c ======================================================================
c ======================================================================
c ===                                                                ===
c ===                  Density Functional Theory                     ===
c ===                                                                ===
c ======================================================================
c ======================================================================
c
      subroutine dft(igga,rho,drho,ddrho,r,vxc,exc)
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      real*8 pi
      parameter (pi=3.14159265358979323846264338327950d0)
      real*8 rho,drho,ddrho,r
      real*8 ex(2),vx(2),vc(2),vxc(2)
      integer igga,idft
      save

c **********************************************************************
c *   exchange-correlation-potential and -energy according to several  *
c *   parametrisation of LSDA with GGA extensions                      *
c *   all energies and potentials are in rydberg                       *
c *                                                                    *
c *   written by u.gerstmann, paderborn 1998                           *
c **********************************************************************
C
C we do not have spinpolarisation any more
      z=0.0d0
      dz=0.0d0 
C 
      if(rho.lt.1.d-20)then
        vxc(1)=0.0d0
        vxc(2)=0.0d0
        exc=0.d0
        return
      endif

c *********************************************************************
      rs=(4.d0*pi*rho/3.d0)**(-1.d0/3.d0)
      alfa=(4.d0/(9.d0*pi))**(1.d0/3.d0)
      gg=((1+z)**(2.d0/3.d0)+(1-z)**(2.d0/3.d0))/2.d0   
      t=dabs(drho)/rho*dsqrt(pi/4.d0*alfa*rs)/(2.d0*gg)
      u=dabs(drho)*ddrho/(rho**2)*(dsqrt(pi/4.d0*alfa*rs)/(2.d0*gg))**3
      v=(ddrho+2.d0/r*drho)/rho * (dsqrt(pi/4.d0*alfa*rs)/(2.d0*gg))**2
C      w=drho*dz/rho*(dsqrt(pi/4.d0*alfa*rs)/(2.d0*gg))**2
      w=0.0d0
      idft=igga
      if(igga.ge.4)idft=0
      call correlation(rs,z,t,u,v,w,idft,ec,vc(1),vc(2))

c**********************************************************************
      rho1  =rho/2.d0
      rho2  =rho/2.d0
      drho1 =drho/2.d0
      drho2 =drho/2.d0
      ddrho1=ddrho/2.d0
      ddrho2=ddrho/2.d0
      rs1=(8.d0*pi*rho1/3.d0)**(-1.d0/3.d0)
      rs2=(8.d0*pi*rho2/3.d0)**(-1.d0/3.d0)
      s1=dabs(drho1)*(alfa*rs1/2.d0)/rho1
      s2=dabs(drho2)*(alfa*rs2/2.d0)/rho2
      u1=dabs(drho1)*ddrho1/(rho1**2)*(alfa*rs1/2.d0)**3
      u2=dabs(drho2)*ddrho2/(rho2**2)*(alfa*rs2/2.d0)**3
      t1=(ddrho1+2.d0/r*drho1)/rho1*(alfa*rs1/2.d0)**2
      t2=(ddrho2+2.d0/r*drho2)/rho2*(alfa*rs2/2.d0)**2
    
      call exchange(2.d0*rho1,s1,u1,t1,igga,ex(1),vx(1))
      call exchange(2.d0*rho2,s2,u2,t2,igga,ex(2),vx(2))
      
      exc=((1.d0+z)*ex(1)+(1.d0-z)*ex(2))+2.d0*ec
      vxc(1)=2.d0*(vx(1)+vc(1))
      vxc(2)=2.d0*(vx(2)+vc(2))

      return
      end


















