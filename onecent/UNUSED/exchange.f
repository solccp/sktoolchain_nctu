      subroutine exchange(rho,s,u,t,igga,EX,VX)
      implicit integer*4 (i-n)
      implicit real*8 (a-h,o-z)

      parameter(thrd=1.d0/3.d0,thrd4=4.d0/3.d0)
      parameter(pi=3.14159265358979323846264338327950d0)
      parameter(ax=-0.738558766382022405884230032680836d0)

      parameter(um=0.21951d0,uk=0.8040d0,ul=um/uk)
      
      parameter(ap=1.647127d0,bp=0.980118d0,cp=0.017399d0)
      parameter(aq=1.523671d0,bq=0.367229d0,cq=0.011282d0)
      parameter(ah=0.19645d0,bh=7.7956d0)
      parameter(ahp=0.27430d0,bhp=0.15084d0,ahq=0.004d0)
      parameter(a1=0.19645d0,a2=0.27430d0,a3=0.15084d0,a4=100.d0)
      parameter(a=7.79560d0,b1=0.004d0,eps=1.d-15)

c----------------------------------------------------------------------
c----------------------------------------------------------------------
c  GGA EXCHANGE FOR A SPIN-UNPOLARIZED ELECTRONIC SYSTEM
c----------------------------------------------------------------------
c  INPUT rho : DENSITY
c  INPUT S:  ABS(GRAD rho)/(2*KF*rho), where kf=(3 pi^2 rho)^(1/3)
c  INPUT U:  (GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*KF)**3)
c  INPUT V: (LAPLACIAN rho)/(rho*(2*KF)**2)  (for U,V, see PW86(24))
c  input igga:  (=0=>don't put in gradient corrections, just LDA)
c  OUTPUT:  EXCHANGE ENERGY PER ELECTRON (LOCAL: EXL, NONLOCAL: EXN, 
c           TOTAL: EX) AND POTENTIAL (VX)
c----------------------------------------------------------------------
c References:
c [a]J.P.~Perdew, K.~Burke, and M.~Ernzerhof, submiited to PRL, May96
c [b]J.P. Perdew and Y. Wang, Phys. Rev.  B {\bf 33},  8800  (1986);
c     {\bf 40},  3399  (1989) (E).
c----------------------------------------------------------------------
c Formulas: e_x[unif]=ax*rho^(4/3)  [LDA]
c           ax = -0.75*(3/pi)^(1/3)
c	    e_x[PBE]=e_x[unif]*FxPBE(s)
c	    FxPBE(s)=1+uk-uk/(1+ul*s*s)                 [a](13)
c           uk, ul defined after [a](13) 
c----------------------------------------------------------------------
c----------------------------------------------------------------------
c     construct LDA exchange energy density

      exunif = ax*rho**thrd
      if((igga.eq.0).or.(s.lt.eps))then
	EXL=exunif
	EXN=0.d0
        EX=EXL+EXN
        VX= exunif*thrd4
	return
      endif
c----------------------------------------------------------------------
c     construct GGA enhancement factor
c     find first and second derivatives of f and:
c     fs=(1/s)*df/ds  and  fss=dfs/ds = (d2f/ds2 - (1/s)*df/ds)/s 

C
C PBE enhancement factors checked against NRLMOL
C
      if(igga.eq.1)then
        p0 =1.d0+ul*s**2
        f  =1.d0+uk-uk/p0
        fs =2.d0*uk*ul/p0**2
        fss=-4.d0*ul*s*fs/p0
      endif

C
C the next lines essentially damp numerical instabilities in the
C second derivative near a finite nucleus if the first point of
C the mesh is too close to it
C
C      flimit=2.273d0
C      if(f.gt.flimit)then
C        f=1.d0
C        fs=0.d0
C        fss=0.d0
C      endif  

      EXL= exunif
      EXN= exunif*(f-1.0d0)
      EX = EXL+EXN
c----------------------------------------------------------------------
c     energy done. calculate potential from [b](24) 
c
      VX = exunif*(thrd4*f-(u-thrd4*s**3)*fss-t*fs )
C	 print*,'e igga is',igga,vx,xunif*thrd4


      RETURN
      END

