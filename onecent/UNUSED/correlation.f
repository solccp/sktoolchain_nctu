      SUBROUTINE CORRELATION(RS,ZET,T,UU,VV,WW,igga,ec,vc1,vc2)
      IMPLICIT REAL*8 (A-H,O-Z)
c----------------------------------------------------------------------
C  INPUT: RS=SEITZ RADIUS=(3/4pi rho)^(1/3)
C       : ZET=RELATIVE SPIN POLARIZATION = (rhoup-rhodn)/rho
C       : t=ABS(GRAD rho)/(rho*2.*KS*G)  -- only needed for PBE
C       : UU=(GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*KS*G)**3)
C       : VV=(LAPLACIAN rho)/(rho * (2*KS*G)**2)
C       : WW=(GRAD rho)*(GRAD ZET)/(rho * (2*KS*G)**2
c       : UU,VV,WW, only needed for PBE potential
c       : igga=flag to do gga (0=>LSD only)
c  output: ecl=lsd correlation energy from [a]
c        : ecn=NONLOCAL PART OF CORRELATION ENERGY PER ELECTRON
c        : vcup=lsd up correlation potential
c        : vcdn=lsd dn correlation potential
c        : dvcup=nonlocal correction to vcup
c        : dvcdn=nonlocal correction to vcdn
c----------------------------------------------------------------------
c References:
c [a] J.P.~Perdew, K.~Burke, and M.~Ernzerhof, 
c     {\sl Generalized gradient approximation made simple}, sub.
c     to Phys. Rev.Lett. May 1996.
c [b] J. P. Perdew, K. Burke, and Y. Wang, {\sl Real-space cutoff
c     construction of a generalized gradient approximation:  The PW91
c     density functional}, submitted to Phys. Rev. B, Feb. 1996.
c [c] J. P. Perdew and Y. Wang, Phys. Rev. B {\bf 45}, 13244 (1992).
c----------------------------------------------------------------------
c     bet=coefficient in gradient expansion for correlation, [a](4).
      parameter(thrd=1.d0/3.d0,thrdm=-thrd,thrd2=2.d0*thrd)
      parameter(GAM=0.5198420997897463295344212145565d0)
      parameter(thrd4=4.d0*thrd, fzz=8.d0/(9.d0*GAM))
      parameter(gamma=0.03109069086965489503494086371273d0)
      parameter(bet=0.06672455060314922d0,delt=bet/gamma)
      dimension u(6),p(6),s(6)
      data u/ 0.03109070D0, 0.2137000D0, 7.5957000D0,
     $        3.58760000D0, 1.6382000D0, 0.4929400D0/
      data p/ 0.01554535D0, 0.2054800D0,14.1189000D0,
     $        6.19770000D0, 3.3662000D0, 0.6251700D0/
      data s/ 0.01688690D0, 0.1112500D0,10.3570000D0,
     $        3.62310000D0, 0.8802600D0, 0.4967100D0/
c----------------------------------------------------------------------
c     find LSD energy contributions, using [c](10) .
c     EU=unpolarized LSD correlation energy , EURS=dEU/drs
c     EP=fully polarized LSD correlation energy , EPRS=dEP/drs
c     ALFM=-spin stiffness, [c](3) , ALFRSM=-dalpha/drs .
c     F=spin-scaling factor from [c](9).
c     construct ecl, using [c](8) .
c

      rtrs=dsqrt(rs)
      Q0 = -2.D0*u(1)*(1.D0+u(2)*rtrs*rtrs)
      Q1 = 2.D0*u(1)*rtrs*(u(3)+rtrs*(u(4)+rtrs*(u(5)+u(6)*rtrs)))
      Q2 = DLOG(1.D0+1.D0/Q1)
      Q3 = u(1)*(u(3)/rtrs+2.D0*u(4)+rtrs*(3.D0*u(5)+4.D0*u(6)*rtrs))
      EU = Q0*Q2
      EURS = -2.D0*u(1)*u(2)*Q2-Q0*Q3/(Q1*(1.d0+Q1))
      Q0 = -2.D0*p(1)*(1.D0+p(2)*rtrs*rtrs)
      Q1 = 2.D0*p(1)*rtrs*(p(3)+rtrs*(p(4)+rtrs*(p(5)+p(6)*rtrs)))
      Q2 = DLOG(1.D0+1.D0/Q1)
      Q3 = p(1)*(p(3)/rtrs+2.D0*p(4)+rtrs*(3.D0*p(5)+4.D0*p(6)*rtrs))
      EP = Q0*Q2
      EPRS = -2.D0*p(1)*p(2)*Q2-Q0*Q3/(Q1*(1.d0+Q1))
      Q0 = -2.D0*s(1)*(1.D0+s(2)*rtrs*rtrs)
      Q1 = 2.D0*s(1)*rtrs*(s(3)+rtrs*(s(4)+rtrs*(s(5)+s(6)*rtrs)))
      Q2 = DLOG(1.D0+1.D0/Q1)
      Q3 = s(1)*(s(3)/rtrs+2.D0*s(4)+rtrs*(3.D0*s(5)+4.D0*s(6)*rtrs))
      ALFM = Q0*Q2
      ALFRSM = -2.D0*s(1)*s(2)*Q2-Q0*Q3/(Q1*(1.d0+Q1))

      Z4 = ZET**4
      F=((1.D0+ZET)**THRD4+(1.D0-ZET)**THRD4-2.D0)/GAM
      ECL= EU*(1.D0-F*Z4)+EP*F*Z4-ALFM*F*(1.D0-Z4)/FZZ
c----------------------------------------------------------------------
c     LSD potential from [c](A1)
c     ECRS = dEc/drs , ECZET=dEc/dzeta , FZ = dF/dzeta   [c](A2-A4)
c
      ECRS = EURS*(1.D0-F*Z4)+EPRS*F*Z4-ALFRSM*F*(1.D0-Z4)/FZZ
      FZ = THRD4*((1.D0+ZET)**THRD-(1.D0-ZET)**THRD)/GAM
      ECZET = 4.D0*(ZET**3)*F*(EP-EU+ALFM/FZZ)
     $        +FZ*(Z4*EP-Z4*EU-(1.D0-Z4)*ALFM/FZZ)
      COMM = ECL -RS*ECRS/3.D0-ZET*ECZET
      VCUP = COMM + ECZET
      VCDN = COMM - ECZET
      if(igga.eq.0)then
        EC=ECL
        VC1=VCUP
        VC2=VCDN 
        return
      endif
c----------------------------------------------------------------------
c     PBE correlation energy
c     G=phi(zeta), given after [a](3)
c     DELT=bet/gamma , B=A of [a](8)
c
      G=((1.d0+ZET)**thrd2+(1.d0-ZET)**thrd2)/2.d0
      G3 = G**3
      PON=-ECL/(G3*gamma)
      B = DELT/(DEXP(PON)-1.D0)
      B2 = B*B
      T2 = T*T
      T4 = T2*T2
      Q4 = 1.D0+B*T2
      Q5 = 1.D0+B*T2+B2*T4
      ECN= G3*(BET/DELT)*DLOG(1.D0+DELT*Q4*T2/Q5)
      EC = ECL + ECN
c----------------------------------------------------------------------
c     ENERGY DONE. NOW THE POTENTIAL, using appendix E of [b].
c
      G4 = G3*G
      T6 = T4*T2
      RSTHRD = RS/3.D0
C      GZ=((1.d0+zet)**thirdm-(1.d0-zet)**thirdm)/3.d0
C ckoe: hack thirdm never gets defined, but 1-1 should be zero anyway
      GZ=0.0d0
      FAC = DELT/B+1.D0
      BG = -3.D0*B2*ECL*FAC/(BET*G4)
      BEC = B2*FAC/(BET*G3)
      Q8 = Q5*Q5+DELT*Q4*Q5*T2
      Q9 = 1.D0+2.D0*B*T2
      hB = -BET*G3*B*T6*(2.D0+B*T2)/Q8
      hRS = -RSTHRD*hB*BEC*ECRS
      FACT0 = 2.D0*DELT-6.D0*B
      FACT1 = Q5*Q9+Q4*Q9*Q9
      hBT = 2.D0*BET*G3*T4*((Q4*Q5*FACT0-DELT*FACT1)/Q8)/Q8
      hRST = RSTHRD*T2*hBT*BEC*ECRS
      hZ = 3.D0*GZ*ecn/G + hB*(BG*GZ+BEC*ECZET)
      hT = 2.d0*BET*G3*Q9/Q8
      hZT = 3.D0*GZ*hT/G+hBT*(BG*GZ+BEC*ECZET)
      FACT2 = Q4*Q5+B*T2*(Q4*Q9+Q5)
      FACT3 = 2.D0*B*Q5*Q9+DELT*FACT2
      hTT = 4.D0*BET*G3*T*(2.D0*B/Q8-(Q9*FACT3/Q8)/Q8)
      COMM = ECN+HRS+HRST+T2*HT/6.D0+7.D0*T2*T*HTT/6.D0
      PREF = HZ-GZ*T2*HT/G
      FACT5 = GZ*(2.D0*HT+T*HTT)/G
      COMM = COMM-PREF*ZET-UU*HTT-VV*HT-WW*(HZT-FACT5)
      DVCUP = COMM + PREF
      DVCDN = COMM - PREF
      VC1 = VCUP + DVCUP 
      VC2 = VCDN + DVCDN
C	print*,'c igga is',dvcup
      
      RETURN
      END


