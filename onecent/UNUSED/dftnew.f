c ======================================================================
c ======================================================================
c ===                                                                ===
c ===                  Density Functional Theory                     ===
c ===                                                                ===
c ======================================================================
c ======================================================================
c
      subroutine dftnew(igga,rho,drho,ddrho,r,vxc,vx,vc)
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      real*8 pi
      parameter (pi=3.14159265358979323846264338327950d0)
      real*8 rho,drho,ddrho,r
      real*8 vx,vc,vxc,ex(2)
      integer igga,idft
      save

c **********************************************************************
c *   exchange-correlation-potential and -energy according to several  *
c *   parametrisation of  LDA with GGA extensions                      *
c *   all energies and potentials are in rydberg                       *
c *                                                                    *
c *   written by u.gerstmann, paderborn 1998                           *
c **********************************************************************
C
C we do not have spinpolarisation any more
      zero=0.0d0
      one=1.0d0
C 
      if(rho.lt.1.d-20)then
        vxc=0.0d0
        return
      endif

      idft=igga
      if(igga.ge.4)idft=0

c *********************************************************************
      rs=(4.d0*pi*rho/3.d0)**(-1.d0/3.d0)
      alfa=(4.d0/(9.d0*pi))**(1.d0/3.d0)
c *********************************************************************
      t=dabs(drho)/rho*dsqrt(pi/4.d0*alfa*rs)/(2.d0)
      u=dabs(drho)*ddrho/(rho**2)*(dsqrt(pi/4.d0*alfa*rs)/(2.d0))**3
      v=(ddrho+2.d0/r*drho)/rho * (dsqrt(pi/4.d0*alfa*rs)/(2.d0))**2
      call correlationnew(rs,t,u,v,idft,vc)

c**********************************************************************
      s1=dabs(drho)*(alfa*rs/2.d0)/rho
      u1=dabs(drho)*ddrho/(rho**2)*(alfa*rs/2.d0)**3
      t1=(ddrho+2.d0/r*drho)/rho*(alfa*rs/2.d0)**2
      call exchange(rho,s1,u1,t1,igga,ex(1),vx)
      
      vxc=vx+vc

      return
      end

