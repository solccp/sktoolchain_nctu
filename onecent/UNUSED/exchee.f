C***********************************************************************
	real*8 function exchee(d,dr)
	implicit none
	real*8 d,dr,x,rx
	if (dabs(d).le.1.0d-99) then
	 exchee = 0.0d0
	else
	 x  = (3.0d0*dr*dr/d)**(1.0d0/3.0d0)/30.0d0
	 rx = 1.0d0/x
	 exchee = 0.0252d0*(x*x*x*dlog(1.0d0+rx)+x/2
     &   -x*x-1.0d0/3.0d0-0.20201292d0*rx)
	end if
	return
	end
C***********************************************************************
