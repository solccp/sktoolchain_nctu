!***********************************************************************
!***********************************************************************
subroutine insld(den,dq1,dfl,dv,dr,nqn,nql,nk,nmax,nel,titre,    &
                 norb,dpas,z,nes,np,dcop,test,teste,testy, &
                 testv,niter,ion,conf_w,conf_a,conf_r,rn,nucmod,dvc, &
                 fxcname)
!***********************************************************************
!
!     This routine reads input data and initializes various variables
!
!     The structure of the input file is as follows:
!       line1: z ion norb npg fxcname
!       line2: dcop conf
!       line3-end: den(ii) nqn(ii) nk(ii) nel(ii)  (ii=1,...,norb)
!
!       where: z      is the atomic number
!              ion    is the ionic charge (0 for neutral, -1 for 1 extra electron, etc)
!              norb   is the total number of occupied spinors (s1/2, p1/2, p3/2, etc)
!              npg    is the number of points on the grid (user provided)
!              np     is the number of points on the grid (program default)
!              nes    is the number of trials of spinor energy adjustments 
!              niter  is the maximal number of SCF iterations
!	       fxcname is the name of XC potential to be used in the calculations
!              nucmod is the flag for using point (0) or finite-size (1) nucleus model
!              dcop   is a mixing parameter in SCF for averaging old and new effective potential
!              den    is an array of initial spinor energies
!              dq1    gives the sign of expansion of the small component of each spinor at origin
!              dfl    is the leading power of spinor expansion at origin
!              dv     is (here) the starting Thomas-Fermi effective potential
!              dr     is the vector of grid points
!              nqn    is an array of the principal quantum number of spinors
!              nql    is an array of the angular quantum number of spinors
!              nk     is an array of the kappa quantum number of spinors
!              nmax   is a vector of the last non-zero grid point of each spinor
!              nel    is an array of electron occupation numbers for each spinor
!	       conf_w is the height of the Woods-Saxon confining potential
!	       conf_a is the exponent of the Woods-Saxon confining potential
!	       conf_r is the radius of the Woods-Saxon confining potential
!              titre  is a vector with spinor labels
!              dpas   is the step between points on the logarithmic grid
!              teste  is the convergence threshold for spinor energies
!              testy  is the convergence threshold for spinor radial wave functions
!              testv  is the convergence threshold for effective potential
!              test   is the convergence threshold for the total energy
!              rn     is the effective radius of the nucleus in finite-size nuclear model
!              
!***********************************************************************
    use Accuracy
    use Limits
    implicit none

    integer nucmod,norb,z,ion,npg,maxadj,maxit,nes,niter,ii
    integer jj,np
    integer nk(maxsh),nmax(maxsh),nqn(maxsh),nql(maxsh)
    logical stpot

    real(dp) :: den(maxsh),dq1(maxsh),dfl(maxsh)
    real(dp) :: dv(maxgrid),dr(maxgrid)
    real(dp) :: nel(maxsh),rn,dvc,dpas,test,teste,testv,testy,dcop
    real(dp) :: grid0,dval,eltot,fpot
    real(dp), intent(out) :: conf_w(maxn, maxl),conf_a(maxn, maxl),conf_r(maxn, maxl)

    real(dp) :: l_conf_w, l_conf_a, l_conf_r
    integer  :: dummy_int, dummy_int2, kk
    real(dp) :: dummy_real
    logical  :: orbital_resolved
    character(len=200) :: line

    character(len=20) :: fxcname
    character(len=4)  :: titre(maxsh),ttire(9)
    data ttire /'S1/2','P1/2','P3/2','D3/2','D5/2','F5/2','F7/2','G7/2','G9/2'/

!     initialize variables
!     ====================
!     speed of light
    dvc = 137.03599911_dp
!   non rel
!    dvc = 10000000000_dp

!   step between points on logarithmic grid
!   dpas = 0.0055_dp
!   dpas = 0.055_dp
!   convergence threshold for spinor energies
    teste = 5.0d-9
!   convergence threshold for spinor radial wave function
    testy = 1.0d-9
!   convergence threshold for effective potential
    testv = 4.0d-8
!   convergence threshold for total energy
    test = 1.0d-9
!   parameter for point (0) or finite-size (1) nucleus
!   nucmod = 1
    nucmod = 1
!   maximal number of adjustments of spinor energies
    nes = 50
!   maximal number of SCF iterations
    niter = 1000      

!     read data from the input file
!     =============================
!     z    -> nuclear charge
!     ion  -> ionic charge (0 = neutral, -1 = 1 extra electron, etc)
!     norb -> number of radial spinors (1s1/2, 2s1/2, 2p1/2, 2p3/2, etc)
!     npg  -> number of grid points
!     fxcname -> name of xc functional to be used
    fxcname=' '
    read(5,*)z,ion,norb,npg,fxcname
!   dcop -> mixing parameter for old and new potential
!   conf_w -> height of the Woods-Saxon confining potential
!   conf_a -> exponent of the Woods-Saxon confining potential
!   conf_r -> radius of the Woods-Saxon confining potential
    read(5,*)dcop, l_conf_w,l_conf_a,l_conf_r
    conf_w(:,:) = l_conf_w
    conf_r(:,:) = l_conf_r
    conf_a(:,:) = l_conf_a
    
    jj = 1
    do ii=1, 100
        read(5,'(A)') line
        read(line, *) dummy_real
        dummy_int = int(dummy_real)
        if (dummy_int >= 0 ) then !confining
    
            orbital_resolved = .true.
            read(line, *, ERR=10000, IOSTAT=kk) dummy_int, dummy_int2, l_conf_w, l_conf_a, l_conf_r
            if ( kk /= 0 ) then
                goto 10000
            end if
            Goto 10001
10000       continue 
            orbital_resolved = .false.
            read(line, *) dummy_int2, l_conf_w, l_conf_a, l_conf_r
10001       continue
            if (orbital_resolved) then
                if (dummy_int >0 .and. dummy_int <= maxn ) then
                    if (dummy_int2 < maxl .and. dummy_int2 >= 0) then
                        conf_w(dummy_int, dummy_int2+1) = l_conf_w
                        conf_a(dummy_int, dummy_int2+1) = l_conf_a
                        conf_r(dummy_int, dummy_int2+1) = l_conf_r
                    end if
                end if
            else
                if (dummy_int2 < maxl .and. dummy_int2 >= 0) then
                    conf_w(:, dummy_int2+1) = l_conf_w
                    conf_a(:, dummy_int2+1) = l_conf_a
                    conf_r(:, dummy_int2+1) = l_conf_r
                end if
            end if
        else
            read(line, *) den(1),nqn(1),nk(1),nel(1)
            jj = 2
            exit
        end if
    end do

!   den  -> initial spinor energies
!   nqn  -> principal quantum numbers of spinors
!   nk   -> kappa quantum numbers of spinors
!   nel  -> occupation numbers of spinors
    do ii=jj, norb
        read(5,*)den(ii),nqn(ii),nk(ii),nel(ii)
    end do


!     verify number of grid points
!     ============================
    np=3001
    if(npg > 0)then
        np=2*(npg/2)+1
    end if        
    if(np > maxgrid)then
        write(6,*)' ERROR: too many grid points'
        write(6,*)'  allowed:',maxgrid,'  you have:',NP
        write(6,*)'  recompile with larger maxgrid in maxima.h'
        stop
    end if        

    dpas = log(293.01438857907038_dp/2.0e-5_dp)/dble(np-1)


!     verify number of shells
!     =======================
    if(norb.gt.maxsh)then
        write(6,*)' ERROR: too many shells'
        write(6,*)'  allowed:',maxsh,'  you have:',NORB
        write(6,*)'  recompile with larger maxsh in maxima.h'
        stop
    end if

!     relativistic factor z^2/c^2
!     ===========================
    dval = dble(z*z)/(dvc*dvc)


!     output basic job information
!     ============================
    write(6,10)' Dirac-Fock DFT atomic calculations'
    write(6,11)'  * for element with atomic number:',z
    if (nucmod.eq.0) write(6,10)'  * with point nucleus model'
    if (nucmod.eq.1) write(6,10)'  * with finite-size nucleus model'
    write(6,11)'  * ionic charge:                  ',ION
    write(6,11)'  * number of spinors:             ',NORB
!    print*,fxcname
    write(6,'(a,19x,a)') '  * correlation-exchange model:    ', fxcname
    write(6,11)'  * maximal number of iterations:  ', NITER
    write(6,13)'  * convergence threshold for total energy:  ',TEST
    write(6,13)'  * convergence threshold for shell energies:',TESTE
    write(6,13)'  * convergence threshold for wave function: ',TESTY
    write(6,13)'  * convergence threshold for potential:     ',TESTV
    write(6,11)'  * number of grid points:         ',NP
    write(6,12)'  * integration exponential step:            ',DPAS
    write(6,12)'  * potential mixing parameter in SCF:       ',dcop

!     compute the total number of electrons in the atom
!     =================================================
!    eltot = 0.0_dp 
!    do ii = 1,norb
!        eltot = eltot + nel(ii)
!    end do
    eltot = sum(nel(:norb))
    write(6,12)'  * total number of electrons:               ',eltot
    if(nint(eltot+0.2_dp) /= z-ion)then
        write(6,*)' WARNING: charge inconsistency'
        write(6,*)'  * number of electrons:',eltot
        write(6,*)'  * nuclear charge:',z
        write(6,*)'  * ionic charge:',ion
!        stop
    end if

!     initialize radial spinor related vectors
!     ========================================
    do ii = 1,norb
!       number of non-zero grid point for each radial spinor
        nmax(ii) = np
!       nonrelativistic-like angular quantum number for each spinor
        nql(ii) = iabs(nk(ii))
        if (nk(ii) < 0) nql(ii) = nql(ii)-1
!       sign of small component function at origin for each radial spinor
        dq1(ii) = dble(nk(ii)/iabs(nk(ii)))
        if (mod(nqn(ii)-nql(ii),2) == 0) dq1(ii) = -dq1(ii)
!       leading power for expansion of radial spinors at origin
!       for point nucleus model
        if (nucmod == 0) dfl(ii) = dsqrt(dble(nk(ii)*nk(ii))-dval)
!       for finite-size nucleus model
        if (nucmod == 1) dfl(ii) = dabs(dble(nk(ii)))
!       label of each radial spinor
        titre(ii) = ttire(nql(ii)+iabs(nk(ii)))
    end do

!     verify input information about quantum and occupation numbers of each spinor
!     ============================================================================
    do ii = 1,norb
        if (nql(ii).ge.nqn(ii)) then
            write(6,*)' ERROR: wrong input data'
            write(6,*)'  * l > n for spinor #',ii
            stop
        else if (nel(ii).gt.dble(2*iabs(nk(ii)))) then
            write(6,*)' ERROR: wrong input data'
            write(6,*)'  * too many electrons in spinor #',ii
!            stop
        else if (nqn(ii).le.0) then
            write(6,*)' ERROR: wrong input data'
            write(6,*)'  * n smaller than 1 for spinor #',ii
            stop
        else if (nql(ii).gt.4) then
            write(6,*)' ERROR: wrong input data'
            write(6,*)'  * too high angular momentum for shell #',ii
            stop
        end if
        do jj = 1,ii-1
            if (nqn(ii) == nqn(jj)) then
                if (nk(ii) == nk(jj)) then
                    write(6,*)' ERROR: wrong input data'
                    write(6,*)'  * spinors: ',ii,' and ',jj, ' have the same quantum numbers'
                    stop
                end if
            end if
        end do
    end do
               
!     define integration grid
!     =======================
    grid0 = 2.0e-5_dp
    dr(1) = grid0
    do ii = 2, np
        dr(ii) = dr(1)*dexp((ii-1)*dpas)
    end do

!   compute starting Thomas-Fermi potential (no confinement)
!   ========================================================
    do ii = 1,np
        dv(ii) = fpot(dr(ii),z,-ion-1,rn,nucmod)
    end do
    inquire(file='start_pot',exist=stpot)
    if (stpot) then
        write(6,*)" * using starting total potential from file"
        open(26,file='start_pot')
        do ii = 1, np
            read(26,*) dv(ii)
        end do
        close(26)
    end if


!   printing confining potential info
!   =======================================================
    write(6, 10) '  * confining potentials:'
    do jj = 1, maxval(nqn(:norb))
        dummy_int = 0
        do ii = 1, norb
            if ( nqn(ii) == jj ) then
                dummy_int = max(dummy_int, nql(ii))
            end if
        end do
        do ii = 0, dummy_int
            write(6, 14) '    n= ', jj , 'l= ', ii, 'W = ', conf_w(jj, ii+1), 'A = ', conf_a(jj, ii+1), 'R = ', conf_r(jj, ii+1)
        end do
    end do




!   format cards
!   ============
10  format(a)
11  format(a,16x,i6)
12  format(a,f12.8)
13  format(a,4x,d9.2)
14  format(a,i3,1x,a,i3,1x,a,f13.5,1x,a,f13.5,1x,a,f13.5)

end subroutine
!***********************************************************************
!***********************************************************************

