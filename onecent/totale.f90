!***********************************************************************
!***********************************************************************
subroutine totale(dval,z,dr,np,density,dpas,vcoul,vtot,energy,rn,nucmod)
!***********************************************************************
!
!      compute energy expectation values over various operators
!
!      dval:  (input)  bandstructure energy, on output total energy
!      z:     (input)  nuclear charge
!      dr:    (input)  integration grid
!      np:    (input)  number of grid points
!      density:     (input)  total density at each grid point, watch out for 4pi*r 
!      dpas:  (input)  step width on mesh, dr(ii)=dr(1)*dexp((ii-1)*dpas)
!      vcoul:   (input)  coulomb potential eventually multiplied with 1/r
!      vtot:   (input)  total potential
!      energy:    (output) energies
!                        energy(4) kinetic energy
!                        energy(3) Coulomb energy
!                        energy(2) exchange energy
!                        energy(1) nuclear attraction energy
!      fxcname: (input) name of the XC potential to be used in calculations
!      rn:    (input)  radius of the nucleus in the finite-size nucleus model
!      nucmod:(input)  flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
!
!***********************************************************************
    use Accuracy
    use Limits
    use libxc
    implicit none

    integer :: ii

    integer, intent(in) :: z, np, nucmod
    real(dp), intent(in) :: dpas
    real(dp), intent(inout) :: dval

    real(dp) :: exchee,nucpot,rn,exc,vxc(2),exc1,vxc1
    real(dp) :: d0(maxgrid),d1(maxgrid),d2(maxgrid)
    real(dp) :: temp1(maxgrid),dq(maxgrid)
    real(dp), intent(out) :: energy(5)
    real(dp), intent(in) :: density(maxgrid), vcoul(maxgrid), dr(maxgrid), vtot(maxgrid)

    dq = 0.0
    !     compute integral energy(1) over nuclear repulsion potential
    !     =======================================================
    if (nucmod == 0) energy(1) = 1.0_dp
    if (nucmod == 1) energy(1) = 4.0_dp
    do ii = 1,np
        temp1(ii) = density(ii)*nucpot(dr(ii),z,rn,nucmod)
    end do

    call somm(dr,temp1,dq,dpas,energy(1),0,np)

    !     compute integral energy(3) over total effective potential
    !     =====================================================
    if (nucmod == 0) energy(3) = 1.0_dp
    if (nucmod == 1) energy(3) = 4.0_dp
    temp1(:np)  = density(:np)*vtot(:np)
    call somm(dr,temp1,dq,dpas,energy(3),0,np)

    !     compute integral energy(5) over Coulomb potential
    !     =============================================
    energy(5) = 2.0_dp
    temp1(:np) = density(:np)*vcoul(:np)
    call somm(dr,temp1,dq,dpas,energy(5),0,np)

    !     compute integral energy(2) over exchange-correlation density
    !     ========================================================
    d0(:np) = density(:np) / (4.0_dp * pi * dr(:np) * dr(:np))  
    call diffrho(d0,np,d1,d2,dr,dpas)
    do ii = 1,np
        call xcpotential(d0(ii),d1(ii),d2(ii),dr(ii),exc1,vxc1)
        d0(ii) = exc1 * 4.0_dp * pi * dr(ii) * dr(ii)
    end do
    energy(2)=2.0_dp      
    call somm(dr,d0,dq,dpas,energy(2),0,np)

    !     compute kinetic energy integral energy(4) as a difference between band
    !     structure energy and the integral over the total effective potential
    !     ====================================================================
    energy(4) = dval - energy(3)

    !     compute the integral over the exchange-correlation potential
    !     ============================================================
    vxc(1) = energy(3) - energy(1) - energy(5) - energy(2)

    !     compute total atomic energy by subtracting the double counting 
    !     terms from the band structure energy
    !     ====================================
    dval = dval - 0.5_dp*energy(5) - vxc(1)
    energy(3) = 0.5_dp*energy(5)

!
end subroutine
!***********************************************************************
!***********************************************************************
