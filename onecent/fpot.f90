!***********************************************************************
!***********************************************************************
function fpot(r,z,wa,rn,nucmod) result(res)
!***********************************************************************
!
!     compute initial effective potential for the atomic problem at distance r
!     for an atom with z+wa electrons and the atomic number z using Thomas-Fermi 
!     method and a point (nucmod=0) or finite-size (nucmod=1) nucleus model
!
!     wa is equal to -ion-1, where ion is the ionic charge
!
!***********************************************************************
    use Accuracy
    implicit none
    real(dp) :: res
    integer :: nucmod,z,wa
    real(dp) r,rn,wc,wd,we,nucpot
    wc = dsqrt((r*dble(z+wa)**(1.0D0/3.0D0))/0.8853D0)
    wd = wc*(0.60112D0*wc+1.81061D0)+1.0D0
    we = wc*(wc*(wc*(wc*(0.04793D0*wc+0.21465D0)+0.77112D0)+1.39515D0)+1.81061D0)+1.0D0
    wc = dble(z+wa)*(wd/we)**2 - dble(wa)
    res = (wc/dble(z))*nucpot(r,z,rn,nucmod)
end function
!***********************************************************************
!***********************************************************************

