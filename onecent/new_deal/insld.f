C***********************************************************************
C***********************************************************************
      subroutine insld(den,dq1,dfl,nqn,nql,nk,nmax,nel,titre,
     &           norb,dpas,z,nes,np,dcop,test,teste,testy,
     &           testv,niter,ion,conf_w,conf_a,conf_r,rn,nucmod,dvc,
     &           fxcname)
C***********************************************************************
C
C     This routine reads input data and initializes various variables
C
C     The structure of the input file is as follows:
C       line1: z ion norb npg fxcname
C       line2: dcop conf
C       line3-end: den(ii) nqn(ii) nk(ii) nel(ii)  (ii=1,...,norb)
C
C       where: z      is the atomic number
C              ion    is the ionic charge (0 for neutral, -1 for 1 extra electron, etc)
C              norb   is the total number of occupied spinors (s1/2, p1/2, p3/2, etc)
C              npg    is the number of points on the grid (user provided)
C              np     is the number of points on the grid (program default)
C              nes    is the number of trials of spinor energy adjustments 
C              niter  is the maximal number of SCF iterations
C	       fxcname is the name of XC potential to be used in the calculations
C              nucmod is the flag for using point (0) or finite-size (1) nucleus model
C              dcop   is a mixing parameter in SCF for averaging old and new effective potential
C              den    is an array of initial spinor energies
C              dq1    gives the sign of expansion of the small component of each spinor at origin
C              dfl    is the leading power of spinor expansion at origin
C              dv     is (here) the starting Thomas-Fermi effective potential
C              dr     is the vector of grid points
C              nqn    is an array of the principal quantum number of spinors
C              nql    is an array of the angular quantum number of spinors
C              nk     is an array of the kappa quantum number of spinors
C              nmax   is a vector of the last non-zero grid point of each spinor
C              nel    is an array of electron occupation numbers for each spinor
C	       conf_w is the height of the Woods-Saxon confining potential
C	       conf_a is the exponent of the Woods-Saxon confining potential
C	       conf_r is the radius of the Woods-Saxon confining potential
C              titre  is a vector with spinor labels
C              dpas   is the step between points on the logarithmic grid
C              teste  is the convergence threshold for spinor energies
C              testy  is the convergence threshold for spinor radial wave functions
C              testv  is the convergence threshold for effective potential
C              test   is the convergence threshold for the total energy
C              rn     is the effective radius of the nucleus in finite-size nuclear model
C              
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer nucmod,norb,z,ion,npg,maxadj,maxit,nes,niter,ii
      integer jj,np
      integer nk(maxsh),nmax(maxsh),nqn(maxsh),nql(maxsh)

      real*8 den(maxsh),dq1(maxsh),dfl(maxsh)
      !real*8 dv(maxgrid),dr(maxgrid)
      real*8 nel(maxsh),rn,dvc,dpas,test,teste,testv,testy,dcop
      real*8 grid0,dval,eltot,fpot,conf_w,conf_a,conf_r

      character*20 fxcname
      character*4 titre(maxsh),ttire(9)
      data ttire /'S1/2','P1/2','P3/2','D3/2','D5/2','F5/2',
     &'F7/2','G7/2','G9/2'/

C     initialize variables
C     ====================
C     speed of light
      dvc = 137.03599911d0
C     step between points on logarithmic grid
      dpas = 0.0018d0
C     convergence threshold for spinor energies
      teste = 5.0d-9
C     convergence threshold for spinor radial wave function
      testy = 1.0d-9
C     convergence threshold for effective potential
      testv = 4.0d-8
C     convergence threshold for total energy
      test = 1.0d-9
C     parameter for point (0) or finite-size (1) nucleus
      nucmod = 1
C     maximal number of adjustments of spinor energies
      nes = 30
C     maximal number of SCF iterations
      niter = 250      

C     read data from the input file
C     =============================
C     z    -> nuclear charge
C     ion  -> ionic charge (0 = neutral, -1 = 1 extra electron, etc)
C     norb -> number of radial spinors (1s1/2, 2s1/2, 2p1/2, 2p3/2, etc)
C     npg  -> number of grid points
C     fxcname -> name of xc functional to be used
      fxcname=' '
      read(5,*)z,ion,norb,npg,fxcname
C     dcop -> mixing parameter for old and new potential
C     conf_w -> height of the Woods-Saxon confining potential
C     conf_a -> exponent of the Woods-Saxon confining potential
C     conf_r -> radius of the Woods-Saxon confining potential
      read(5,*)dcop,conf_w,conf_a,conf_r
C     den  -> initial spinor energies
C     nqn  -> principal quantum numbers of spinors
C     nk   -> kappa quantum numbers of spinors
C     nel  -> occupation numbers of spinors
      do ii=1,norb
        read(5,*)den(ii),nqn(ii),nk(ii),nel(ii)
      end do

C     verify number of grid points
C     ============================
      np=9001
      if(npg.gt.0)then
        np=2*(npg/2)+1
      end if        
      if(np.gt.maxgrid)then
        write(6,*)' ERROR: too many grid points'
        write(6,*)'  allowed:',maxgrid,'  you have:',NP
        write(6,*)'  recompile with larger maxgrid in maxima.h'
        stop
      end if        

C     verify number of shells
C     =======================
      if(norb.gt.maxsh)then
        write(6,*)' ERROR: too many shells'
        write(6,*)'  allowed:',maxsh,'  you have:',NORB
        write(6,*)'  recompile with larger maxsh in maxima.h'
        stop
      end if

C     relativistic factor z^2/c^2
C     ===========================
      dval = dble(z*z)/(dvc*dvc)


C     output basic job information
C     ============================
      write(6,10)' Dirac-Fock DFT atomic calculations'
      write(6,11)'  * for element with atomic number:',z
      if (nucmod.eq.0) write(6,10)'  * with point nucleus model'
      if (nucmod.eq.1) write(6,10)'  * with finite-size nucleus model'
      write(6,11)'  * ionic charge:                  ',ION
      write(6,11)'  * number of spinors:             ',NORB
	print*,fxcname
      write(6,'(a,19x,a)') '  * correlation-exchange model:    ',
     &                    fxcname
      write(6,11)'  * maximal number of iterations:  ', NITER
      write(6,13)'  * convergence threshold for total energy:  ',TEST
      write(6,13)'  * convergence threshold for shell energies:',TESTE
      write(6,13)'  * convergence threshold for wave function: ',TESTY
      write(6,13)'  * convergence threshold for potential:     ',TESTV
      write(6,11)'  * number of grid points:         ',NP
      write(6,12)'  * integration exponential step:            ',DPAS
      write(6,12)'  * potential mixing parameter in SCF:       ',dcop

C     compute the total number of electrons in the atom
C     =================================================
      eltot = 0.0d0
      do ii = 1,norb
        eltot = eltot + nel(ii)
      end do
      write(6,12)'  * total number of electrons:               ',eltot
      if(nint(eltot+0.2d0).ne.z-ion)then
        write(6,*)' ERROR: charge inconsistency'
        write(6,*)'  * number of electrons:',eltot
        write(6,*)'  * nuclear charge:',z
        write(6,*)'  * ionic charge:',ion
        stop
      end if

C     initialize radial spinor related vectors
C     ========================================
      do ii = 1,norb
C       number of non-zero grid point for each radial spinor
        nmax(ii) = np
C       nonrelativistic-like angular quantum number for each spinor
        nql(ii) = iabs(nk(ii))
        if (nk(ii).lt.0) nql(ii) = nql(ii)-1
C       sign of small component function at origin for each radial spinor
        dq1(ii) = dble(nk(ii)/iabs(nk(ii)))
        if (mod(nqn(ii)-nql(ii),2).eq.0) dq1(ii) = -dq1(ii)
C       leading power for expansion of radial spinors at origin
C       for point nucleus model
        if (nucmod.eq.0) dfl(ii) = dsqrt(dble(nk(ii)*nk(ii))-dval)
C       for finite-size nucleus model
        if (nucmod.eq.1) dfl(ii) = dabs(dble(nk(ii)))
C       label of each radial spinor
        titre(ii) = ttire(nql(ii)+iabs(nk(ii)))
      end do

C     verify input information about quantum and occupation numbers of each spinor
C     ============================================================================
      do ii = 1,norb
        if (nql(ii).ge.nqn(ii)) then
          write(6,*)' ERROR: wrong input data'
          write(6,*)'  * l > n for spinor #',ii
          stop
        else if (nel(ii).gt.dble(2*iabs(nk(ii)))) then
          write(6,*)' ERROR: wrong input data'
          write(6,*)'  * too many electrons in spinor #',ii
          stop
        else if (nqn(ii).le.0) then
          write(6,*)' ERROR: wrong input data'
          write(6,*)'  * n smaller than 1 for spinor #',ii
          stop
        else if (nql(ii).gt.4) then
          write(6,*)' ERROR: wrong input data'
          write(6,*)'  * too high angular momentum for shell #',ii
          stop
        end if
        do jj = 1,ii-1
          if (nqn(ii).eq.nqn(jj)) then
            if (nk(ii).eq.nk(jj)) then
              write(6,*)' ERROR: wrong input data'
              write(6,*)'  * spinors: ',ii,' and ',jj,
     &        ' have the same quantum numbers'
              stop
            end if
          end if
        end do
      end do
               

C     format cards
C     ============
10    format(a)
11    format(a,16x,i6)
12    format(a,f12.8)
13    format(a,4x,d8.2)
      return
      end
C***********************************************************************
C***********************************************************************

