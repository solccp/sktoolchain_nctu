C***********************************************************************
C***********************************************************************
      subroutine potsl(dv,d,dp,dq,dr,dvn,dpas,z,np,ion,conf_w,
     &                 conf_a,conf_r,iter,rn,nucmod,fxcname)
C***********************************************************************
C
C     this routine integrates the potential using a 4-points integration method
C
C     on exit: dv  => total effective potential
C              dvn => Coulomb potential
C              dp  => Coulomb + nuclear potential
C              dq  => Coulomb + nuclear + xc potential
C      
C     dv     - potential
C     d      - density
C     dp     - working space 
C     dq     - working space 
C     dvn    - working space 
C     dr     - grid
C     dpas   - integration step (exponential)
C     z      - atomic number
C     np     - number of points on grid
C     ion    - atomic total charge
C     iter   - number of current iteration
C     rn     - radius of the nucleus in the finite-size nucleus model
C     nucmod - flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
C     fxcname - name of the XC potential to be used in the calculations
C
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer z,np,ion,iter,nucmod,ii,kk

      real*8 dpas,das,dlo,dlo2,conf_w,conf_a,conf_r,rn,nucpot
      real*8 dv(maxgrid),dr(maxgrid),dp(maxgrid),d(maxgrid)
      real*8 d0(maxgrid),d1(maxgrid),d2(maxgrid)
      real*8 dvn(maxgrid),dq(maxgrid)
      real*8 vxc,exc
      character*20 fxcnamelocal,fxcname

C     initialize constants
C     ====================
      das  = dpas/24.0d0
      dlo  = dexp(dpas)
      dlo2 = dlo*dlo

C     compute integrand for obtaining Coulomb potential
C     =================================================
      do ii = 1,np
        dv(ii) = d(ii)*dr(ii)
      end do

C     boundary conditions for integration
C     ===================================
      dp(2) = dr(1)*(d(2)-d(1)*dlo2)/(12.0d0*(dlo-1.0d0))
      dp(1) = dv(1)/3.0d0 - dp(2)/dlo2
      dp(2) = dv(2)/3.0d0 - dp(2)*dlo2
      
C     compute Coulomb potential
C     =========================
      do ii = 3,np-1
        dp(ii) = dp(ii-1) + das*(13.0d0*
     &  (dv(ii)+dv(ii-1))-(dv(ii-2)+dv(ii+1)))
      end do
      dp(np)   = dp(np-1)
      dv(np-1) = dp(np-1)
      dv(np)   = dp(np-1)
      do kk = np-2,2,-1
        dv(kk) = dv(kk+1)/dlo
     &  + das*(13.0d0*(dp(kk)+dp(kk+1)/dlo)
     &  - (dp(kk+2)/dlo2+dp(kk-1)*dlo))
      end do
      dv(1) = dv(3)/dlo2 
     &+ dpas*(dp(1)+4.0d0*dp(2)/dlo+dp(3)/dlo2)/3.0d0

C     save Coulomb potential for calculating expectation values
C     =========================================================
      do ii = 1,np
        dvn(ii) = dv(ii)/dr(ii)
        dv(ii)  = dvn(ii)
      end do

C     transform density to radial density
C     ===================================
      do ii = 1,np
        d0(ii) = d(ii)/(4*pi*dr(ii)*dr(ii))  
      end do

C     if GGA, compute first and second derivatives of density
C     =======================================================
      fxcnamelocal='lda'
      if (iter.gt.10) then
        fxcnamelocal=fxcname
        call diffrho(d0,maxgrid,d1,d2,dr,dpas)
      end if

	print*,iter
        write(*,'(3a)')"(",fxcname,")"
        write(*,'(3a)')"(",fxcnamelocal,")"


C     compute nuclear potential
C     =========================
      do ii = 1,np
        dv(ii) = dv(ii) + nucpot(dr(ii),z,rn,nucmod)
        dp(ii) = dv(ii)
      end do

C     compute exchange-correlation potential
C     ======================================
      do ii = 1,np
        call xcpotential(fxcnamelocal,d0(ii),d1(ii),d2(ii),
     &                   dr(ii),exc,vxc)
        dv(ii) = dv(ii) + vxc
        dq(ii) = dv(ii)
      end do

C     compute confining Woods-Saxon potential
C     =======================================
      do ii = 1,np
        dv(ii) = dv(ii) + conf_w/(1.0d0+exp(-conf_a*(dr(ii)-conf_r)))
      end do

      return
      end
C***********************************************************************
C***********************************************************************

