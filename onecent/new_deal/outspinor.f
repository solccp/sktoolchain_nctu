      subroutine outspinor(norb,np,nql,nqn,nk,den,nel,dr,dpc,dqc,titre)
      implicit none
      include 'maxima.h'

      integer norb,ii,np,jj
      integer nk(maxsh),nqn(maxsh),nql(maxsh)

      real*8 aa,bb,ab,nel(maxsh),den(maxsh),dr(maxgrid)
      real*8 dpc(maxgrid,maxsh),dqc(maxgrid,maxsh)

      character*4 titre(maxsh)

C     output calculated radial spinors to file
C     ========================================
      open(7,file='spinors')
      write(7,10000)norb
      do ii = 1,norb
        write(7,10001)ii,nqn(ii),titre(ii),nqn(ii),nql(ii),
     &  nk(ii),den(ii)
        do jj = 1,np
C          aa = dpc(jj,ii)/(2*dsqrt(pi)*dr(jj))
C          bb = dqc(jj,ii)/(2*dsqrt(pi)*dr(jj))
          aa = dpc(jj,ii)
          bb = dqc(jj,ii)
          ab = aa*aa + bb*bb
          write(7,'(5d25.16)')dr(jj),aa,bb,ab,nel(ii)*ab
        end do
      end do
      close(7)

C     format section
C     ==============
10000 format('# ',I3,' spinor radial wave functions'/
     &'      normalization: I(P*P+Q*Q)dr = 1'/'      if you need ',
     &'real radial wave functions divide P and Q at each',
     &' point by 2*sqrt(pi)*r'///'         distance              ',
     &'large component P        small component Q           ',
     &' P*P+Q*Q             electron density'/
     &11x,'||',25x,'||',23x,'||',23x,'||',22x,'||'/
     &10x,'_||_',23x,'_||_',21x,'_||_',21x,'_||_',20x,'_||_'/
     &10x,'\  /',23x,'\  /',21x,'\  /',21x,'\  /',20x,'\  /'/
     &11x,'\/',25x,'\/',23x,'\/',23x,'\/',22x,'\/'/)
10001 format(/'# spinor shell',i3,':',2x,i2,1x,a4,'  with  N =',i3,
     &'  L =',i3,'  K =',i3,'   and energy',f16.8)

      return
      end
