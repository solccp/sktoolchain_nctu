!***********************************************************************
!***********************************************************************
program atom
  !***********************************************************************
  !
  !     Program to solve many-electron atomic Dirac problem; a numerical
  !     integration scheme is used on a logarithmic grid; based on 
  !     relativistic atomic code by J.P. DESCLAUX (CEA PARIS 1969);
  !     final radial wavefunctions, density and potential are saved on grid
  !
  !***********************************************************************
  implicit none
  include 'maxima.h'
  real*8 zero
  parameter (zero=0.0d0)
  integer maxtotal
  parameter (maxtotal=maxgrid*maxsh)
  integer norb,z,nes,np,ion,niter,nucmod,iter,ii,jj
  integer i,j,ll,im,ip,filout,filin
  integer nk(maxsh),nmax(maxsh),nqn(maxsh),nql(maxsh)
  real*8 econv,vmax,emax,ymax,de,val,dval,aa,bb,ab,rn,dvc,ddot
  real*8 dpas,dcop,test,teste,testy,testv,conf_w,conf_a,conf_r
  real*8 nel(maxsh),den(maxsh),dq1(maxsh),dfl(maxsh)

!  real*8 dv(maxgrid),dr(maxgrid),dp(maxgrid),dq(maxgrid)
!  real*8 d(maxgrid),dc(maxgrid),dvn(maxgrid)
!  real*8 dpc(maxgrid,maxsh),dqc(maxgrid,maxsh)

  double precision, dimension(:), allocatable :: dv, dr, dp, dq
  double precision, dimension(:), allocatable :: d, dc, dvn
  double precision, dimension(:, :), allocatable :: dpc, dqc

  !     first point of the grid
  double precision, parameter :: grid0 = 2.0d-5


  logical conv
  character*20 fxcname
  character*4 titre(maxsh)
  !data dpc /maxtotal*0.0d0/
  !data dqc /maxtotal*0.0d0/

  double precision :: fpot

  !     open all files
  !     ==============
  filin = 5
  filout = 6
  open(filin,file='input')
  open(filout,file='output')

  !     read input data
  !     ===============
  call insld(den,dq1,dfl,nqn,nql,nk,nmax,nel,titre,norb, &
             dpas,z,nes,np,dcop,test,teste,testy,testv,niter,ion,&
             conf_w,conf_a,conf_r,rn,nucmod,dvc,fxcname)



  allocate(dv(np), dr(np), dp(np), dq(np))
  allocate(d(np), dc(np), dvn(np))
  allocate(dpc(np, norb), dqc(np, norb))

  dpc = 0
  dqc = 0


!     define integration grid
!     =======================

  dr(1) = grid0
  do ii = 2,np
     dr(ii) = dr(1)*dexp((ii-1)*dpas)
  end do

  !     compute starting Thomas-Fermi potential (no confinement)
  !     ========================================================
  do ii = 1,np
     dv(ii) = fpot(dr(ii),z,-ion-1,rn,nucmod)
  end do


  !     starts solving the atomic DFT problem
  !     =====================================
  write(6,10000)

  conv = .false.

  !     start SCF iterations
  !     ====================
  do iter = 1,niter

     !     initialize SCF convergence indicators
     !     =====================================
     d = 0
     econv = test
     ymax = zero
     vmax = zero
     emax = zero

     !     solving radial Dirac equation for each radial spinor
     !     ====================================================
     do jj = 1,norb
        de = den(jj)
        call resld(nqn(jj),nql(jj),nk(jj),nmax(jj),den(jj),dfl(jj),&
                  dq1(jj),jj,dv,dr,dp,dq,dpas,z,nes,econv,np, &
                  conf_w,rn,nucmod,dvc)

        !     test change of spinor energy between last 2 iterations
        !     ======================================================
        val = dabs((den(jj)-de)/de)  
        if (val.gt.emax) emax = val

        !     test change of |spinor wave function| between last 2 iterations
        !     ===============================================================
        do ii = 1,np
           val = dpc(ii,jj) - dp(ii)
           if (dabs(dp(ii)).gt.1.0d0) val = val/dp(ii)
           if (dabs(val).ge.dabs(ymax)) then
              ymax = val
           end if
           val = dqc(ii,jj) - dq(ii)
           if (dabs(dq(ii)).gt.1.0d0) val = val/dq(ii)
           if (dabs(val).ge.dabs(ymax)) then
              ymax = val
           end if

           !     update radial spinors
           !     =====================
           dpc(ii,jj)=dp(ii)
           dqc(ii,jj)=dq(ii)

           !     compute contribution from spinor jj to total electron density
           !     =============================================================
           d(ii) = d(ii) + nel(jj)*(dp(ii)*dp(ii)+dq(ii)*dq(ii))
        end do
     end do

     !     evaluate new potential
     !     ======================
     call potsl(dc,d,dp,dq,dr,dvn,dpas,z,np,ion,conf_w,&
            conf_a,conf_r,iter,rn,nucmod,fxcname)

     !     find largest discrepancy between the old and the new potential
     !     ==============================================================
     do ii = 20,np
        dval = dabs(dc(ii)-dv(ii))
        if (dr(ii)*dc(ii).le.-ion-1) dval = -dval/dc(ii)
        if (dval.gt.vmax) then
           vmax = dval
           jj = ii
        end if
     end do

     !     print convergence information in this iteration
     !     ===============================================
     write(6,10007)iter,vmax,ymax,emax,dr(jj),dc(jj)

     !     check convergence criteria
     !     ==========================
     conv = econv.le.test .and. emax.le.teste .and. vmax.le.testv &
          .and. ymax.le.testy
     if (conv) then
        exit
     end if

     !     average old and new potentials with coefficient dcop
     !     ====================================================
     do ii = 1,np
        dv(ii) = (1.0d0-dcop)*dv(ii) + dcop*dc(ii)
     end do

     !     finish SCF iterations
     !     =====================
  end do

  !     SCF convergence message
  !     =======================
  if (.not.conv) then
     write(6,10002)niter
     !        stop
  end if
  write(6,10003)

  !     print occupations and energies of converged spinor shells
  !     =========================================================
  write(6,10001)
  do ii = 1,norb
     write(6,10006)nqn(ii),titre(ii),nel(ii),den(ii)
  end do

  !     compute overlap integrals
  !     =========================
  write(6,10004)
  call overlap(6,norb,dpas,nk,nql,nqn,nmax,dfl,dr,dp,dq,dpc, &
               dqc,titre)

  !     calculate various energy related expectation values
  !     ===================================================
  dval = ddot(norb,nel,1,den,1)
  call totale(dval,z,dr,dp,dq,np,d,dpas,dvn,dc,dv,rn,nucmod,&
              fxcname)
  write(6,10005)dval,dv(4),dv(3),dv(2),dv(1)

  !     save computed and renormalized radial spinor shells to file
  !     ===========================================================
  !      call outspinor(norb,np,nql,nqn,nk,den,nel,dr,dpc,dqc,titre)

  !     compute final atomic density
  !     ============================
  d = 0
  do ii = 1,norb
     do ip = 1,np
        d(ip) = d(ip)&
            + nel(ii)*(dpc(ip,ii)*dpc(ip,ii)+dqc(ip,ii)*dqc(ip,ii))
     end do
  end do

  !     compute final potentials
  !     dv->total, dp->nucl+coul, dq->nucl+coul+xc, dvn->Coulomb
  !     ========================================================
  call potsl(dv,d,dp,dq,dr,dvn,dpas,z,np,ion,conf_w,&
             conf_a,conf_r,iter,rn,nucmod,fxcname)

  !     spline fitting of potentials
  !     ============================
  call spline3(np,dr,dp,dpas,'potnc')
  call spline3(np,dr,dq,dpas,'pottot')

  !     transform final density into radial density
  !     ===========================================
  do ii = 1,np
     d(ii) = d(ii)/(4*pi*dr(ii)*dr(ii))
  end do

  !     spline fitting of radial density
  !     ================================
  call spline3(np,dr,d,dpas,'dens')

  !     compute scalar relativistic valence orbitals
  !     ============================================
  call nonrel(norb,np,nqn,nql,nk,dr,dpc,nel,den,dpas)

  !     format section
  !     ==============
10000 format('   * start of self-consistent DFT procedure'//&
  '                potential      wave function     spinor energy',&
  '     largest potential change this iteration'/&
  '  iteration    convergence      convergence       convergence',&
  '         at distance       currrent value'/&
  ' ===========  =============    =============     =============',&
  '       =============     ================')
10001 format(/'  * atom electronic structure and final spinor ',&
  'energies:'//'    spinor type     occupation       final energy'/&
  '  ==============  ==============  ==================')
10002 format(' ERROR: your calculations has not converged ',&
  'in ',I3,' iterations'/'   * you may want to increase',&
  ' the number of iterations in the input file')
10003 format(/'  * SCF procedure has converged'/)
10004 format(//'  * radial overlap integrals for spinors'//&
  '     spinor 1        spinor 2            overlap integral'/&
  '    ==========      ==========          ==================')
10005 format(/'  * energy related expectation values:'//&
  '                    total energy: ',f17.8/&
  '                  kinetic energy: ',f17.8/&
  '                  Coulomb energy: ',f17.8/&
  '                 exchange energy: ',f17.8/&
  '       nuclear attraction energy: ',f17.8)
10006 format(7x,i1,1x,a4,10x,f5.2,2x,f20.5)
10007 format(I7,3(1PE18.4),2(1PE20.6))

end program atom
!***********************************************************************
!***********************************************************************
