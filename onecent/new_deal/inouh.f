C***********************************************************************
C***********************************************************************
      subroutine inouh(dp,dq,dr,dq1,dfl,dv,z,test,jc,dep,deq,
     &                 dd,c,dk,rn,nucmod)
C***********************************************************************
C
C     this routine finds initial conditions (5 initial points of dp and dq
C     and 5 initial points of dep and deq) for the outward integration
C     for the spinor shell jc using a finite-size (nucmod=1) or a point
C     (nucmod=0) model of the nucleus
C
C     dp     - large component
C     dq     - small component
C     dr     - integration grid
C     dq1    - sign of small component Q at r=0
C     dfl    - gamma factor = sqrt(dk*dk - z*z/c*c)
C     dv     - potential
C     z      - atomic number
C     test   - energy convergence criterion
C     dep    - derivatives of dp at 5 first points
C     deq    - derivatives of dq at 5 first points
C     dd     - spinor energy
C     c      - speed of light
C     dk     - kappa quantum number
C     rn     - radius of finite-size nucleus
C     nucmod - flag for point (0) or finite-size (1) nucleus 
C
C***********************************************************************
      implicit none
      include 'maxima.h'
      integer i,m,jc,nucmod,z,dk
      real*8 test,c,dfl,dm,rr,dd,dq1,pot,rn,s,a,b
      real*8 dp(5),dq(5),dep(5),deq(5),dv(maxgrid)
      real*8 pc(0:20),qc(0:20),two,one
      real*8 dr(maxgrid)
	real*8 ap,bp
      data one,two/1.0d0,2.0d0/

C     initialize wave function for point nucleus model
C     ================================================
      if (nucmod.eq.0) then

C       initialize q0 for small component as +1 or -1
C       (choice of sign yields P>0 for large r)
C       ***************************************
        qc(0) = dq1

C       initialize p0 for large component
C       *********************************
        if(dk.gt.0) pc(0) = dq1*(dble(z)/c)/(dble(dk)+dfl)
        if(dk.lt.0) pc(0) = dq1*(c/dble(z))*(dble(dk)-dfl)

C       calculate first 20 expansion coefficients for P and Q
C       *****************************************************
        pot = dv(1)/c + (dble(z)/dr(1))/c - dd/c - c
        do i = 1,20
          dm = dfl + dble(i)
          pc(i) = (dm-dble(dk))*(c-pot)*qc(i-1) 
     &          + (dble(z)/c)*(c+pot)*pc(i-1)
          qc(i) = (dm+dble(dk))*(c+pot)*pc(i-1) 
     &          - (dble(z)/c)*(c-pot)*qc(i-1)
          pc(i) = pc(i)/(dm*dm - dble(dk*dk) + dble(z*z)/(c*c))
          qc(i) = qc(i)/(dm*dm - dble(dk*dk) + dble(z*z)/(c*c))
        end do

C       initialize P, P', Q, Q' at first 5 points of mesh
C       *************************************************
        do m = 1,5
          dp(m) = 0.0d0
          dq(m) = 0.0d0
          dep(m) = 0.0d0
          deq(m) = 0.0d0
        end do

C       calculate value of P, P', Q, Q' at first 5 points of mesh
C       *********************************************************
        do m = 1,5
          rr = one
          do i = 0,20
            if (i.ne.0) rr = rr*dr(m)
            dp(m) = dp(m) + pc(i)*rr
            dq(m) = dq(m) + qc(i)*rr
            dep(m) = dep(m) + (dble(i)+dfl)*pc(i)*rr
            deq(m) = deq(m) + (dble(i)+dfl)*qc(i)*rr
          end do
        end do

C       check convergence of the expansion
C       **********************************
        if (abs(pc(20)*rr/dp(5)).gt.test .or. 
     &  abs(qc(20)*rr/dq(5)).gt.test) then
          write(6,*)' ERROR: could not find initial conditions',
     &    ' for outward integration for shell ',jc
          write(6,*)'   * polynomial expansion did not converged'
          stop
        end if

C     initialize wave function for finite-size nucleus model
C     ======================================================
      else

C       fit potential at origin to a parabola (first 10 points)
C       ===================================== =================
C	bp = (dv(20)-dv(1))/(dr(20)-dr(1))/(dr(20)+dr(1))
C	ap = 0.5d0*(dv(20)+dv(1)-bp*(dr(20)*dr(20)+dr(1)*dr(1)))

C       initialize p0 and q0 choosing a sign that yields P>0 for large r
C       ****************************************************************
        if(dk.gt.0)then
          pc(0) = 0.0d0
          qc(0) = dq1
        else
          pc(0) = -dq1
          qc(0) = 0.0d0
        end if

C       calculate auxiliary quantities
C       ******************************
        s = 0.5d0*dble(z)/(c*rn*rn*rn)
        b = dd/c + 1.5d0*dble(z)/(rn*c)
C	s = - bp/c
C	b = (dd-ap)/c
        a = 2*c + b

C       calculate p1, q1, p2, and q2
C       ****************************
        do i = 1,2
          pc(i) =  a*qc(i-1)/(dble(i+dk+abs(dk)))
          qc(i) = -b*pc(i-1)/(dble(i-dk+abs(dk)))
        end do

C       calculate first 20 expansion coefficients for P and Q
C       *****************************************************
        do i = 3,20
          pc(i) = ( a*qc(i-1) - s*qc(i-3))/(dble(i+dk+abs(dk)))
          qc(i) = (-b*pc(i-1) + s*pc(i-3))/(dble(i-dk+abs(dk)))
        end do

C       initialize P, P', Q, Q' at first 5 points of mesh
C       *************************************************
        do m = 1,5
          dp(m) = 0.0d0
          dq(m) = 0.0d0
          dep(m) = 0.0d0
          deq(m) = 0.0d0
        end do

C       calculate value of P, P', Q, Q' at first 5 points of mesh
C       *********************************************************
        do m = 1,5
          rr = one
          do i = 0,20
            if (i.ne.0) rr = rr*dr(m)
            dp(m) = dp(m) + pc(i)*rr
            dq(m) = dq(m) + qc(i)*rr
            dep(m) = dep(m) + (dble(i)+dfl)*pc(i)*rr
            deq(m) = deq(m) + (dble(i)+dfl)*qc(i)*rr
          end do
        end do

C       check convergence of the expansion
C       **********************************
        if (abs(pc(20)*rr/dp(5)).gt.test .or. 
     &     abs(qc(20)*rr/dq(5)).gt.test) then
           write(6,*)' ERROR: could not find initial conditions',
     &     ' for outward integration for shell ',jc
            write(6,*)'   * polynomial expansion did not converged'
            stop
        end if
        end if
C
      return
      end
C***********************************************************************
C***********************************************************************

