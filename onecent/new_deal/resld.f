C***********************************************************************
C***********************************************************************
      subroutine resld(nqn,nql,nk,imax,de,dfl,dq1,jc,dv,dr,dp,dq,dpas,
     &                z,nes,test,np,conf_w,rn,nucmod,dvc)
C***********************************************************************
C
C     solution of radial Dirac DFT equation for spinor jc
C
C     note that derivatives of P and Q are taken with respect
C     to (ln r) and not with respect to (r)
C
C     nqn    - principal quantum number of spinor jc
C     nql    - orbital quantum number of spinor jc
C     nk     - kappa quantum number of spinor jc
C     imax   - last grid point for tabulating the wave function
C     de     - spinor energy
C     dfl    - the leading power of spinor jc expansion at origin
C     dq1    - sign of expansion of the small component of spinor jc at origin
C     jc     - label of current spinor (1,2,3,...)
C     dv     - potential
C     dr     - integration grid
C     dp     - large component spinor radial wave function
C     dq     - small component spinor radial wave function
C     dpas   - logarithmic distance between grid points
C     z      - atomic number
C     nes    - maximal number of spinor energy adjustments
C     test   - ???????????????????????
C     np     - number of grid points
C     conf_w - height of the Woods-Saxon confining potential
C     rn     - radius of the nucleus in the finite-size nucleus model
C     nucmod - flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
C     dvc    - speed of light
C
C***********************************************************************
      implicit none
      include 'maxima.h'
      real*8 zero
      parameter (zero=0.0d0)

      integer nqn,nql,nk,imax,nucmod,jc,z,nes,np,lll,ilim
      integer ii,k,imat,ibreak,jj,noeud,imm,nd,ies,iconv

      real*8 de,dfl,dq1,dpas,test,conf_w,rn,dvc,elim,val,dval,dbe,dsum
      real*8 dv(maxgrid),dr(maxgrid),dp(maxgrid),dq(maxgrid)
      real*8 dep(5),deq(5),dpm,dqm,dm,dd,dpq,aa

C     initialize variables and constants
C     ==================================
      imm=0
      nd=0
      noeud=nqn-nql

C     find the minimum of effective radial potential
C     ==============================================
      lll = (nql*(nql+1))/2
      if (lll.eq.0) then
        elim = -dble(z*z)/(1.5d0*dble(nqn*nqn))
        ilim = 1
      else
        elim = dv(1) + dble(lll)/(dr(1)*dr(1))
        ilim = 1
        do ii = 2,np
          val = dv(ii) + dble(lll)/(dr(ii)*dr(ii))
          if (val.le.elim) then
            elim = val
            ilim = ii
          end if
        end do
      end if

C     check if potential is bounded
C     =============================
      if (elim.ge.dv(np)+dble(lll)/(dr(np)*dr(np))) then
        write(6,*)' ERROR: potential is not bounded'
        stop
      end if

C     assure that starting spinor energy is above the potential minimum
C     =================================================================
      if (de.le.elim) then
        de = dv(np) + dble(lll)/(dr(np)*dr(np))
	de = (de+elim)/2.0d0
      end if

C     start iterative procedure of finding de, dp, and dq for spinor jc
C     =================================================================
100   iconv = 0
      do ies = 1,nes
        if (iconv.eq.0) then

C         find the outer classical turning point (imat)
C         =============================================
          if (imm.ne.1) then
            ibreak = 0
            do ii = ilim,np,2
              if (ibreak.eq.0) then
                imat = ii
                val = dv(imat) + dble(lll)/(dr(imat)*dr(imat)) - de
                if (val.ge.zero) then
                  ibreak=1
                end if
              end if
            end do
            if (imat.le.5) then
              de = de/2
              if (de.lt.-test .and. nd.le.noeud) goto 100
              write(6,*)' ERROR: could not find classical turning',
     &        ' point for spinor shell ',jc
              stop
            end if    
          end if    

C         find series expansion of P & Q at origin for outward integration
C         ================================================================
          call inouh(dp,dq,dr,dq1,dfl,dv,z,test,jc,dep,deq,de,
     &               dvc,nk,rn,nucmod)

C         compute number of radial nodes at first 5 points of grid
C         ========================================================
          nd = 1
          do ii = 2,5
            if (dabs(dp(ii-1)).ge.1.0d-20) then
              if ((dp(ii)/dp(ii-1)).le.zero) then
                nd = nd + 1
              end if
            end if
          end do

C         multiply series expansion of P & Q at origin by prefactor r^dfl
C         ===============================================================
          do ii = 1,5
            val = dr(ii)**dfl
            dp(ii) = dp(ii)*val
            dq(ii) = dq(ii)*val
            dep(ii) = dep(ii)*val
            deq(ii) = deq(ii)*val
          end do

C         check sign of components at first grid point
C         ============================================
          k = -1 + 2*mod(noeud,2)
          if (k*dp(1).lt.zero .or. k*nk*dq(1).lt.zero) then
            write(6,*)' ERROR: wrong series expansion at origin',
     &      ' for spinor shell ',jc
            stop
          end if

C         outward integration from the sixth point to the 50th point
C         ==========================================================
          dm = dpas/720.0d0
          do ii = 6,50
            dp(ii) = dp(ii-1)
            dq(ii) = dq(ii-1)
            call inth(dp(ii),dq(ii),dv(ii),dr(ii),dep,deq,de,dvc,nk,dm)
            if (dabs(dp(ii-1)).gt.1.0d-20) then
              if ((dp(ii)/dp(ii-1)).lt.zero) then
                nd = nd + 1
              end if
            end if
          end do        

C         inward integration from 50th to 1st point to clean the wave function
C         ====================================================================
          dm = -dm
          aa = dep(5)
          dep(5) = dep(1)
          dep(1) = aa
          aa = dep(4)
          dep(4) = dep(2)
          dep(2) = aa
          aa = deq(5)
          deq(5) = deq(1)
          deq(1) = aa
          aa = deq(4)
          deq(4) = deq(2)
          deq(2) = aa
          do ii = 45,1,-1
            dp(ii) = dp(ii+1)
            dq(ii) = dq(ii+1)
            call inth(dp(ii),dq(ii),dv(ii),dr(ii),dep,deq,de,dvc,nk,dm)
          end do

C         restart outward integration from the sixth point to the classical turning point
C         ===============================================================================
          aa = dep(5)
          dep(5) = dep(1)
          dep(1) = aa
          aa = dep(4)
          dep(4) = dep(2)
          dep(2) = aa
          aa = deq(5)
          deq(5) = deq(1)
          deq(1) = aa
          aa = deq(4)
          deq(4) = deq(2)
          deq(2) = aa
          dm = dpas/720.0d0
          do ii = 6,imat
            dp(ii) = dp(ii-1)
            dq(ii) = dq(ii-1)
            call inth(dp(ii),dq(ii),dv(ii),dr(ii),dep,deq,de,dvc,nk,dm)
            if (dabs(dp(ii-1)).gt.1.0d-20) then
              if ((dp(ii)/dp(ii-1)).lt.zero) then
                nd = nd + 1
              end if
            end if
          end do        

C         check the number of nodes and rescale energy if necessary
C         =========================================================
          if (dabs(de).lt.1.0d-5) then
            de=de-0.1d0
          else if (de.lt.zero) then
            if (nd.lt.noeud) de=0.8d0*de
            if (nd.gt.noeud) de=1.2d0*de
          else
            if (nd.gt.noeud) de=0.8d0*de
            if (nd.lt.noeud) de=1.2d0*de
          end if

C         check if rescaled energy matches the limits of potential
C         ========================================================
          if (nd.ne.noeud) then
            if (de.lt.elim) then
              write(6,*)' ERROR: could not find the proper number',
     &        'of nodes for spinor shell ',jc
              write(6,*)'   * expected number of nodes:',noeud
              write(6,*)'   * found number of nodes:',nd
              stop
            end if
          end if

C         restart the iterative procedure with rescaled spinor energy
C         ===========================================================
          if (nd.ne.noeud) then
            go to 100
          end if

C         save values of P & Q at turning point from outward integration
C         ==============================================================
          dpm = dp(imat)
          dqm = dq(imat)

C         find grid point IMAX to start out inward integration
C         ====================================================
          if (imm.ne.1) then
            ibreak = 0
            do ii = 1,np,2
              if (ibreak.eq.0) then
                imax = np + 1 - ii
                val = (dv(imax)-de)*dr(imax)*dr(imax)
                if (val.le.400.0d0) ibreak=1
              end if
            end do
          end if

C         find asymptotic expansion of P & Q at large distances
C         =====================================================
          dd = dsqrt(abs(de-conf_w)*(2.0d0+(de-conf_w)/(dvc*dvc)))
          dpq = -dd/(2.0d0*dvc + (de-conf_w)/dvc)
          do ii = 1,5
            jj = imax+1-ii
            dp(jj)  =  dexp(-dd*dr(jj))
            dep(ii) = -dd*dp(jj)*dr(jj)
            dq(jj)  =  dpq*dp(jj)
            deq(ii) =  dpq*dep(ii)
          end do

C         inward integration from imax-5 to imat
C         ======================================
          dm = -dm
          do ii = imax-5,imat,-1
            dp(ii) = dp(ii+1)
            dq(ii) = dq(ii+1)
            call inth(dp(ii),dq(ii),dv(ii),dr(ii),dep,deq,de,dvc,nk,dm)
          end do

C         check the sign of the large component at imat
C         =============================================
          dval = dpm/dp(imat)
          if (dval.le.zero) then
            write(6,*)' ERROR: wrong sign of large component',
     &      ' for spinor shell ',jc
            stop
          end if

C         rescale the outer (imat:imax) part of dp and dq
C         ===============================================
          do ii = imat,imax
            dp(ii) = dp(ii)*dval
            dq(ii) = dq(ii)*dval
          end do

C         compute the norm of the spinor jc
C         =================================
          dsum = 3*dr(1)*(dp(1)*dp(1)+dq(1)*dq(1))/(dpas*(2*dfl+1.0d0))
          do ii = 3,imax,2
            dsum = dsum + dr(ii)*(dp(ii)*dp(ii)+dq(ii)*dq(ii))
     &      + 4*dr(ii-1)*(dp(ii-1)*dp(ii-1)+dq(ii-1)*dq(ii-1))
     &      + dr(ii-2)*(dp(ii-2)*dp(ii-2)+dq(ii-2)*dq(ii-2))
          end do
          dsum = dpas*(dsum+dr(imat)*(dqm*dqm-dq(imat)*dq(imat)))/3

C         modification of spinor energy
C         =============================
          dbe = dp(imat)*(dqm-dq(imat))*dvc/dsum
          val = dabs(dbe/de)
          if (val.le.0.1d0) then
            imm = 1
          else
            imm = 0
          end if

C         calculations for spinor jc converged
C         ====================================
          if (val.le.test) iconv=1
        end if

C       readjust spinor energy and go to the next iteration
C       ===================================================
        if (iconv.eq.0) then
150       dval = de + dbe
          de = dval
          if (dval.lt.zero) then
            de = dval
          else if (ii.eq.12491023) then
            dbe = dbe/2
            val = val/2
            if (val.gt.test) then
              go to 150
            else
              write(6,*)' ERROR: matching large and small component ',
     &        'at classical turning point'
              write(6,*)'        yielded positive spinor energy'
              stop
            end if
          end if
        end if


C     the end of iteration
C     ====================
      end do

C     calculations did not converge
C     =============================
      if (iconv.eq.0) then
        write(6,*)' ERROR: iterative procedure of spinor energy ',
     &  'adjustment did not converged for spinor shell ',jc
        write(6,*)'   * try changing the number of trials in the ',
     &  'input file'
        write(6,*)'   * current number of trials is ',nes,
     &  ' (default=15)'
        stop
      end if

C     renormalize both components
C     ===========================
200   dsum = dsqrt(dsum)
      dq1 = dq1/dsum
      do ii = 1,imax
        dp(ii) = dp(ii)/dsum
        dq(ii) = dq(ii)/dsum
      end do
      do ii = imax+1,np
        dp(ii) = zero
        dq(ii) = zero
      end do
C
      return
      end
C***********************************************************************
C***********************************************************************

