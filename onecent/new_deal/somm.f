C***********************************************************************
C***********************************************************************
      subroutine somm(dr,dp,dq,dpas,da,m,np)
C***********************************************************************
C
C     compute m'th moment of a given spinor shell
C     integration of (dp+dq)*(dr^m)  using the Simpson method 
C     over all the grid points
C
C     dr - grid
C     dp - large component
C     dq - small component
C     dpas - integration step (exponential)
C     da - value of the computed moment
C     m - exponent for the calculated moment
C     np - number of points on the grid
C
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer m,np,ii

      real*8 d1,db,dc,dl
      real*8 dr(maxgrid),dp(maxgrid),dq(maxgrid),dpas,da
C

      d1 = da + m + 1
      da = 0.0d0
      db = 0.0d0
      do ii = 1,np
        dl = dr(ii)**dble(m+1)
        if (ii.ne.1 .and. ii.ne.np) then
          dl = 2.0d0*dl
          if (mod(ii,2).eq.0) dl = 2.0d0*dl
        end if
        dc = dp(ii)*dl
        if (dc.lt.0.0d0) then
          db = db + dc
        else if (dc.gt.0.0d0) then
          da = da + dc
        end if
        dc = dq(ii)*dl
        if (dc.lt.0.0d0) then
          db = db + dc
        else if (dc.gt.0.0d0) then
          da = da + dc
        end if
      end do
      da = dpas*(da+db)/3.0d0
      dc = dexp(dpas) - 1.0d0
      db = d1*(d1+1.0d0)*dc*dexp(dpas*(d1-1.0d0))
      db = dr(1)*(dr(2)**dble(m))/db
      dc = (dr(1)**dble(m+1))*(1.0d0+1.0d0/(dc*(d1+1.0d0)))/d1
      da = da + dc*(dp(1)+dq(1)) - db*(dp(2)+dq(2))
C
      return
      end
