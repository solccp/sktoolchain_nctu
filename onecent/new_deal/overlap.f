      subroutine overlap(filout,norb,dpas,nk,nql,nqn,nmax,dfl,
     &                   dr,dp,dq,dpc,dqc,titre)
      implicit none
      include 'maxima.h'

      integer norb,ii,jj,filout,im,ll
      integer nk(maxsh),nql(maxsh),nmax(maxsh),nqn(maxsh)
      real*8 dr(maxgrid),dp(maxgrid),dq(maxgrid),dval,dpas
      real*8 dpc(maxgrid,maxsh),dqc(maxgrid,maxsh),dfl(maxsh)
      character*4 titre(maxsh)

C     compute overlap integrals between two radial spinors
C     ====================================================
      do ii = 1,norb
        do jj = 1,ii-1
          if (nql(ii).eq.nql(jj)) then
            if (nk(ii).eq.nk(jj)) then
              im = min(nmax(jj),nmax(ii))
              do ll = 1,im
                dq(ll) = dqc(ll,ii)*dqc(ll,jj)
                dp(ll) = dpc(ll,ii)*dpc(ll,jj)
              end do
              dval = dfl(ii) + dfl(jj)
              call somm(dr,dp,dq,dpas,dval,0,im)
              write(filout,'(6X,I1,1X,A4,8x,I3,1x,A4,3x,F25.12)')
     &        nqn(jj),titre(jj),nqn(ii),titre(ii),dval
            end if
          end if
        end do
      end do

      return
      end

