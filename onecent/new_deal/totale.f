C***********************************************************************
C***********************************************************************
      subroutine totale(dval,z,dr,dp,dq,np,d,dpas,dvn,dvf,dc,rn,
     &                  nucmod,fxcname)
C***********************************************************************
C
C      compute energy expectation values over various operators
C
C      dval:  (input)  bandstructure energy, on output total energy
C      z:     (input)  nuclear charge
C      dr:    (input)  integration grid
C      dp:    (scratch)
C      dq:    (scratch)
C      np:    (input)  number of grid points
C      d:     (input)  total density at each grid point, watch out for 4pi*r 
C      dpas:  (input)  step width on mesh, dr(ii)=dr(1)*dexp((ii-1)*dpas)
C      dvn:   (input)  coulomb potential eventually multiplied with 1/r
C      dvf:   (input)  total potential
C      dc:    (output) energies
C                        dc(4) kinetic energy
C                        dc(3) Coulomb energy
C                        dc(2) exchange energy
C                        dc(1) nuclear attraction energy
C      fxcname: (input) name of the XC potential to be used in calculations
C      rn:    (input)  radius of the nucleus in the finite-size nucleus model
C      nucmod:(input)  flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
C
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer z,np,ii,nucmod

      real*8 dval,dpas,exchee,nucpot,rn,exc,vxc(2),exc1,vxc1
      real*8 d0(maxgrid),d1(maxgrid),d2(maxgrid)
      real*8 dr(maxgrid),dp(maxgrid),dq(maxgrid)
      real*8 dvn(maxgrid),dvf(maxgrid),dc(maxgrid),d(maxgrid)
      character*20 fxcname

C     compute integral dc(1) over nuclear repulsion potential
C     =======================================================
      if (nucmod.eq.0) dc(1) = 1.0d0
      if (nucmod.eq.1) dc(1) = 4.0d0
      do ii = 1,np
        dp(ii) = d(ii)*nucpot(dr(ii),z,rn,nucmod)
      end do
      call somm(dr,dp,dq,dpas,dc(1),0,np)

C     compute integral dc(3) over total effective potential
C     =====================================================
      if (nucmod.eq.0) dc(3) = 1.0d0
      if (nucmod.eq.1) dc(3) = 4.0d0
      do ii = 1,np
        dp(ii)  = d(ii)*dvf(ii)
      end do
      call somm(dr,dp,dq,dpas,dc(3),0,np)

C     compute integral dc(5) over Coulomb potential
C     =============================================
      dc(5) = 2.0d0      
      do ii = 1,np
        dvn(ii) = d(ii)*dvn(ii)
      end do
      call somm(dr,dvn,dq,dpas,dc(5),0,np)

C     compute integral dc(2) over exchange-correlation density
C     ========================================================
      do ii = 1,np
        d0(ii) = d(ii) / (4.0d0 * pi * dr(ii) * dr(ii))  
      end do
      call diffrho(d0,np,d1,d2,dr,dpas)
      do ii = 1,np
        call xcpotential(fxcname,d0(ii),d1(ii),d2(ii),dr(ii),exc1,vxc1)
	d0(ii) = exc1 * 4.0d0 * pi * dr(ii) * dr(ii)
      end do
      dc(2)=2.0d0      
      call somm(dr,d0,dq,dpas,dc(2),0,np)

C     compute kinetic energy integral dc(4) as a difference between band
C     structure energy and the integral over the total effective potential
C     ====================================================================
      dc(4) = dval - dc(3)

C     compute the integral over the exchange-correlation potential
C     ============================================================
      vxc(1) = dc(3) - dc(1) - dc(5) - dc(2)

C     compute total atomic energy by subtracting the double counting 
C     terms from the band structure energy
C     ====================================
      dval = dval - 0.5d0*dc(5) - vxc(1)
      dc(3) = 0.5d0*dc(5)
C
      return
      end
C***********************************************************************
C***********************************************************************
