C***********************************************************************
C***********************************************************************
      subroutine inth(P,Q,v,r,P1,Q1,e,c,k,h)
C***********************************************************************
C
C     written by Henryk Witek, NCTU, 2007 
C     (based on Desclaux's Dirac-Slater program)
C
C***********************************************************************
C
C     this routine performs integration of Dirac radial equation 
C     (for small and large components P and Q) using 5 points 
C     predictor-corrector method of Adams (Adams-Bashfort predictor and 
C     Adams-Moulton corrector);
C     details of the approach can be found in:
C       * "Relativistic atomic wave functions" by J.P.Desclaux, 
C          D.F.Mayers, and F.O'Brien, J.Phys.B 4, 631 (1971)
C       * "Introduction to Numerical Analysis" 
C          by F.B.Hildebrand,McGraw-Hill,NY,1965
C       * "Introduction to Numerical Analysis" by J.Stoer 
C          and R. Burlich, chapter 7.2.6, Springer, NY,1980
C       * "Electron scattering in solid matter" by J.Zabloudil,
C          R.Hammerling, L.Szunyogh, and P.Weinberger,
C          Springer,Berlin,2005
C
C***********************************************************************
C
C     Description of input parameters
C
C     P  - large component at distance r
C     Q  - small component at distance r
C     v  - potential at distance r
C     r  - distance from origin
C     P1 - first derivatives of P at 5 subsequent points
C     Q1 - first derivatives of Q at 5 subsequent points
C     e  - spinor energy
C     c  - speed of light
C     k  - quantum number kappa
C     h  - integration step (dpas/720)
C
C***********************************************************************
      implicit none
      integer ii,k
      real*8 P1(5),Q1(5),P,Q,v,r,e,c,h,two,dpr,dqr
      data two /2.0d0/

C     calculate value of predictor at r
C     *********************************
      dpr = P + h*(1901.0d0*P1(5) - 2774.0d0*P1(4) + 2616.0d0*P1(3) 
     &           - 1274.0d0*P1(2) +  251.0d0*P1(1))
      dqr = Q + h*(1901.0d0*Q1(5) - 2774.0d0*Q1(4) + 2616.0d0*Q1(3) 
     &           - 1274.0d0*Q1(2) +  251.0d0*Q1(1))

C     update the vectors of first derivative of P & Q at r
C     ****************************************************
      do ii = 2,5
        P1(ii-1) = P1(ii)
        Q1(ii-1) = Q1(ii)
      end do
      P1(5) = -dble(k)*dpr + r*(two*c + (e-v)/c)*dqr
      Q1(5) =  dble(k)*dqr - r*(        (e-v)/c)*dpr

C     calculate value of corrector at r
C     *********************************
      P = P + h*(251.0d0*P1(5) + 646.0d0*P1(4) - 264.0d0*P1(3)
     &         + 106.0d0*P1(2) -  19.0d0*P1(1))
      Q = Q + h*(251.0d0*Q1(5) + 646.0d0*Q1(4) - 264.0d0*Q1(3)
     &         + 106.0d0*Q1(2) -  19.0d0*Q1(1))

C     average predictor and corrector to get P & Q at r
C     *************************************************
      P = (475.0d0*P + 27.0d0*dpr)/502.0d0
      Q = (475.0d0*Q + 27.0d0*dqr)/502.0d0

C     calculate values of first derivatives of P & Q at r
C     ***************************************************
      P1(5) = -dble(k)*P + r*(two*c + (e-v)/c)*Q
      Q1(5) =  dble(k)*Q - r*(        (e-v)/c)*P
C
      return
      end
C***********************************************************************
C***********************************************************************

