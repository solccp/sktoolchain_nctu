C***********************************************************************
C***********************************************************************
      subroutine diffrho(f,n,df,ddf,dr,dx)
C***********************************************************************
C
c   *    <subroutine diffrho> differentiates the functions           *
c   *    <f(i)> and <df(i)>the first <i> net points.                 *
c   *    <di> is the increment in the logarithmic i-net.             *
c   *                                                                *
c   *    the results are stored in <df(r)> and <ddf(r)>              *
c   *    <r> is the radial koordinate in he local r-net              *
c   *                                                                *
c   *    @U.Gerstmann                                   11.11.1998   *
C
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer j,n,ip

      real*8 dr(maxgrid),f(maxgrid),df(maxgrid),ddf(maxgrid)
      real*8 h(maxgrid),dh(maxgrid),ddh(maxgrid),dx

C     local copy of density
C     =====================
      call dcopy(n,f,1,h,1)

C     2 x differentiation using 4 point formula and 
C     transformation to logarithmic coordintes
C     ========================================
      call differ(h,dx,n,dh)
      call differ(dh,dx,n,ddh)
      do j = 1,n
        ddf(j) = (ddh(j)-dh(j))/(dr(j)*dr(j))
        df(j)  = dh(j)/dr(j)
      enddo

      return
      end
C***********************************************************************
C***********************************************************************


C***********************************************************************
C***********************************************************************
      subroutine differ(f,dx,n,df)
C***********************************************************************
C
c   *                                                                *
c   *    <subroutine differ> differentiates the function <f(x)> in   *
c   *    the first <n> net points.                                   *
c   *    <dx> is the increment in the logarithmic x-net.             *
c   *    the result is stored in <df(x)>.                            *
c   *                                                                *
c   *    @U.Gerstmann                                   05.11.1998   *
C
C***********************************************************************
      implicit none
      include 'maxima.h'

      integer n,i
      real*8 dx,f(maxgrid),df(maxgrid)
     
      df(1)=( 6.d0*f(2)+(20.d0/3.d0)*f(4)+1.2d0*f(6)
     $     -2.45d0*f(1)-7.5d0*f(3)-3.75d0*f(5)
     $     -1.d0/6.d0*f(7) )/dx
      df(2)=( 6.d0*f(3)+(20.d0/3.d0)*f(5)+1.2d0*f(7)
     $     -2.45d0*f(2)-7.5d0*f(4)-3.75d0*f(6)
     $     -1.d0/6.d0*f(8) )/dx
      do i=3,n-2
        df(i)=( f(i-2)+8.d0*f(i+1)
     $       -8.d0*f(i-1)-f(i+2) )/12.d0/dx
      end do
      df(n-1)= (f(n)-f(n-2))/2.d0/dx
      df(n)  = (0.5d0*f(n-2)-2.d0*f(n-1)+1.5d0*f(n))/dx


C      df(1) = ( 6.d0*f(2) + (20.d0/3.d0)*f(4) +  1.2d0*f(6)
C     $      - 2.45d0*f(1) -        7.5d0*f(3) - 3.75d0*f(5)
C     $                       - 1.d0/6.d0*f(7) )/dx
C      df(2) = ( 6.d0*f(3) + (20.d0/3.d0)*f(5) +  1.2d0*f(7)
C     $     -  2.45d0*f(2) -        7.5d0*f(4) - 3.75d0*f(6)
C     $                       - 1.d0/6.d0*f(8) )/dx
C      do i = 3,n-2
C        df(i) = ( f(i-2)+8.d0*f(i+1)-8.d0*f(i-1)-f(i+2) )/12.d0/dx
C      end do
C      df(n-1) = (f(n)-f(n-2))/2.d0/dx
C      df(n)   = (0.5d0*f(n-2)-2.d0*f(n-1)+1.5d0*f(n))/dx

      return
      end
C***********************************************************************
C***********************************************************************

