function firstderiv_7point_for(xdata,ydata,dpas) result(res)
    real(kind=8) :: res
    real(kind=8),intent(in) :: xdata(7), ydata(7), dpas

     res     =(     6.d0*ydata(2)       &
           +(20.d0/3.d0)*ydata(4)       &
                  +1.2d0*ydata(6)       &
                 -2.45d0*ydata(1)       &
                  -7.5d0*ydata(3)       &
                 -3.75d0*ydata(5)       &
              -1.d0/6.d0*ydata(7) )/dpas/xdata(1)

end function


function firstderiv_7point_back(xdata,ydata,dpas) result(res)
    real(kind=8) :: res
    real(kind=8),intent(in) :: xdata(7), ydata(7), dpas

      res = (ydata(7)-ydata(6))/(xdata(7)-xdata(6))
!     res     =(     6.d0*ydata(6)       &
!           +(20.d0/3.d0)*ydata(4)       &
!                  +1.2d0*ydata(2)       &
!                 -2.45d0*ydata(7)       &
!                  -7.5d0*ydata(5)       &
!                 -3.75d0*ydata(3)       &
!              -1.d0/6.d0*ydata(1) )/dpas/xdata(7)

end function



function firstderiv_4point_for(xdata,ydata) result(res)
    real(kind=8) :: res
    real(kind=8),intent(in) :: xdata(4), ydata(4)
    real(kind=8) :: delta

     res = (ydata(1)-ydata(2))/(xdata(1)-xdata(2))
!    delta=xdata(2)-xdata(1)
    
!    res= -11.0d0*ydata(1)+18.0d0*ydata(2)-9.0d0*ydata(3)+2.0d0*ydata(4)
!    res= res /(6.0d0*delta)
end function


subroutine spline3(npoints,xdata,ydata,dpas,filename)
!======================================================================================
!
!   computes cubic splines coefficients using npoints values stored in ydata and xdata
!   following the linear scaling algorithm presented in "Numerical methods using Matlab"
!   by J.H. Mathews and K.D. Fink, ISBN 0130652482, section 5.3, pages: 280-290
!
!=======================================================================================
    implicit none
    integer, intent(in) :: npoints
    integer ::  i, info

    real(kind=8), intent(in) :: xdata(npoints),ydata(npoints),dpas
    real(kind=8), allocatable :: coef(:,:)
    real(kind=8), allocatable :: d(:),h(:),u(:)
    real(kind=8), allocatable :: A(:),AL(:),AU(:)
    real(kind=8) :: dx1, dxn, firstderiv_7point_back, firstderiv_7point_for

    character(len=20), intent(in) :: filename
    
    allocate ( d(npoints-1))
    allocate ( h(npoints-1))
    allocate ( u(npoints))
    allocate ( coef(3,npoints-1))
    allocate ( A(npoints-2))
    allocate (AU(npoints-3))
    allocate (AL(npoints-3))

!   initialize distance vector (h) and slope vector (d), and free terms vector u
!   ============================================================================
    do i=1, npoints-1
        h(i) =  xdata(i+1) - xdata(i)
        d(i) = (ydata(i+1) - ydata(i))/h(i)
        if ( i == 1 ) then
            cycle
        end if
        u(i) = 6.0d0*(d(i)-d(i-1))
    end do    

!   initialize first derivative at end points using 4-points formula
!   ================================================================
    dx1 = firstderiv_7point_for(xdata(1:7),ydata(1:7),dpas)
    dxn = firstderiv_7point_back(xdata(npoints-6:),ydata(npoints-6:),dpas)

!   construct the tridiagonal matrix of coefficients [AL,A,AU] 
!   ==========================================================
    u(2)         = u(2)-3.0d0*(d(1)-dx1)
    u(npoints-1) = u(npoints-1) - 3.0d0*(dxn-d(npoints-1))
    A(1)  = 1.5d0*h(1) + 2.0d0*h(2)
    AU(1) = h(2)
    do i=3, npoints-2
        AL(i-2) = h(i-1)
        A(i-1)  = 2.0d0*(h(i-1)+h(i))
        AU(i-1) = h(i)
    end do
    AL(npoints-3) = h(npoints-3)
    A(npoints-2)  = 2.0d0*h(npoints-3) + 1.5d0*h(npoints-2)

!   solve the system of tridiagonal linear equations
!   ================================================
    call dgtsv(npoints-2, 1, AL, A, AU, u(2:npoints-1), npoints-2, info)
    if ( info /= 0 ) then
        write(*,*) 'Spline fitting error...'
        stop
    end if   

!   compute second derivative at end points
!   =======================================
    u(1)       = (3.0d0/h(1))*(d(1)-dx1)-u(2)/2.0d0
    u(npoints) = (3.0d0/h(npoints-1))*(dxn-d(npoints-1)) - u(npoints-1)/2.0d0

!   compute spline coefficients
!   ===========================
    do i=1, npoints-1
        coef(1,i) = d(i) - (h(i)*(2.0d0*u(i)+u(i+1)))/6.0d0
        coef(2,i) = u(i)/2.0d0
        coef(3,i) = (u(i+1)-u(i))/(6.0d0*h(i))
    end do

!   write out the spline coefficients
!   =================================    
    open(unit=99,file=trim(filename))
	write(99,*)npoints-1
        do i=1, npoints-1
            write(99,'(6es28.16E3)') xdata(i), xdata(i+1), ydata(i), coef(:,i)
        end do
    close(99)

end subroutine
