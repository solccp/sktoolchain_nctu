	subroutine xcpotential(functional_name,rho,drho,ddrho,r,exc,vxc)
C       ****************************************************************
C       *
C       * calculate the value of exchange-correlation potential at point r
C       * type of the potential is decided by the flag functional_name
C       *
C       * input:
C       *   rho   - electron density
C       *   drho  - first derivative of electron density (drho)/(dr)
C       *   ddrho - second derivative of electron density (d2rho)/(dr2)
C       *   r     - distance from nucleus
C       *   
C       * output:
C       *   vxc   - value of the potential
C       *   
C       ****************************************************************
	implicit none

	integer jj
	real*8 r,rho,drho,ddrho,rhoa,gmaa,drho2
	real*8 pi,cc,dd,ee,der,ff
	parameter(pi=3.14159265358979323846264338327950d0)

 	character*60 functional_name

	real*8 res(21),vxc,exc

	rhoa = rho/2.0d0
	gmaa = drho*drho/4.0d0
	drho2 = drho * drho
	jj=index(functional_name," ")-1

	if (functional_name(1:jj) .eq. 'lda') then
	  call lda_xc_functional(1, rhoa, rhoa, res)
C	  exc = res(1)
C	  vrhoa = res(2)
	  exc = res(1)
	  vxc = res(2)
	  return
	else if (functional_name(1:jj) .eq. 'svwn') then
	  call svwn_xc_functional(1, rhoa, rhoa, res)
C	  vrhoa = res(2)
	  exc = res(1)
	  vxc = res(2)
	  return
	else if (functional_name(1:jj) .eq. 'blyp') then
	  call blyp_xc_functional(2,rhoa,rhoa,gmaa,gmaa,gmaa,res)
	else if (functional_name(1:jj) .eq. 'slyp') then
	  call slyp_xc_functional(2,rhoa,rhoa,gmaa,gmaa,gmaa,res)
	else if (functional_name(1:jj) .eq. 'pw91') then
	  call pw91_xc_functional(2,rhoa,rhoa,gmaa,gmaa,gmaa,res)
	else if (functional_name(1:jj) .eq. 'revpbe') then ! needs testing
	  call revpbe_xc_functional(2,rhoa,rhoa,gmaa,gmaa,gmaa,res)
	else if (functional_name(1:jj) .eq. 'pbe') then
	  call pbe_xc_functional(2,rhoa,rhoa,gmaa,gmaa,gmaa,res)
	else
	  write(*,*) "Ooops, not supported functional"
	  write(*,*) "XC functional =", functional_name
	  stop
	end if

C	vrhoa = res(2)
C	vsigmaaa = res(7)
C	vsigmaab = res(8)
C	v2rhoasigmaaa = res(16)
C	v2rhoasigmaab = res(17)
C	v2rhobsigmaaa = res(19)
C	v2rhobsigmaab = res(20)
C	v2sigmaaa2 = res(10)
C	v2sigmaaaab = res(11)
C	v2sigmaaabb = res(12)
C	v2sigmaab2 = res(13)
C	v2sigmaabbb = res(14)

	exc = res(1)

	vxc = res(2) - (res(7)+0.5d0*res(8)) * (ddrho + 2.0d0 * drho/r)
	vxc = vxc - 0.25d0 * (2.0d0*res(16) + 2.0d0*res(19) + res(17) 
     &            + res(20)) * drho * drho
	vxc = vxc - 0.25d0 * (2.0d0*res(10) + 3.0d0*res(11) + res(13)
     &            + 2.0d0*res(12) + res(14)) * drho * drho * ddrho

	return

	end
