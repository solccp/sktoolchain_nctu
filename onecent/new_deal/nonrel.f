C***********************************************************************
C***********************************************************************
	subroutine nonrel(norb,np,nqn,nql,nk,dr,dpc,nel,den,dpas)
C***********************************************************************
C
C       averaging spin-orbit splitted spinor large components into
C	spin-free orbital for all valence shells
C
C       nqn  - principal quantum number
C       nql  - orbital quantum number
C       nk   - kappa quantum number
C       dr   - integration grid
C       dpc  - large component of all spinors
C       np   - number of grid points
C       norb - number of spinor shells
C
C***********************************************************************
	implicit none
	include 'maxima.h'
	integer i,ii,jj,ip,nz,morb,iorb,j,k
	integer norb,np,nqn(maxsh),nql(maxsh),nk(maxsh)
	real*8 dr(maxgrid),dpc(maxgrid,maxsh),orb(maxgrid,maxsh)
	real*8 nel(maxsh),den(maxsh),aa,norm,dpas
	real*8 mnel(maxsh),mden(maxsh)
	real*8 dp2(maxgrid),dq2(maxgrid),dval
	integer part(maxsh),mqn(maxsh),mql(maxsh),map(maxsh,2)
	integer shells(8,4),maxel(8),core(maxsh)
	character*2 olbl(maxsh)
	character*1 anglbl(4)
	character*7 corval(2)
	character*64 filnam

	data dq2 /maxgrid*0.0d0/
	data part,shells /maxsh*0,32*0/
	data anglbl,corval /'s','p','d','f',' core   ','valence'/
	data maxel /2,8,18,32,32,32,32,32/

C	find nonrelativistic partner for each spinor
C	============================================
	do ii = 1,norb
	 if (part(ii).eq.0) then
	  do jj = ii+1,norb
	   if (nqn(ii).eq.nqn(jj)) then
	    if (nql(ii).eq.nql(jj)) then
	     part(ii) = jj
	     part(jj) = ii
	    end if
	   end if
	  end do
	 end if
	end do

C	set up tables for scalar orbitals
C	=================================
	morb = 0	
	do ii = 1,norb
	 if (part(ii).eq.0) then
	  morb = morb+1
	  map(morb,1) = ii
	  map(morb,2) = 0
	  part(ii) = -1
	  part(morb) = 1
	 else if (part(ii).gt.0) then
	  morb = morb+1
	  if (nk(ii).gt.0) then
	   map(morb,1) = ii
	   map(morb,2) = part(ii)
	  else
	   map(morb,1) = part(ii)
	   map(morb,2) = ii
	  end if
	  part(part(ii)) = -1
	  part(ii) = -1
	  part(morb) = 2
	 end if
	end do

C	find which shells have been requested
C	=====================================
	do ii = 1,morb
	 shells(nqn(map(ii,1)),nql(map(ii,1))+1) = ii
	end do

C       normalization of Schrodinger-like orbital P is  I[(P*P)*4*pi*r^2]dr = 1
C       =======================================================================

C	calculate orbitals for requested shells
C	=======================================
	iorb = 0
	do ii = 1,8
	 do jj = 1,min(4,ii)
	  if (shells(ii,jj).ne.0) then
	   iorb = iorb+1
	   write(olbl(iorb)(1:1),'(i1)')ii
	   write(olbl(iorb)(2:2),'(a1)')anglbl(jj)
	   if (part(shells(ii,jj)).eq.1) then
	    do ip = 1,np
	     orb(ip,iorb) = dpc(ip,map(shells(ii,jj),1))
     &        /(2*sqrt(pi)*dr(ip))
	    end do
	    mnel(iorb) = nel(map(shells(ii,jj),1))
	    mden(iorb) = den(map(shells(ii,jj),1))
	   else
	    do ip = 1,np
	     orb(ip,iorb) =
     &        (dble(nql(map(shells(ii,jj),1)))
     &        *dpc(ip,map(shells(ii,jj),1))
     &        +dble(nql(map(shells(ii,jj),1))+1)
     &        *dpc(ip,map(shells(ii,jj),2)))
     &        /dble(2*nql(map(shells(ii,jj),1))+1)
     &        /(2*sqrt(pi)*dr(ip))
	    end do
	    mnel(iorb) = nel(map(shells(ii,jj),1))
     &       + nel(map(shells(ii,jj),2))
	    mden(iorb) =
     &       (dble(nql(map(shells(ii,jj),1)))
     &       *den(map(shells(ii,jj),1))
     &       +dble(nql(map(shells(ii,jj),1))+1)
     &       *den(map(shells(ii,jj),2)))
     &       /dble(2*nql(map(shells(ii,jj),1))+1)
	   end if
	   mqn(iorb) = nqn(map(shells(ii,jj),1))
	   mql(iorb) = nql(map(shells(ii,jj),1))
	  end if
	 end do
	end do

C	divide scalar orbitals into clases: core and valence
C	====================================================
	do ii = 1,8
	 aa = 0.0d0
	 do jj = 1,min(4,ii)
	  if (shells(ii,jj).ne.0) then
	   aa = aa + mnel(shells(ii,jj))
	  end if
	 end do
	 if(idint(aa).eq.maxel(ii))then
	  iorb = 1
	 else
	  iorb = 2
	 end if
	 do jj = 1,min(4,ii)
	  if (shells(ii,jj).ne.0) then
	   core(shells(ii,jj)) = iorb
	  end if
	 end do
	end do

C	print occupations and energies of scalar orbitals
C	=================================================
	write(6,'(//a/)')'  * info about scalar atomic orbitals'
	write(6,'(a,a)')'     num    orbital     occupation',
     &  '       final energy          type               normalization'
	write(6,'(a,a)')'    =====  =========  ==============  ======',
     &  '============     =========        ===================='

C	normalize valence scalar obitals
C	================================
	call dcopy(np,0.0d0,0,dq2,1)
	do i=1,morb
	 call dcopy(np,orb(1,i),1,dp2,1)
	 do j=1,np
	  dp2(j)=dp2(j)*dp2(j)*dr(j)*dr(j)
	 end do
         dval = dble(mql(i)+1)
	 call somm(dr,dp2,dq2,dpas,dval,0,np)
	 call dscal(np,1.0d0/sqrt(dval),orb(1,i),1)
	 call dcopy(np,orb(1,i),1,dp2,1)
	 do j=1,np
	  dp2(j)=dp2(j)*dp2(j)*dr(j)*dr(j)
	 end do
         dval = dble(mql(i)+1)
	 call somm(dr,dp2,dq2,dpas,dval,0,np)
	 write(6,'(6x,i2,7x,a2,10x,f5.2,2x,f20.5,8x,a,3x,f25.16,i3)')
     &   i,olbl(i),mnel(i),mden(i),corval(core(i)),dval
	end do

C	save atomic valence orbitals to file
C	====================================
	open(file='orbitals',unit=21)
	do ii=1,morb
	 if(core(ii).eq.2)then
	  write(21,'(a)')olbl(ii)
	  filnam=olbl(ii)//'_orb'
	  call spline3(np,dr,orb(1,ii),dpas,filnam)

C         save laplacian of radial wave function to file
C         ==============================================
	  call diffrho(orb(1,ii),np,dp2,dq2,dr,dpas)
	  do j = 1,np
	   dp2(j) = - dp2(j)/dr(j) - dq2(j)/2.0d0 + dble(mql(ii)
     &     * (mql(ii)+1)/2) * orb(j,ii)/(dr(j)*dr(j))
	  end do
	  filnam=olbl(ii)//'_orb_lapl'
	  call spline3(np,dr,dp2,dpas,filnam)
	 end if
	end do
	close(21)	 

	end
C***********************************************************************
