!***********************************************************************
!***********************************************************************
subroutine nonrel(norb,np,nqn,nql,nk,dr,dpc,nel,den,dpas)
!***********************************************************************
!
!       averaging spin-orbit splitted spinor large components into
!	spin-free orbital for all valence shells
!
!       nqn  - principal quantum number
!       nql  - orbital quantum number
!       nk   - kappa quantum number
!       dr   - integration grid
!       dpc  - large component of all spinors
!       np   - number of grid points
!       norb - number of spinor shells
!
!***********************************************************************
    use Accuracy
    use Limits
    use spline
    implicit none
    integer :: i,ii,jj,ip,nz,morb,iorb,j,k
    integer :: norb,np,nqn(maxsh),nql(maxsh),nk(maxsh)
    real(dp) :: dr(maxgrid),dpc(maxgrid,maxsh),orb(maxgrid,maxsh)
    real(dp) :: nel(maxsh),den(maxsh),aa,norm,dpas
    real(dp) :: mnel(maxsh),mden(maxsh)
    real(dp) :: dp2(maxgrid),dq2(maxgrid),dval,dp3(maxgrid),dp4(maxgrid)
    integer :: part(maxsh),mqn(maxsh),mql(maxsh),map(maxsh,2)
    integer :: shells(8,4),maxel(8),core(maxsh)
    real(dp), allocatable, dimension(:) :: temp_dv, temp_d2v

    character(len=2) :: olbl(maxsh)
    character(len=1) :: anglbl(4)
    character(len=7) :: corval(2)
    character(len=64) :: filnam

    integer :: orb_indexes(20)
    integer :: t1


    data anglbl,corval /'s','p','d','f',' core   ','valence'/

    maxel = (/2,8,18,32,32,32,32,32/)
    part = 0
    shells = 0
    dq2 = zero

!	find nonrelativistic partner for each spinor
!	============================================
    do ii = 1,norb
        if (part(ii) == 0) then
            do jj = ii+1,norb
                if (nqn(ii) == nqn(jj)) then
                    if (nql(ii) == nql(jj)) then
                        part(ii) = jj
                        part(jj) = ii
                    end if
                end if
            end do
        end if
    end do

!	set up tables for scalar orbitals
!	=================================
    morb = 0
    do ii = 1,norb
        if (part(ii) == 0) then
            morb = morb+1
            map(morb,1) = ii
            map(morb,2) = 0
            part(ii) = -1
            part(morb) = 1
        else if (part(ii) > 0) then
            morb = morb+1
            if (nk(ii) > 0) then
                map(morb,1) = ii
                map(morb,2) = part(ii)
            else
                map(morb,1) = part(ii)
                map(morb,2) = ii
            end if
            part(part(ii)) = -1
            part(ii) = -1
            part(morb) = 2
        end if
    end do

!	find which shells have been requested
!	=====================================
    do ii = 1,morb
        shells(nqn(map(ii,1)),nql(map(ii,1))+1) = ii
    end do

!       normalization of Schrodinger-like orbital P is  I[(P*P)*4*pi*r^2]dr = 1
!       =======================================================================

!	calculate orbitals for requested shells
!	=======================================
    iorb = 0
    do ii = 1,8
        do jj = 1,min(4,ii)
            if (shells(ii,jj) /= 0) then
                iorb = iorb+1
                write(olbl(iorb)(1:1),'(i1)')ii
                write(olbl(iorb)(2:2),'(a1)')anglbl(jj)
                if (part(shells(ii,jj)) == 1) then !s orbitals
                    do ip = 1,np
                        orb(ip,iorb) = dpc(ip,map(shells(ii,jj),1))/(2*sqrt(pi)*dr(ip))
                    end do
                    mnel(iorb) = nel(map(shells(ii,jj),1))
                    mden(iorb) = den(map(shells(ii,jj),1))
                else
                    do ip = 1,np
                        orb(ip,iorb) = (dble(nql(map(shells(ii,jj),1)))*dpc(ip,map(shells(ii,jj),1)) &
                                     + dble(nql(map(shells(ii,jj),1))+1)*dpc(ip,map(shells(ii,jj),2))) &
                                     / dble(2*nql(map(shells(ii,jj),1))+1)/(2*sqrt(pi)*dr(ip))
                    end do
                    mnel(iorb) = nel(map(shells(ii,jj),1)) + nel(map(shells(ii,jj),2))
                    mden(iorb) = (dble(nql(map(shells(ii,jj),1)))*den(map(shells(ii,jj),1)) &
                               + dble(nql(map(shells(ii,jj),1))+1)*den(map(shells(ii,jj),2))) &
                               / dble(2*nql(map(shells(ii,jj),1))+1)
                end if
                mqn(iorb) = nqn(map(shells(ii,jj),1))
                mql(iorb) = nql(map(shells(ii,jj),1))
            end if
        end do
    end do

!	divide scalar orbitals into clases: core and valence
!	====================================================
    do ii = 1,8
        aa = zero
        do jj = 1,min(4,ii)
            if (shells(ii,jj) /= 0) then
                aa = aa + mnel(shells(ii,jj))
            end if
        end do
        if(idint(aa) == maxel(ii))then
            iorb = 1
        else
            iorb = 2
        end if
        do jj = 1,min(4,ii)
            if (shells(ii,jj) /= 0) then
                core(shells(ii,jj)) = iorb
            end if
        end do
    end do

!	print occupations and energies of scalar orbitals
!	=================================================
    write(6,'(//a/)')'  * info about scalar atomic orbitals'
    write(6,'(a,a)')'     num    orbital     occupation       final energy          type               normalization'
    write(6,'(a,a)')'    =====  =========  ==============  ==================     =========        ===================='

!	normalize valence scalar obitals
!	================================
    dq2(:np) = zero
    do i=1,morb
        dp2(:np) = orb(:np,i)
        dp2(:np)=dp2(:np)*dp2(:np)*dr(:np)*dr(:np)
        dval = dble(mql(i)+1)
        call somm(dr,dp2,dq2,dpas,dval,0,np)
        orb(:np,i) = orb(:np,i) / sqrt(dval)
    end do


!   Gran-Schmidt reorthogonalization
!   ===================================
    ! jj loop for all different orb angular momentum
    jj = maxval(mql(:morb))
    do i = 0, jj
        ! find indexes of orbitals corresponding to the same orb angular momentum jj.
        t1 = 0
        do j = 1, morb
            if ( mql(j) == i ) then
                t1 = t1 + 1
                orb_indexes(t1) = j
            end if
        end do
        ! gs
        do j = 2, t1
            do k = 1, j-1
                dval = dble(i+1)
                dp2(:np) = orb(:np,orb_indexes(j))*orb(:np,orb_indexes(k))*dr(:np)*dr(:np)
                call somm(dr,dp2,dq2,dpas,dval,0,np) 
                orb(:np,orb_indexes(j)) = orb(:np,orb_indexes(j)) - dval*orb(:np,orb_indexes(k))
            end do
            dval = dble(i+1)
            dp2(:np) = orb(:np,orb_indexes(j))*orb(:np,orb_indexes(j))*dr(:np)*dr(:np)
            call somm(dr,dp2,dq2,dpas,dval,0,np)
            orb(:np,orb_indexes(j)) = orb(:np,orb_indexes(j)) / sqrt(dval)
        end do
    end do

!	print normalization info of valence scalar obitals
!	================================
    do i=1,morb
        dp2(:np) = orb(:np,i)
        dp2(:np)=dp2(:np)*dp2(:np)*dr(:np)*dr(:np)
        dval = dble(mql(i)+1)
        call somm(dr,dp2,dq2,dpas,dval,0,np)
        write(6,'(6x,i2,7x,a2,7x,f8.5,2x,f20.8,8x,a,3x,f25.16,i3)') i,olbl(i),mnel(i),mden(i),corval(core(i)),dval
    end do

! Print overlaps
  write(6, *) ''
  write(6, '(a)') 'Overlap of atomic orbitals:'
  do ii=1, morb
    do jj=1, ii    
        if (mql(ii) == mql(jj)) then
            dval = dble(mql(ii)+1)
            dp2(:np) = orb(:np, ii)*orb(:np, jj)*dr(:np)*dr(:np)
            call somm(dr,dp2,dq2,dpas,dval,0,np) 
            write (6, '(2x, a3, 1x, a1, 1x, a3, a1, f20.12)') olbl(ii), '-', olbl(jj), ':', dval
        else
            dval = 0
            write (6, '(2x, a3, 1x, a1, 1x, a3, a1, f20.12)') olbl(ii), '-', olbl(jj), ':', dval
        end if
    end do
  end do 


!	save atomic valence orbitals to file
!	====================================
    allocate(temp_dv(np))
    allocate(temp_d2v(np))
    open(file='orbitals',unit=21)
    do ii=1, morb
        temp_dv = 0.0_dp
        temp_d2v = 0.0_dp
        if(core(ii) == 2)then
            write(21,'(a)')olbl(ii)
            filnam=olbl(ii)//'_orb'
            call spline3_return(np,dr,orb(:,ii),dpas,filnam, temp_dv, temp_d2v)
            filnam='wave_0'//olbl(ii)//'_up.dat'
            open(file=filnam, unit=37)
            write(37, *) np
            do jj = 1, np
                write(37, '(5E26.16)') dr(jj), 0.0, orb(jj,ii), temp_dv(jj), temp_d2v(jj)
            end do
            close(37)

!         save laplacian of radial wave function to file
!         ==============================================
            call diffrho(orb(1,ii),np,dp2,dq2,dr,dpas)
            dp2(:np) = -dp2(:np)/dr(:np) - dq2(:np)/two + dble(mql(ii) * (mql(ii)+1)/2) * orb(:np,ii)/(dr(:np)*dr(:np))
            filnam=olbl(ii)//'_orb_lapl'
            call spline3(np,dr,dp2,dpas,filnam)
        end if
    end do
    close(21)
    deallocate(temp_dv)
    deallocate(temp_d2v)






end subroutine
!***********************************************************************
