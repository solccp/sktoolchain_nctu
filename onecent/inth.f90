!***********************************************************************
!***********************************************************************
subroutine inth(P,Q,P1,Q1,v,r,e,c,k,h)
!***********************************************************************
!
!     written by Henryk Witek, NCTU, 2007 
!     (based on Desclaux's Dirac-Slater program)
!
!***********************************************************************
!
!     this routine performs integration of Dirac radial equation 
!     (for small and large components P and Q) using 5 points 
!     predictor-corrector method of Adams (Adams-Bashfort predictor and 
!     Adams-Moulton corrector);
!     details of the approach can be found in:
!       * "Relativistic atomic wave functions" by J.P.Desclaux, 
!          D.F.Mayers, and F.O'Brien, J.Phys.B 4, 631 (1971)
!       * "Introduction to Numerical Analysis" 
!          by F.B.Hildebrand,McGraw-Hill,NY,1965
!       * "Introduction to Numerical Analysis" by J.Stoer 
!          and R. Burlich, chapter 7.2.6, Springer, NY,1980
!       * "Electron scattering in solid matter" by J.Zabloudil,
!          R.Hammerling, L.Szunyogh, and P.Weinberger,
!          Springer,Berlin,2005
!
!***********************************************************************
!
!     Description of input parameters
!
!     P  - large component at distance r
!     Q  - small component at distance r
!     v  - potential at distance r
!     r  - distance from origin
!     P1 - first derivatives of P at 5 subsequent points
!     Q1 - first derivatives of Q at 5 subsequent points
!     e  - spinor energy
!     c  - speed of light
!     k  - quantum number kappa
!     h  - integration step (dpas/720)
!
!***********************************************************************
    use Accuracy
    use Limits
    implicit none
    integer :: ii,k
    real(dp), intent(in)    :: v, r, e, c, h
    real(dp), intent(inout) :: P1(5), Q1(5), P, Q
    real(dp) :: dpr, dqr

!     calculate value of predictor at r
!     *********************************
    dpr = P + h*(1901.0_dp*P1(5) - 2774.0_dp*P1(4) + 2616.0_dp*P1(3) &
               - 1274.0_dp*P1(2) +  251.0_dp*P1(1))
    dqr = Q + h*(1901.0_dp*Q1(5) - 2774.0_dp*Q1(4) + 2616.0_dp*Q1(3) &
               - 1274.0_dp*Q1(2) +  251.0_dp*Q1(1))

!     update the vectors of first derivative of P & Q at r
!     ****************************************************
    do ii = 2, 5
        P1(ii-1) = P1(ii)
        Q1(ii-1) = Q1(ii)
    end do
    P1(5) = -dble(k)*dpr + r*(two*c + (e-v)/c)*dqr
    Q1(5) =  dble(k)*dqr - r*(        (e-v)/c)*dpr

!     calculate value of corrector at r
!     *********************************
    P = P + h*(251.0_dp*P1(5) + 646.0_dp*P1(4) - 264.0_dp*P1(3) &
             + 106.0_dp*P1(2) -  19.0_dp*P1(1))
    Q = Q + h*(251.0_dp*Q1(5) + 646.0_dp*Q1(4) - 264.0_dp*Q1(3) &
             + 106.0_dp*Q1(2) -  19.0_dp*Q1(1))

!     average predictor and corrector to get P & Q at r
!     *************************************************
    P = (475.0_dp*P + 27.0_dp*dpr)/502.0_dp
    Q = (475.0_dp*Q + 27.0_dp*dqr)/502.0_dp

!     calculate values of first derivatives of P & Q at r
!     ***************************************************
    P1(5) = -dble(k)*P + r*(two*c + (e-v)/c)*Q
    Q1(5) =  dble(k)*Q - r*(        (e-v)/c)*P
!
end subroutine
!***********************************************************************
!***********************************************************************

