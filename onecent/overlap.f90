subroutine overlap(filout,norb,dpas,nk,nql,nqn,nmax,dfl,dr,dpc,dqc,titre)
    use Accuracy
    use Limits
    implicit none

    integer, intent(in) :: filout, norb
    integer, intent(in) :: nk(maxsh),nql(maxsh),nmax(maxsh),nqn(maxsh)
    real(dp), intent(in) :: dr(maxgrid),dpas
    real(dp), intent(in) :: dpc(maxgrid,maxsh),dqc(maxgrid,maxsh),dfl(maxsh)
    character(len=4), intent(in) :: titre(maxsh)

    real(dp) :: dpl(maxgrid),dq(maxgrid)


    integer :: ii, jj, im
    real(dp) :: dval

!   compute overlap integrals between two radial spinors
!   ====================================================
    do ii = 1, norb
        do jj = 1, ii-1
            if ( nql(ii) /= nql(jj) ) cycle
            if ( nk(ii)  /= nk(jj)  ) cycle
            im = min(nmax(jj),nmax(ii))

            dq(:im) = dqc(:im,ii)*dqc(:im,jj)
            dpl(:im) = dpc(:im,ii)*dpc(:im,jj)

            dval = dfl(ii) + dfl(jj)
            call somm(dr,dpl,dq,dpas,dval,0,im)

            write(filout,'(6X,I1,1X,A4,8x,I3,1x,A4,3x,F25.12)') nqn(jj),titre(jj),nqn(ii),titre(ii),dval
        end do
    end do
end subroutine

