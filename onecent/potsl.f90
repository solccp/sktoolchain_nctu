!***********************************************************************
!***********************************************************************
subroutine potsl(veff,vcoul,vnc,vtot,dr,den,dpas,z,np,ion,iter,rn,nucmod)
!***********************************************************************
!
!     this routine integrates the potential using a 4-points integration method
!
!     on exit: veff  => total effective potential
!              vcoul => Coulomb potential
!              vnc  => Coulomb + nuclear potential
!              vtot  => Coulomb + nuclear + xc potential
!      
!     den      - density

!     dr     - grid
!     dpas   - integration step (exponential)
!     z      - atomic number
!     np     - number of points on grid
!     ion    - atomic total charge
!     iter   - number of current iteration
!     rn     - radius of the nucleus in the finite-size nucleus model
!     nucmod - flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
!     fxcname - name of the XC potential to be used in the calculations
!
!***********************************************************************
    use Accuracy
    use Limits
    use libxc
    implicit none

    integer, intent(in) :: z, np, ion, iter, nucmod
    real(dp), intent(in) :: dpas, rn
    real(dp), intent(in) :: den(maxgrid), dr(maxgrid)
    real(dp), intent(out) :: vtot(maxgrid), vnc(maxgrid), veff(maxgrid), vcoul(maxgrid)

    real(dp) :: vxc, exc
    real(dp) :: das, dlo, dlo2, nucpot 
    integer ii,kk
    real(dp) :: d0(maxgrid),d1(maxgrid),d2(maxgrid)


!   initialize constants
!   ====================
    das  = dpas/24.0_dp
    dlo  = dexp(dpas)
    dlo2 = dlo*dlo

!     compute integrand for obtaining Coulomb potential
!     =================================================
    veff(:np) = den(:np)*dr(:np)

!   boundary conditions for integration
!   ===================================
    vnc(2) = dr(1)*(den(2)-den(1)*dlo2)/(12.0_dp*(dlo-1.0_dp))
    vnc(1) = veff(1)/3.0_dp - vnc(2)/dlo2
    vnc(2) = veff(2)/3.0_dp - vnc(2)*dlo2
      
!   compute Coulomb potential
!   =========================
    do ii = 3, np-1
        vnc(ii) = vnc(ii-1) + das*(13.0_dp*(veff(ii)+veff(ii-1))-(veff(ii-2)+veff(ii+1)))
    end do
    vnc(np)   = vnc(np-1)
    veff(np-1) = vnc(np-1)
    veff(np)   = vnc(np-1)
    do kk = np-2, 2, -1
        veff(kk) = veff(kk+1)/dlo + das*(13.0_dp*(vnc(kk)+vnc(kk+1)/dlo) - (vnc(kk+2)/dlo2+vnc(kk-1)*dlo))
    end do
    veff(1) = veff(3)/dlo2 + dpas*(vnc(1)+4.0_dp*vnc(2)/dlo+vnc(3)/dlo2)/3.0_dp

!     save Coulomb potential for calculating expectation values
!     =========================================================
    vcoul(:np) = veff(:np) / dr(:np)    
    veff(:np) = vcoul(:np)

!     transform density to radial density
!     ===================================
    d0(:np) = den(:np)/(4.0_dp*pi*dr(:np)*dr(:np))  

!   if GGA, compute first and second derivatives of density
!   =======================================================
    if (is_gga()) then
        call diffrho(d0,maxgrid,d1,d2,dr,dpas)
    end if

!   compute nuclear potential
!   =========================
    do ii = 1,np
        veff(ii) = veff(ii) + nucpot(dr(ii),z,rn,nucmod)
        vnc(ii) = veff(ii)
    end do

!   compute exchange-correlation potential
!   ======================================
    do ii = 1,np
        call xcpotential(d0(ii),d1(ii),d2(ii),dr(ii),exc,vxc)
        veff(ii) = veff(ii) + vxc
        vtot(ii) = veff(ii)
    end do

!   compute confining Woods-Saxon potential
!   =======================================
    
!    veff(:np) = veff(:np) + conf_w/(1.0_dp+exp(-conf_a*(dr(:np)-conf_r)))
    

end subroutine
!***********************************************************************
!***********************************************************************

