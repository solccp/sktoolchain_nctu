!***********************************************************************
!***********************************************************************
program atom
!***********************************************************************
!
!     Program to solve many-electron atomic Dirac problem; a numerical
!     integration scheme is used on a logarithmic grid; based on 
!     relativistic atomic code by J.P. DESCLAUX (CEA PARIS 1969);
!     final radial wavefunctions, density and potential are saved on grid
!
!***********************************************************************
    use Accuracy
    use Limits
    use libxc
    use spline
    implicit none
    integer, parameter :: maxtotal=maxsh*maxgrid
    integer  :: norb,z,nes,np,ion,niter,nucmod,iter,ii,jj,kk
    integer  :: i,j,ll,im,ip,filout,filin
    integer  :: nk(maxsh),nmax(maxsh),nqn(maxsh),nql(maxsh)
    real(dp) :: econv,vmax,emax,ymax,de,val,dval,aa,bb,ab,rn,dvc,ddot
    real(dp) :: dpas,dcop,test,teste,testy,testv
    real(dp), dimension(:,:) :: conf_w(maxn,maxl),conf_a(maxn,maxl),conf_r(maxn,maxl)
    real(dp) :: nel(maxsh),den(maxsh),dq1(maxsh),dfl(maxsh)
    real(dp) :: dv(maxgrid),dr(maxgrid),v_dp(maxgrid),dq(maxgrid)
    real(dp) :: d(maxgrid),dc(maxgrid),dvn(maxgrid), d_core(maxgrid)
    real(dp) :: dpc(maxgrid,maxsh),dqc(maxgrid,maxsh)
   
    real(dp) :: veff(maxgrid), vcoul(maxgrid), vnc(maxgrid), vtot(maxgrid)
    real(dp) :: energy(5)
    logical  :: conv, include_virtual_orbitals
    character(len=20) :: fxcname
    character(len=4)  :: titre(maxsh)
    integer :: highest_l, n_core = 0
    real(dp), allocatable, dimension(:) :: temp_dv, temp_d2v

    integer :: argcount
    CHARACTER(len=32) :: arg
    character(len=32), parameter :: version = '20180123'

    argcount = command_argument_count()    
    
    if (argcount>0) then
        CALL get_command_argument(1, arg)
        if ( trim(arg) == '-v' ) then
            print *, version
            stop
        else if (trim(arg) == '-c') then
            CALL get_command_argument(2, arg)
            read(arg, *) n_core
            print*, n_core
        end if

    end if

    dpc = 0.0_dp
    dqc = 0.0_dp

!   open all files
!   ==============
    filin = 5
    filout = 6
    open(filin,file='input')
    open(filout,file='output')

!   read input data
!   ===============
    call insld(den,dq1,dfl,dv,dr,nqn,nql,nk,nmax,nel,titre,norb,      &
               dpas,z,nes,np,dcop,test,teste,testy,testv,niter,ion,   &
               conf_w,conf_a,conf_r,rn,nucmod,dvc,fxcname) 

    call init_libxc('lda ')

!   starts solving the atomic DFT problem
!   =====================================
    write(6,10000)

    conv = .false.

!   start SCF iterations
!   ====================
    include_virtual_orbitals = .false.
    do iter = 1,niter
        if (niter > 10 .and. iter == 10) then
            call init_libxc(fxcname)
        end if 

!   initialize SCF convergence indicators
!   =====================================
        d(1:np) = zero
        econv = test
        ymax = zero
        vmax = zero
        emax = zero

!   solving radial Dirac equation for each radial spinor
!   ====================================================
        do jj = 1,norb
            if (include_virtual_orbitals .or. nel(jj) > 0.0_dp ) then
                de = den(jj)
                call resld( nqn(jj), nql(jj), nk(jj), nmax(jj), &
                     den(jj),   &  ! output (spinor energy)
                     dfl(jj),   &  
                     dq1(jj),   &  ! output (sign of the small comp at origin)
                     jj, &
                     dv, &
                     dr, &
                     v_dp, dq, &  ! output (large and small comp of spinor jj)
                     dpas, z, nes, econv, np, &
                     conf_w(nqn(jj), nql(jj)+1), conf_a(nqn(jj), nql(jj)+1), conf_r(nqn(jj), nql(jj)+1), &
                     rn, nucmod, dvc)

    !   test change of spinor energy between last 2 iterations
    !   ======================================================
                val = dabs((den(jj)-de)/de)  
                if (val > emax) emax = val

    !     test change of |spinor wave function| between last 2 iterations
    !     ===============================================================
                do ii = 1, np
                    val = dpc(ii,jj) - v_dp(ii)
                    if (dabs(v_dp(ii)) > 1.0_dp) val = val/v_dp(ii)
                    if (dabs(val) >= dabs(ymax)) then
                        ymax = val
                    end if
                    val = dqc(ii,jj) - dq(ii)
                    if (dabs(dq(ii)) > 1.0_dp) val = val/dq(ii)
                    if (dabs(val) >= dabs(ymax)) then
                        ymax = val
                    end if

    !     update radial spinors
    !     =====================
                    dpc(ii,jj)=v_dp(ii)
                    dqc(ii,jj)=dq(ii)

    !     compute contribution from spinor jj to total electron density
    !     =============================================================
                    d(ii) = d(ii) + nel(jj)*(v_dp(ii)*v_dp(ii)+dq(ii)*dq(ii))
                end do
            end if
        end do

!   evaluate new potential
!   ======================
        call potsl(veff,vcoul,vnc,vtot,dr,d,dpas,z,np,ion,iter,rn,nucmod)

!   find largest discrepancy between the old and the new potential
!   ==============================================================
        do ii = 20, np
            dval = dabs(veff(ii)-dv(ii))
            if (dr(ii)*veff(ii) <= -ion-1) dval = -dval/veff(ii)
            if (dval > vmax) then
                vmax = dval
                jj = ii
            end if
        end do

!   print convergence information in this iteration
!   ===============================================
        write(6,10007) iter,vmax,ymax,emax,dr(jj),veff(jj)

        if (( .not. include_virtual_orbitals ) .and. abs(vmax) < 1.0e-5_dp) then
            write (6, '(A)') 'Enable virtual orbitals in SCF cycles.'
            include_virtual_orbitals = .true.
        end if

!   check convergence criteria
!   ==========================
!        conv = econv <= test .and. emax <= teste .and. vmax <= testv .and. ymax <= testy
        conv = (econv <= test .and. emax <= teste .and. ymax <= testy)
        if (conv) then
            exit
        end if

!     average old and new potentials with coefficient dcop
!     ====================================================
        dv(:np) = (1.0_dp-dcop)*dv(:np) + dcop*veff(:np)

!     finish SCF iterations
!     =====================
    end do

    do jj = 1, norb
        if (nel(jj) > 0.0_dp) then
            cycle
        end if
        de = den(jj)
        call resld( nqn(jj), nql(jj), nk(jj), nmax(jj), &
            den(jj),   &  ! output (spinor energy)
            dfl(jj),   &  
            dq1(jj),   &  ! output (sign of the small comp at origin)
            jj, &
            dv, &
            dr, &
            v_dp, dq, &  ! output (large and small comp of spinor jj)
            dpas, z, nes, econv, np, &
            conf_w(nqn(jj), nql(jj)+1), conf_a(nqn(jj), nql(jj)+1), conf_r(nqn(jj), nql(jj)+1), &
            rn, nucmod, dvc)

        do ii = 1, np
   !     update radial spinors
   !     =====================
            dpc(ii,jj)=v_dp(ii)
            dqc(ii,jj)=dq(ii)
        end do
    end do

!     SCF convergence message
!     =======================
    if (.not. conv) then
        write(6,10002)niter
        stop
    end if


    write(6,10003)

!     print occupations and energies of converged spinor shells
!     =========================================================
    write(6,10001)
    do ii = 1,norb
        write(6,10006)nqn(ii),titre(ii),nel(ii),den(ii)
    end do

!   compute overlap integrals
!   =========================
    write(6,10004)
    call overlap(6,norb,dpas,nk,nql,nqn,nmax,dfl,dr,dpc,dqc,titre)

!   calculate various energy related expectation values
!   ===================================================
    dval = ddot(norb,nel,1,den,1)
    energy = 0.0
    call totale(dval,z,dr,np,d,dpas,vcoul,vtot,energy,rn,nucmod)
    write(6,10005) dval , energy(4), energy(3), energy(2), energy(1)

!   save computed and renormalized radial spinor shells to file
!   ===========================================================
!   call outspinor(norb,np,nql,nqn,nk,den,nel,dr,dpc,dqc,titre)

!   compute final atomic density
!   ============================
    d(:np) = 0.0_dp
    do ii = 1,norb
        d(:np) = d(:np) + nel(ii)*(dpc(:np,ii)*dpc(:np,ii)+dqc(:np,ii)*dqc(:np,ii))
    end do

    d_core(:np) = 0.0_dp
    do ii = 1, n_core
        d_core(:np) = d_core(:np) + nel(ii)*(dpc(:np,ii)*dpc(:np,ii)+dqc(:np,ii)*dqc(:np,ii))
    end do


!     compute final potentials
!     veff->total+confining, vcoul->Coulomb dtot->nucl+coul+xc, vnc->Coulomb+nucl
!     ========================================================
    call potsl(veff, vcoul, vnc, vtot, & ! outputs
         dr, &
         d, & ! (input: density)
         dpas, z, np, ion, &
         iter, rn, nucmod, fxcname)
    
!    open(26,file='finalpot')
!    do ii = 1, np
!        write(26,'(d25.16)') veff(ii)
!    end do
!    close(26)

!     spline fitting of potentials
!     ============================
    call spline3(np,dr,vnc,dpas,'potnc')
    call spline3(np,dr,vtot,dpas,'pottot')

!     transform final density into radial density
!     ===========================================
    d(:np) = d(:np)/(4.0_dp*pi*dr(:np)*dr(:np))
    if (n_core > 0) then
        d_core(:np) = d_core(:np)/(4.0_dp*pi*dr(:np)*dr(:np))
        call spline3(np,dr,d_core,dpas,'dens_core')
    end if

!   spline fitting of radial density
!   ================================
    call spline3(np,dr,d,dpas,'dens')
    
    !for bccms twocent
    open(26, file='pot.dat')
    write(26, *) np
    do ii = 1, np
        write(26, '(6D25.16)') dr(ii), 0.0, vnc(ii)-vcoul(ii), vcoul(ii), vtot(ii)-vnc(ii), vtot(ii)-vnc(ii)
    end do
    close(26)

    open(26, file='dens.dat')
    allocate(temp_dv(np))
    allocate(temp_d2v(np))
    d(:) = d(:)*4.0_dp*pi
    call spline3_return(np,dr,d,dpas,'dens_bccms', temp_dv, temp_d2v)
    write(26, *) np
    do ii = 1, np
        write(26, '(7D25.16)') dr(ii), 0.0, d(ii), temp_dv(ii), temp_d2v(ii), 0.0, 0.0
    end do
    close(26)
    deallocate(temp_dv, temp_d2v)

!   compute scalar relativistic valence orbitals
!   ============================================
    call nonrel(norb,np,nqn,nql,nk,dr,dpc,nel,den,dpas)

    call finalize_libxc

!     format section
!     ==============
10000 format('   * start of self-consistent DFT procedure'//             &
      '                potential      wave function     spinor energy',  &
      '     largest potential change this iteration'/                    &
      '  iteration    convergence      convergence       convergence',   &
      '         at distance       currrent value'/                       &
      ' ===========  =============    =============     =============',  &
      '       =============     ================')
10001 format(/'  * atom electronic structure and final spinor ',         &
      'energies:'//'    spinor type     occupation       final energy'/  &
      '  ==============  ==============  ==================')
10002 format(' ERROR: your calculations has not converged ',             &
      'in ',I3,' iterations'/'   * you may want to increase',            &
      ' the number of iterations in the input file')
10003 format(/'  * SCF procedure has converged'/)
10004 format(//'  * radial overlap integrals for spinors'//              &
      '     spinor 1        spinor 2            overlap integral'/       &
      '    ==========      ==========          ==================')
10005 format(/'  * energy related expectation values:'//                 &
      '                    total energy: ',f17.8/                        &
      '                  kinetic energy: ',f17.8/                        &
      '                  Coulomb energy: ',f17.8/                        &
      '                 exchange energy: ',f17.8/                        &
      '       nuclear attraction energy: ',f17.8)
10006 format(7x,i1,1x,a4,7x,f8.5,2x,f20.5)
10007 format(I7,3(1PE18.4),2(1PE20.6))

end
!***********************************************************************
!***********************************************************************
