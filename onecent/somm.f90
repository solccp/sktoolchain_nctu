!***********************************************************************
!***********************************************************************
subroutine somm(dr,dpc,dq,dpas,da,m,np)
!***********************************************************************
!
!     compute m'th moment of a given spinor shell
!     integration of (dpc+dq)*(dr^m)  using the Simpson method 
!     over all the grid points
!
!     dr - grid
!     dpc - large component
!     dq - small component
!     dpas - integration step (exponential)
!     da - value of the computed moment
!     m - exponent for the calculated moment
!     np - number of points on the grid
!
!***********************************************************************
    use Accuracy
    use Limits
    implicit none

    integer, intent(in) :: m, np
    real(dp), intent(in) :: dr(maxgrid),dpc(maxgrid),dq(maxgrid),dpas
    real(dp), intent(inout) :: da

    integer :: ii
    real(dp) :: d1,db,dc,dl


    d1 = da + m + 1
    da = 0.0d0
    db = 0.0d0
    do ii = 1, np
        dl = dr(ii)**dble(m+1)
        if (ii /= 1 .and. ii /= np) then
            dl = 2.0d0*dl
            if (mod(ii,2) == 0) dl = 2.0d0*dl
        end if
        dc = dpc(ii)*dl
        if (dc < 0.0d0) then
            db = db + dc
        else if (dc > 0.0d0) then
            da = da + dc
        end if
        dc = dq(ii)*dl
        if (dc < 0.0d0) then
            db = db + dc
        else if (dc > 0.0d0) then
            da = da + dc
        end if
    end do
    da = dpas*(da+db)/3.0d0
    dc = dexp(dpas) - 1.0d0
    db = d1*(d1+1.0d0)*dc*dexp(dpas*(d1-1.0d0))
    db = dr(1)*(dr(2)**dble(m))/db
    dc = (dr(1)**dble(m+1))*(1.0d0+1.0d0/(dc*(d1+1.0d0)))/d1
    da = da + dc*(dpc(1)+dq(1)) - db*(dpc(2)+dq(2))
end subroutine
