!***********************************************************************
!***********************************************************************
subroutine inouh(dpc,dq,dep,deq,dr,dq1,dfl,dv,z,test,jc,dd,c,dk,rn,nucmod)
!***********************************************************************
!
!     this routine finds initial conditions (5 initial points of dpc and dq
!     and 5 initial points of dep and deq) for the outward integration
!     for the spinor shell jc using a finite-size (nucmod=1) or a point
!     (nucmod=0) model of the nucleus
!
!     dpc     - large component
!     dq     - small component
!     dr     - integration grid
!     dq1    - sign of small component Q at r=0
!     dfl    - gamma factor = sqrt(dk*dk - z*z/c*c)
!     dv     - potential
!     z      - atomic number
!     test   - energy convergence criterion
!     dep    - derivatives of dpc at 5 first points
!     deq    - derivatives of dq at 5 first points
!     dd     - spinor energy
!     c      - speed of light
!     dk     - kappa quantum number
!     rn     - radius of finite-size nucleus
!     nucmod - flag for point (0) or finite-size (1) nucleus 
!
!***********************************************************************
    use Accuracy
    use Limits
    implicit none

    integer, intent(in) :: jc, nucmod, z, dk
    real(dp), intent(in) :: test, c, rn, dfl, dv(maxgrid), dq1, dd, dr(maxgrid)
    real(dp), intent(out) :: dpc(5), dq(5), dep(5), deq(5)

    real(dp) :: pc(0:20), qc(0:20)

    integer :: i,m
    real(dp) :: dm,rr,pot,s,a,b
    real(dp) :: ap,bp

!     initialize wave function for point nucleus model
!     ================================================
    if ( nucmod == 0 ) then

!       initialize q0 for small component as +1 or -1
!       (choice of sign yields P>0 for large r)
!       ***************************************
        qc(0) = dq1

!       initialize p0 for large component
!       *********************************
        if(dk > 0) pc(0) = dq1*(dble(z)/c)/(dble(dk)+dfl)
        if(dk < 0) pc(0) = dq1*(c/dble(z))*(dble(dk)-dfl)

!       calculate first 20 expansion coefficients for P and Q
!       *****************************************************
        pot = dv(1)/c + (dble(z)/dr(1))/c - dd/c - c
        do i = 1,20
            dm = dfl + dble(i)
            pc(i) = (dm-dble(dk))*(c-pot)*qc(i-1) + (dble(z)/c)*(c+pot)*pc(i-1)
            qc(i) = (dm+dble(dk))*(c+pot)*pc(i-1) - (dble(z)/c)*(c-pot)*qc(i-1)
            pc(i) = pc(i)/(dm*dm - dble(dk*dk) + dble(z*z)/(c*c))
            qc(i) = qc(i)/(dm*dm - dble(dk*dk) + dble(z*z)/(c*c))
        end do

!       initialize P, P', Q, Q' at first 5 points of mesh
!       *************************************************
        dpc(1:5) = zero
        dq(1:5) = zero
        dep(1:5) = zero
        deq(1:5) = zero

!       calculate value of P, P', Q, Q' at first 5 points of mesh
!       *********************************************************
        do m = 1,5
            rr = one
            do i = 0,20
                if (i /= 0) rr = rr*dr(m)
                dpc(m) = dpc(m) + pc(i)*rr
                dq(m) = dq(m) + qc(i)*rr
                dep(m) = dep(m) + (dble(i)+dfl)*pc(i)*rr
                deq(m) = deq(m) + (dble(i)+dfl)*qc(i)*rr
            end do
        end do

!       check convergence of the expansion
!       **********************************
        if (abs(pc(20)*rr/dpc(5)) > test .or. abs(qc(20)*rr/dq(5)) > test) then
            write(6,*)' ERROR: could not find initial conditions for outward integration for shell ',jc
            write(6,*)'   * polynomial expansion did not converged'
            stop
        end if

!   initialize wave function for finite-size nucleus model
!   ======================================================
    else

!       fit potential at origin to a parabola (first 10 points)
!       ===================================== =================
!	bp = (dv(20)-dv(1))/(dr(20)-dr(1))/(dr(20)+dr(1))
!	ap = 0.5d0*(dv(20)+dv(1)-bp*(dr(20)*dr(20)+dr(1)*dr(1)))

!       initialize p0 and q0 choosing a sign that yields P>0 for large r
!       ****************************************************************
        if(dk > 0)then
            pc(0) = zero
            qc(0) = dq1
        else
            pc(0) = -dq1
            qc(0) = zero
        end if

!       calculate auxiliary quantities
!       ******************************
        s = 0.5_dp*dble(z)/(c*rn*rn*rn)
        b = dd/c + 1.5_dp*dble(z)/(rn*c)
!	s = - bp/c
!	b = (dd-ap)/c
        a = 2.0_dp*c + b

!       calculate p1, q1, p2, and q2
!       ****************************
        do i = 1,2
            pc(i) =  a*qc(i-1)/(dble(i+dk+abs(dk)))
            qc(i) = -b*pc(i-1)/(dble(i-dk+abs(dk)))
        end do

!       calculate first 20 expansion coefficients for P and Q
!       *****************************************************
        do i = 3,20
            pc(i) = ( a*qc(i-1) - s*qc(i-3))/(dble(i+dk+abs(dk)))
            qc(i) = (-b*pc(i-1) + s*pc(i-3))/(dble(i-dk+abs(dk)))
        end do

!       initialize P, P', Q, Q' at first 5 points of mesh
!       *************************************************
        dpc(1:5) = zero
        dq(1:5) = zero
        dep(1:5) = zero
        deq(1:5) = zero

!       calculate value of P, P', Q, Q' at first 5 points of mesh
!       *********************************************************
        do m = 1,5
            rr = one
            do i = 0,20
                if (i /= 0) rr = rr*dr(m)
                dpc(m) = dpc(m) + pc(i)*rr
                dq(m) = dq(m) + qc(i)*rr
                dep(m) = dep(m) + (dble(i)+dfl)*pc(i)*rr
                deq(m) = deq(m) + (dble(i)+dfl)*qc(i)*rr
            end do
        end do

!       check convergence of the expansion
!       **********************************
        if (abs(pc(20)*rr/dpc(5)) > test .or. abs(qc(20)*rr/dq(5)) > test) then
            write(6,*)' ERROR: could not find initial conditions for outward integration for shell ',jc
            write(6,*)'   * polynomial expansion did not converged'
            stop
        end if
    end if
end subroutine
!***********************************************************************
!***********************************************************************

