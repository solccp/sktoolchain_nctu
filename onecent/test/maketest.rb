#!/usr/bin/env ruby

require 'bigdecimal'

OC_PROG = "../../onecent"

names = `ls inputs/input_*`.split.map {|x| File.basename(x).split("_")[1..-1]}

p names

`mkdir -p temp`
pwd = Dir.pwd
Dir.chdir("temp")
errors = []


names.each do |name|
    puts "Running #{name.join("_")}"
    `rm -rf *`
    `cp ../inputs/input_#{name.join("_")} input`
    if File.exist?("../inputs/potential_#{name.join("_")}")
        `cp ../inputs/potential_#{name.join("_")} start_pot`
    else 
        `rm -f start_pot`
    end


    ala = `#{OC_PROG} > ../new/log_#{name.join("_")} 2> ../new/log_#{name.join("_")}`
    `cp output ../new/output_#{name.join("_")}`
    
    ref_ene = BigDecimal.new(`grep '   total energy:' ../ref/output_#{name.join("_")}`.split[2])
    new_ene = BigDecimal.new(`grep '   total energy:' ../new/output_#{name.join("_")}`.split[2])

    errors << [ name, new_ene-ref_ene ]

end

errors.each do |err|
    puts "%8s %13.6E" % [err[0].join("_"), err[1]]
end



Dir.chdir(pwd)
