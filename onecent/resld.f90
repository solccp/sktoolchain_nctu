

module resld_internal
contains
subroutine find_roots(array, num_roots, roots_index, max_roots)
    use Accuracy
    implicit none
    integer, intent(out) :: num_roots
    real(dp), intent(in), dimension(:) :: array
    integer, intent(out), optional, dimension(:) :: roots_index
    integer, intent(in), optional :: max_roots

    integer :: i
    real(dp) :: last, curr
    integer :: n

    real(dp), parameter :: eps = 1.0e-12_dp
!    logical :: write_pot

    n = size(array)

!    write_pot = .false.
    num_roots = 0
    i = 1
    do while (i<n)
        i = i + 1
        last = array(i-1)
        curr = array(i)
        if ( abs(last) <= eps ) then
            cycle
        end if
        if ( abs(last) > eps .and. abs(curr) > eps ) then
            if ( sign(one,last) * sign(one,curr) > zero ) then
                cycle
            end if
        end if 
!        if (abs(last) < 1.0e-8 .or. dabs(curr) < 1.0e-8) then
!            print *, 'warning', last, curr
!            write_pot = .true.
!        end if
        num_roots = num_roots + 1
        if (present(roots_index)) then
            if (size(roots_index) >= num_roots) then
                roots_index(num_roots) = i
            end if
        end if
        if (present(max_roots)) then
            if (num_roots >= max_roots) then
                exit
            end if
        end if
    end do
!    if (write_pot) then
!        write(99, *) '#', num_roots
!        do i=1, n
!            write(99, '(i5, F30.12)') i, array(i)
!        end do
!        write(99, *)
!    end if
end subroutine
end module

!***********************************************************************
!***********************************************************************
subroutine resld(nqn,nql,nk,imax,de,dfl,dq1,jc,dv,dr,dpc,dq,dpas,  &
                       z,nes,test,np,conf_w,conf_a,conf_r,rn,nucmod,dvc)
!***********************************************************************
!
!     solution of radial Dirac DFT equation for spinor jc
!
!     note that derivatives of P and Q are taken with respect
!     to (ln r) and not with respect to (r)
!
!     nqn    - principal quantum number of spinor jc
!     nql    - orbital quantum number of spinor jc
!     nk     - kappa quantum number of spinor jc
!     imax   - last grid point for tabulating the wave function
!     de     - spinor energy
!     dfl    - the leading power of spinor jc expansion at origin
!     dq1    - sign of expansion of the small component of spinor jc at origin
!     jc     - label of current spinor (1,2,3,...)
!     dv     - potential
!     dr     - integration grid
!     dpc     - large component spinor radial wave function
!     dq     - small component spinor radial wave function
!     dpas   - logarithmic distance between grid points
!     z      - atomic number
!     nes    - maximal number of spinor energy adjustments
!     test   - ???????????????????????
!     np     - number of grid points
!     conf_w - height of the Woods-Saxon confining potential
!     rn     - radius of the nucleus in the finite-size nucleus model
!     nucmod - flag for the nuclear model (0 for point nucleus, 1 for finite-size nucleus)
!     dvc    - speed of light
!
!***********************************************************************
    use Accuracy
    use Limits
    use resld_internal
    implicit none

    integer, intent(in)    :: nqn, nql, nk, nucmod, jc, z, nes, np
    integer, intent(out)   :: imax
    real(dp), intent(in)   :: dfl, rn, dvc, test, dpas
    real(dp), intent(in)   :: conf_w, conf_a, conf_r
    real(dp), intent(in)   :: dv(maxgrid), dr(maxgrid)
    real(dp), intent(inout):: dq1, de
    real(dp), intent(out)  :: dpc(maxgrid), dq(maxgrid)

    integer :: ilim, lll, ii, k, imat, jj, noeud, ies, nd

    integer :: t1
    integer :: roots(1)


    real(dp) :: elim,val,dval,dbe,dsum
    real(dp) :: dep(5),deq(5),dpm,dqm,dm,dd,dpq,aa, dle

    integer :: steps
    real(dp) :: veff(maxgrid), de1, veff_radial(maxgrid)

    real(dp) :: dep2(5), deq2(5)
    character(len=20) :: filename

    real(dp) :: ori_min, ori_max
    real(dp) :: cur_min, cur_max, shift
    logical :: lock

    steps = 0
    lock = .false.

!   initialize variables and constants
!   ==================================
    nd=0
    noeud=nqn-nql


!   find the minimum of effective radial potential
!   ==============================================
    lll = (nql*(nql+1))/2
    veff_radial(:np) = dv(:np) + conf_w/(1.0_dp+exp(-conf_a*(dr(:np)-conf_r)))
    veff(:np) = veff_radial(:np) + dble(lll)/(dr(:np)*dr(:np))
    if (lll == 0) then
        elim = -dble(z*z)/(1.5d0*dble(nqn*nqn))
        ilim = 1
    else
        elim = veff(1) 
        ilim = 1
        do ii = 2,np
          val = veff(ii) 
          if (val <= elim) then
            elim = val
            ilim = ii
          end if
        end do
    end if
    
!   check if potential is bounded
!   =============================
    if ( elim >= veff(np) ) then 
        write(6,*)' ERROR: potential is not bounded'
        stop
    end if

!   assure that starting spinor energy is above the potential minimum
!   =================================================================
    if (de <= elim) then
        de = veff(np) 
        de = (de+9.0_dp*elim)/10.0_dp
    end if

    

    ori_min = elim
    ori_max = veff(np)

    cur_min = ori_min
    cur_max = ori_max

    de = (cur_min+cur_max)/2.0



!   start iterative procedure of finding de, dpc, and dq for spinor jc
!   =================================================================
100 continue
    do ies = 1, nes

!       find the outer classical turning point (imat)
!       =============================================
        call find_roots(veff(ilim:np)-de,nd,roots,1)
        imat = roots(1) + ilim 
        if (imat < 5) then
            write(6,*)' ERROR: Outer turning point too close to nucleus for spinor ', jc
            stop
        end if    

!   find series expansion of P & Q at origin for outward integration
!   ================================================================
    call inouh(dpc,dq,dep,deq,dr,dq1,dfl,veff_radial,z,test,jc,de,dvc,nk,rn,nucmod)

!   multiply series expansion of P & Q at origin by prefactor r^dfl
!   ===============================================================
    dpc(1:5)  = dpc(1:5)*dr(1:5)**dfl
    dq(1:5)  = dq(1:5)*dr(1:5)**dfl
    dep(1:5) = dep(1:5)*dr(1:5)**dfl
    deq(1:5) = deq(1:5)*dr(1:5)**dfl

!   check sign of components at first grid point
!   ============================================
    k = -1 + 2*mod(noeud,2)
    if ( k*dpc(1) < zero .or. k*nk*dq(1) < zero ) then
        write(6,*)' ERROR: wrong series expansion at origin for spinor shell ',jc
        stop
        end if

!       outward integration from the sixth point to the 50th point
!       ==========================================================

        dm = dpas / 720.0_dp
        do ii = 6, 50
            dpc(ii) = dpc(ii-1)
            dq(ii) = dq(ii-1)
            call inth(dpc(ii),dq(ii),dep,deq,veff_radial(ii),dr(ii),de,dvc,nk,dm)
        end do        

!       inward integration from 50th to 1st point to clean the wave function
!       ====================================================================
        dep(:) = dep(5:1:-1)
        deq(:) = deq(5:1:-1)
        do ii = 45,1,-1
            dpc(ii) = dpc(ii+1)
            dq(ii) = dq(ii+1)
            call inth(dpc(ii),dq(ii),dep,deq,veff_radial(ii),dr(ii),de,dvc,nk,-dm)
        end do
!       restart outward integration from the sixth point to the classical turning point
!       ===============================================================================
        dep(:) = dep(5:1:-1)
        deq(:) = deq(5:1:-1)
        do ii = 6, imat
            dpc(ii) = dpc(ii-1)
            dq(ii) = dq(ii-1)
            call inth(dpc(ii),dq(ii),dep,deq,veff_radial(ii),dr(ii),de,dvc,nk,dm)
        end do        

!       check the number of nodes and rescale energy if necessary
!       =========================================================
        call find_roots(dpc(5:imat),nd)
!       the minimum number of nodes is 1, so add 1
        nd = nd + 1

!        write(*, '(i4, i4, 5F20.12)') nd, noeud, de, cur_min, cur_max, ori_min, ori_max
        if (nd /= noeud) then

          if (lock) then
              de = de - shift
                if ( nd < noeud ) then
                  de = de+0.001
                else
                  de = de-0.001
                end if
              de = de + shift
          else
            if ( nd < noeud ) then
                cur_min = (cur_min+de)/2.0
                if (dabs(cur_min-de)<1.0e-4) then
                    cur_min = (cur_min+ori_min)/2.0
                end if
            else
                cur_max = (cur_max+de)/2.0
                if (dabs(cur_max-de)<1.0e-4) then
                    cur_max = (cur_max+ori_max)/2.0
                end if
            end if
            de = (cur_min+cur_max)/2.0             
          end if
!            if ( de < 0.0d0 ) then
!                if ( nd < noeud ) then
!                    de = de + abs(0.2_dp*de)
!                else
!                    de = de - abs(0.2_dp*de)
!                end if
!                if ( abs(de) < 1.0e-8 ) then
!                    de = 0.1
!                end if
!            else
!                if ( nd < noeud ) then
!                    de = de + abs(0.2_dp*de)
!                else
!                    de = de - abs(0.2_dp*de)
!                end if
!                if ( abs(de) < 1.0e-8 ) then
!                    de = -0.1
!                end if
!            end if
        else
            lock = .true.
            ori_max = cur_max
            ori_min = cur_min
            shift = cur_max
        end if

!       check if rescaled energy matches the limits of potential
!       ========================================================
        if (nd <= noeud) then
            if (de < elim) then
                write(6,*)' ERROR: could not find the proper number', &
                            ' of nodes for spinor shell ',jc
                write(6,*)'   * expected number of nodes:',noeud
                write(6,*)'   * found number of nodes:',nd
                stop
            end if
        end if

!       restart the iterative procedure with rescaled spinor energy
!       ===========================================================
        if ( nd /= noeud ) then
            steps = steps + 1
            if (steps > 500)  then
                write(6,*)' ERROR: iterative procedure of spinor energy ', &
                        'adjustment did not converged for spinor shell ',jc 
                write(*,'(4(I3))') jc,nqn,nql,nk
                write(*, '(2I3,1x,2F20.12)') nd, noeud, de, veff(imat)
                stop
            end if
            go to 100
        end if


!       save values of P & Q at turning point from outward integration
!       ==============================================================
        dpm = dpc(imat)
        dqm = dq(imat)

!       find grid point IMAX to start out inward integration
!       ====================================================
!        if (ies == 1) then
            do ii = 1,np,2
                imax = np + 1 - ii
                val = (veff_radial(imax)-de)*dr(imax)*dr(imax)
                if (val <= 400.0_dp) exit
            end do
!        end if

999     continue
!       find asymptotic expansion of P & Q at large distances
!       =====================================================

        dd = dsqrt(abs(de-conf_w)*(2.0_dp+abs(de-conf_w)/(dvc*dvc)))
        dpq = -dd/(2.0_dp*dvc + (de-conf_w)/dvc)
        do ii = 1,5
            jj = imax+1-ii
            dpc(jj)  =  dexp(-dd*dr(jj))
            dep(ii) = -dd*dpc(jj)*dr(jj)
            dq(jj)  =  dpq*dpc(jj)
            deq(ii) =  dpq*dep(ii)
        end do
    
        if (ies == 1) then
           if ( abs(dq(imax)) < 1.0e-12_dp) then
                imax = imax - 10
                goto 999
            end if
        end if



!       inward integration from imax-5 to imat
!       ======================================
        do ii = imax-5,imat,-1
            dpc(ii) = dpc(ii+1)
            dq(ii) = dq(ii+1)
            call inth(dpc(ii),dq(ii),dep,deq,veff_radial(ii),dr(ii),de,dvc,nk,-dm)
        end do

!       check the sign of the large component at imat
!       =============================================
        dval = dpm/dpc(imat)
        if (dval <= zero) then
        !    print *, imat, dr(imax), dpm, dpc(imat)
            write(6,*)' ERROR: wrong sign of large component for spinor shell ',jc
            stop
        end if

!       rescale the outer (imat:imax) part of dp and dq
!       ===============================================
        dpc(imat:imax) = dpc(imat:imax)*dval
        dq(imat:imax) = dq(imat:imax)*dval

!       compute the norm of the spinor jc
!       =================================
        dsum = 3.0_dp*dr(1)*(dpc(1)*dpc(1)+dq(1)*dq(1))/(dpas*(2.0_dp*dfl+1.0_dp))
        do ii = 3,imax,2
           dsum = dsum + dr(ii)*(dpc(ii)*dpc(ii)+dq(ii)*dq(ii)) &
           + 4.0_dp*dr(ii-1)*(dpc(ii-1)*dpc(ii-1)+dq(ii-1)*dq(ii-1)) &
           + dr(ii-2)*(dpc(ii-2)*dpc(ii-2)+dq(ii-2)*dq(ii-2))
        end do
        dsum = dpas*(dsum+dr(imat)*(dqm*dqm-dq(imat)*dq(imat)))/3.0_dp

!         modification of spinor energy
!         =============================
        dbe = dpc(imat)*(dqm-dq(imat))*dvc/dsum
        val = dabs(dbe/de)

!       calculations for spinor jc converged
!       ====================================
        if (val <= test) exit 

!       readjust spinor energy and go to the next iteration
!       ===================================================
        dval = de + dbe/2.0_dp
        de = dval


!   the end of iteration
!   ====================
    end do

!   calculations did not converge
!   =============================
    if (ies == nes) then
        write(6,*)' ERROR: iterative procedure of spinor energy ', &
                    'adjustment did not converged for spinor shell ',jc 
        write(6,*)'   * try changing the number of trials in the ', &
                    'input file'
        write(6,*) '   * current number of trials is ',nes, &
                    ' (default=15)'
        stop
    end if

!   renormalize both components
!   ===========================
    dsum = dsqrt(dsum)
    dq1 = dq1/dsum
    dpc(:imax) = dpc(:imax)/dsum
    dq(:imax) = dq(:imax)/dsum
    dpc(imax+1:) = zero
    dq(imax+1:) = zero
end subroutine
!***********************************************************************
!***********************************************************************


    
