!***********************************************************************
!   *    transcribed into f90
!   *    @Chien-Pin Chou                                12.13.2009   *
!***********************************************************************
subroutine diffrho(f,n,df,ddf,dr,dx)
!***********************************************************************
!
!   *    <subroutine diffrho> differentiates the functions           *
!   *    <f(i)> and <df(i)>the first <i> net points.                 *
!   *    <di> is the increment in the logarithmic i-net.             *
!   *                                                                *
!   *    the results are stored in <df(r)> and <ddf(r)>              *
!   *    <r> is the radial koordinate in he local r-net              *
!   *                                                                *
!   *    @U.Gerstmann                                   11.11.1998   *
!
!***********************************************************************
    use Accuracy
    use Limits
    implicit none

    integer, intent(in) :: n
    real(dp), intent(in) :: f(maxgrid)

    integer :: j,ip

    real(dp) :: dr(maxgrid),df(maxgrid),ddf(maxgrid)
    real(dp) :: h(maxgrid),dh(maxgrid),ddh(maxgrid),dx

!   local copy of density
!   =====================
    h = f

!   2 x differentiation using 4 point formula and 
!   transformation to logarithmic coordintes
!   ========================================
    call differ(h,dx,n,dh)
    call differ(dh,dx,n,ddh)
    ddf(:n) = (ddh(:n)-dh(:n))/(dr(:n)*dr(:n))
    df(:n)  = dh(:n)/dr(:n)
end subroutine
!***********************************************************************
!***********************************************************************


!***********************************************************************
!***********************************************************************
subroutine differ(f,dx,n,df)
!***********************************************************************
!
!   *                                                                *
!   *    <subroutine differ> differentiates the function <f(x)> in   *
!   *    the first <n> net points.                                   *
!   *    <dx> is the increment in the logarithmic x-net.             *
!   *    the result is stored in <df(x)>.                            *
!   *                                                                *
!   *    @U.Gerstmann                                   05.11.1998   *
!
!***********************************************************************
    use Accuracy
    use Limits
    implicit none

    real(dp), intent(in) :: f(maxgrid), dx
    real(dp), intent(out) :: df(maxgrid)

    integer  :: n,i
     
    df(1)=( 6.d0*f(2)+(20.d0/3.d0)*f(4)+1.2d0*f(6)-2.45d0*f(1)-7.5d0*f(3)-3.75d0*f(5)-1.d0/6.d0*f(7) )/dx
    df(2)=( 6.d0*f(3)+(20.d0/3.d0)*f(5)+1.2d0*f(7)-2.45d0*f(2)-7.5d0*f(4)-3.75d0*f(6)-1.d0/6.d0*f(8) )/dx
    do i=3, n-2
        df(i)=( f(i-2)+8.d0*f(i+1)-8.d0*f(i-1)-f(i+2) )/12.d0/dx
    end do
    df(n-1)= (f(n)-f(n-2))/2.d0/dx
    df(n)  = (0.5d0*f(n-2)-2.d0*f(n-1)+1.5d0*f(n))/dx

end subroutine
!***********************************************************************
!***********************************************************************

