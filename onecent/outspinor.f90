subroutine outspinor(norb,np,nql,nqn,nk,den,nel,dr,dpc,dqc,titre)
    use Accuracy
    use Limits
    implicit none


    integer, intent(in) :: norb,np
    integer, intent(in) :: nk(maxsh),nqn(maxsh),nql(maxsh)
    real(dp), intent(in) :: nel(maxsh),den(maxsh),dr(maxgrid)
    real(dp), intent(in) :: dpc(maxgrid,maxsh),dqc(maxgrid,maxsh)
    character(len=4), intent(in) :: titre(maxsh)

    integer :: ii, jj
    real(dp) :: aa, bb, ab

!   output calculated radial spinors to file
!   ========================================
    open(7,file='spinors')
    write(7,10000) norb
    do ii = 1, norb
        write(7,10001)ii,nqn(ii),titre(ii),nqn(ii),nql(ii), nk(ii),den(ii)
        do jj = 1,np
            aa = dpc(jj,ii)
            bb = dqc(jj,ii)
            ab = aa*aa + bb*bb
            write(7,'(5d25.16)')dr(jj),aa,bb,ab,nel(ii)*ab
        end do
    end do
    close(7)

!   format section
!   ==============
10000 format('# ',I3,' spinor radial wave functions'/ &
      '      normalization: I(P*P+Q*Q)dr = 1'/'      if you need ', &
      'real radial wave functions divide P and Q at each', &
      ' point by 2*sqrt(pi)*r'///'         distance              ', &
      'large component P        small component Q           ', &
      ' P*P+Q*Q             electron density'/ &
      11x,'||',25x,'||',23x,'||',23x,'||',22x,'||'/ &
      10x,'_||_',23x,'_||_',21x,'_||_',21x,'_||_',20x,'_||_'/ &
      10x,'\  /',23x,'\  /',21x,'\  /',21x,'\  /',20x,'\  /'/ &
      11x,'\/',25x,'\/',23x,'\/',23x,'\/',22x,'\/'/) 
10001 format(/'# spinor shell',i3,':',2x,i2,1x,a4,'  with  N =',i3, &
      '  L =',i3,'  K =',i3,'   and energy',f16.8)

end subroutine
